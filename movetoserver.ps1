Write-Host("Updating KO TO Server:")

$timestamp = Get-Date -UFormat "%y-%m-%d_%H-%M-%S"

[string]$srcDir = "C:\Users\deadbeef\Saved Games\DCS.openbeta\Missions\TKO\scripts\"
[string]$tgtDir = "\\0XDE4DB33F\tko\scripts\"
[string]$h0kDir = "\\0XDE4DB33F\Hooks\"
[string]$bkpDir = $tgtDir + "\backup\"

Function moveToServ {
	param([string]$file)
	$src = $srcDir + $file
	$tgt = $tgtDir + $file
    $bkp = $bkpDir + $file + $timestamp
    $dateSrc = (Get-Item $src).LastWriteTime
    $dateTgt = (Get-Item $tgt).LastWriteTime

    if ($dateSrc -gt $dateTgt) {
        Write-Host(" + backing up and copying '$file' to '$tgtDir'")
        
        Copy-Item -Path $tgt -Destination $bkp
	    Copy-Item -Path $src -Destination $tgt
    } else {
        Write-Host(" - no change for file '$file'")
    }
}

Function moveToHooks {
	param([string]$file)
	$src = $srcDir + $file
	$tgt = $h0kDir + $file
    $bkp = $bkpDir + $file + $timestamp
    
    $dateSrc = (Get-Item $src).LastWriteTime
    $dateTgt = (Get-Item $tgt).LastWriteTime

    if ($dateSrc -gt $dateTgt) {
        Write-Host(" + backing up and copying '$file' from '$srcDir' to '$tgtDir'")
        Write-Host("target is newer")
        Copy-Item -Path $tgt -Destination $bkp
	    Copy-Item -Path $src -Destination $tgt
    } else {
        Write-Host(" - no change for file '$file'")
    }
}

Function moveToServDir([string]$dir2, [string]$file) {
	$src = $srcDir + $file
	$tgt = $tgtDir + $dir2 + $file
    $bkp = $bkpDir + $file + $timestamp
    
    $dateSrc = (Get-Item $src).LastWriteTime
    $dateTgt = (Get-Item $tgt).LastWriteTime

    if ($dateSrc -gt $dateTgt) {
        Write-Host(" + backing up and copying '$file' from '$srcDir' to '$tgtDir'")
        Write-Host("target is newer")
        Copy-Item -Path $tgt -Destination $bkp
	    Copy-Item -Path $src -Destination $tgt
    } else {
        Write-Host(" - no change for file '$file'")
    }
}

moveToServ -File "ko_Engine2.lua"
moveToServ -File "ko_Scoreboard.lua"
moveToServ -File "ko_TCPSocket.lua"
moveToServ -File "CTLD_ko.lua"
moveToServ -File "CTLD.lua"
moveToServ -File "EWRS.lua"
moveToServ -File "ko_data.lua"
moveToHooks -File "koServerToolsGameGUI.lua"
moveToServDir "node\" "wingthor_ssh.js"

Read-Host -Prompt "Press Enter to exit"