function serialize(t, i)													--function to turn a table into a string (works to transmutate strings, numbers and sub-tables)
	if not i then
        i = 0
    elseif i >= 3 then
        return "{ '... deeper level' },\n"
	end
	if not t then 
		return "nil"
	end
    if not type(t) == table then
        if type(t) == "string" then
            return "'"..t.."'"
        elseif type(t) == "number" then
            return tostring(t)
        elseif type(t) == "function" then
            return "function"
        end
	end
	
	local text = "{\n"
	local tab = ""
	for n = 1, i + 1 do																	--controls the indent for the current text line
		tab = tab .. "\t"
	end
	for k,v in pairs(t) do
		if type(k) == "string" then
			text = text .. tab .. "['" .. k .. "']" .. " = "
			if type(v) == "string" then
				text = text .. "'" .. v .. "',\n"
			elseif type(v) == "number" then
				text = text .. v .. ",\n"
			elseif type(v) == "table" then
				text = text .. serialize(v, i + 1)
            elseif type(v) == "boolean" then
                text = text .. tostring(v) .. ",\n"
            elseif type(v) == "function" then
                text = text .. "function(),\n"
            else
                text = text .. type(k)
			end
		elseif type(k) == "number" then
			text = text .. tab .. "[" .. k .. "] = "
			if type(v) == "string" then
				text = text .. "'" .. v .. "',\n"
			elseif type(v) == "number" then
				text = text .. v .. ",\n"
			elseif type(v) == "table" then
				text = text .. serialize(v, i + 1)
			elseif type(v) == "boolean" then
                text = text .. tostring(v) .. ",\n"
            elseif type(v) == "function" then
                text = text .. "function(),\n"
            else
                text = text .. type(k)..",\n"
            end	
        else
            text = text .. type(k)..",\n"
        end
	end
	tab = ""
	for n = 1, i do																		--indent for closing bracket is one less then previous text line
		tab = tab .. "\t"
	end
	if i == 0 then
		text = text .. tab .. "}\n"														--the last bracket should not be followed by an comma
	else
		text = text .. tab .. "},\n"													--all brackets with indent higher than 0 are followed by a comma
    end
    
	return text
end