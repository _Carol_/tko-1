--[[
    populate farps

    This scripts adds all units from a template FARP to all other farps

    usage: 
    - unsanitize lfs in DCS\Scripts\MissionScripting.lua or saving to file will not work. 
    - specify output filenames 
    - specify the FARP name and how many farps per side and objective you have.
        For instance if you have FARP Beslan which consists of two farp-pads for each side, those would be named "FARP Beslan 1" and FARP Beslan 2", what matters is that they 
        have an index at the end of the name. If you create all farps of the same sceme, you can create a template farpname, like "FARP_Template1blue" and "FARP_Template2blue" (masterFARPname = "FARP_Template"), or
        for ease just "FARP1blue" and "FARP2blue" (masterFARPname = "FARP") plus the same with red at the end. the masterFARPname variable specifies just how those are named.

        Now you create the slots for these FARPs, and also name them "FARP1blue whateverslotname". The FARP1 in the slotname will be replaced with your masterFARPname, the rest of the name
        will be kept. The original template-slots will be removed. (if you want to keep them, comment the line with table.remove)
    - run this script after your mission is loaded and ready. 
    - copy the "mission" file into the root directory of your .miz file
    - extract the dictionary file of your .miz into another directory. and open it in a text editor.
    - paste the content of dictionary.lua created by this script without (!) the {} brackets into the brackets of your dictionary file, and copy it back into the same location
        the dictionary file makes sure you don't see "Dict_Key..."-style names in the slot-selection
    
    run the script via trigger in your mission. close the mission, make a copy of your mission file, and copy the created "mission" file into the root of your .miz (.miz files are zip files)
--]]

do

    local masterFARPname = "FARP"  -- the route of this unit is going to be used as the master route for all other units
    local numFARPS = 2              -- how many farps per coalition and objective are in use, if 2 and masterfarp is FARP, this would result in templating FARP1 and FARP2 to all other farps indext 1 and 2

    local missionFile = "C:\\Users\\deadbeef\\Saved Games\\DCS.openbeta\\Missions\\TKO\\miz\\tko\\farps\\mission" -- once finished, remove '_new.lua' from the file, and put it into the .miz zipfile
    local dictFile = "C:\\Users\\deadbeef\\Saved Games\\DCS.openbeta\\Missions\\TKO\\miz\\tko\\farps\\dictionary.lua" -- append this table to the dictionary inside your .miz file, do not replace!

    local typeHelper = {
        "helicopter",
        "plane",
    }

    -- function to convert a table to a string
    function serialize(t, i)
        i = i or 0
        if not t then 
            return "nil"
        end
        
        local text = "{\n"
        local tab = ""
        for n = 1, i + 1 do		--controls the indent for the current text line
            tab = tab .. "\t"
        end
        for k,v in pairs(t) do
            if type(k) == "string" then
                text = text .. tab .. '["' .. k .. '"]' .. " = "
                if type(v) == "string" then
                    text = text .. '"' .. v:gsub('"','\\"') .. '",\n'
                elseif type(v) == "number" then
                    text = text .. v .. ",\n"
                elseif type(v) == "table" then
                    text = text .. serialize(v, i + 1)
                elseif type(v) == "boolean" then
                    text = text .. tostring(v) .. ",\n"
                end
            elseif type(k) == "number" then
                text = text .. tab .. "[" .. k .. "] = "
                if type(v) == "string" then
                    text = text .. '"' .. v:gsub('"','\\"') .. '",\n'
                elseif type(v) == "number" then
                    text = text .. v .. ",\n"
                elseif type(v) == "table" then
                    text = text .. serialize(v, i + 1)
                elseif type(v) == "boolean" then
                    text = text .. tostring(v) .. ",\n"
                end	
            end
        end
        tab = ""
        for n = 1, i do																		--indent for closing bracket is one less then previous text line
            tab = tab .. "\t"
        end
        if i == 0 then
            text = text .. tab .. "}\n"														--the last bracket should not be followed by an comma
        else
            text = text .. tab .. "},\n"													--all brackets with indent higher than 0 are followed by a comma
        end
        return text
    end

    -- make 1:1 copy of table instead of reference (= mist.utils.deepCopy)
    function deepCopy(object)
		local lookup_table = {}
		local function _copy(object)
			if type(object) ~= "table" then
				return object
			elseif lookup_table[object] then
				return lookup_table[object]
			end
			local new_table = {}
			lookup_table[object] = new_table
			for index, value in pairs(object) do
				new_table[_copy(index)] = _copy(value)
			end
			return setmetatable(new_table, getmetatable(object))
		end
		return _copy(object)
	end


    --
    ---------------------------------------------------------
    --
    -- fun starts here

    local masterFARPs = {}
    local otherFARPs = {}
    local masterHelis = {}
    local dictionary = {}
    local dictIdx = env.mission.maxDictId -- start dictionary at safe distance

    local mission = deepCopy(env.mission) -- env.mission contains the mission lua table from inside the .mis zipfile    
        
    env.info("finding all farps")
    for coa, category in pairs(mission.coalition) do
        for cid, country in pairs(category.country) do
            if country.static then
                for id, static in pairs(country.static.group) do
                    if static.units[1].type == "FARP" then
                        local farpName = env.getValueDictByKey(static.name)
                        local master = false

                        -- check if any farp fits into the naming of the master farps and collect the x-y coords in a table
                        for idx=1, numFARPS do 
                            if farpName == masterFARPname..idx..coa then
                                env.info("found master FARP: '"..farpName.."', idx "..idx)
                                --env.info(serialize(static))
                                masterFARPs[coa] = masterFARPs[coa] or {}
                                masterFARPs[coa][cid] = masterFARPs[coa][cid] or {}
                                masterFARPs[coa][cid][idx] = {}
                                masterFARPs[coa][cid][idx].x = static.x
                                masterFARPs[coa][cid][idx].y = static.y
                                masterFARPs[coa][cid][idx].farpName = farpName
                                master = true

                                --table.remove( country.static.group, id) -- remove the farp from the mission
                            end
                        end

                        -- if not a masterfarp collect it's coords to later distribute the helis to them.
                        if not master then
                            env.info("adding FARP: '"..farpName.."' to the list ...")
                            otherFARPs[coa] = otherFARPs[coa] or {}
                            local index = tonumber(farpName:sub(-1))
                            local farp = {
                                ['farpName'] = farpName,
                                ['idx'] = index,
                                ['x'] = static.x,
                                ['y'] = static.y,
                                ['name'] = static.name,
                                ['groupId'] = static.groupId,
                            }
                            table.insert(otherFARPs[coa], farp)
                        end
                    end
                end
            end
        end
    end
    
    env.info("masterfarps: "..serialize(masterFARPs).."\n")

    env.info("looking for helicopters:")        
    for coa, category in pairs(mission.coalition) do
        env.info("checking "..coa.." coalition")
        for cid, country in pairs(category.country) do
            for _t, type in pairs(typeHelper) do
                if country[type] then
                    env.info("checking country #"..cid.." for '"..type.."'")
                    for id, group in ipairs(country[type].group) do -- go through all helicopters in the mission file
                        local groupName = env.getValueDictByKey(group.name)
                        env.info("checking #"..id..": '"..groupName.."'")
                        for idx=1, numFARPS do -- look for FARP1 and FARP2
                            local farpName = masterFARPname..idx..coa
                            if groupName:find(farpName) then            -- check if the groupname is part of the masterfarp nomenclature
                                masterHelis[coa] = masterHelis[coa] or {}
                                masterHelis[coa][cid] = masterHelis[coa][cid] or {}
                                masterHelis[coa][cid][type] = masterHelis[coa][cid][type] or {}

                                -- make sure unit only has half fuel
                                group.units[1].payload.fuel = group.units[1].payload.fuel / 2
                                
                                local heliName = groupName:gsub(farpName.." ", "") -- remove the "FARP1 " prefix from the name
                                local heli = {
                                    ['idx'] = idx,
                                    ['heliName'] = heliName, 
                                    ['origName'] = groupName, 
                                    ['group'] = deepCopy(group),
                                    ['countryId'] = cid,
                                    ['meID'] = id,  -- lets delete it after the party
                                }
                                table.insert(masterHelis[coa][cid][type], heli)
                                --table.remove(country[type].helicopter.group, id)
                                
                                env.info("- found Helicopter on Masterfarp: "..groupName.." reduced fuel to "..group.units[1].payload.fuel)
                                --env.info(serialize(group))
                            end
                        end
                        
                    end
                end
            end
        end
    end

    -- now delete master helis from the mission
    --env.info("masterHelis: "..serialize(masterHelis))
    env.info("masterHelis found:")
    for coa, countries in pairs(masterHelis) do
        for cid, country in pairs(countries) do
            for typeName, types in pairs(country) do
                for id, heli in pairs(types) do
                    env.info("removing "..coa.." "..typeName.." '"..heli.heliName.."' ('"..heli.origName.."'), idx: "..heli.idx..", country: "..heli.countryId)
                    --env.info(serialize(mission.coalition[coa].country[countryID].helicopter.group))
                    --table.remove(mission.coalition[coa].country[cid][typeName].group, heli.meID)
                end
            end
        end
    end

    -- hack hack
    -- need to find the highest groupid
    -- fuck it, lets loop again
    local gId = 0
    for coa, category in pairs(mission.coalition) do
        for cid, country in pairs(category.country) do
            for catName, cat in pairs(country) do
                if type(cat) == "table" then
                    for id, group in pairs(cat.group) do
                        if group.groupId > gId then
                            gId = group.groupId
                        end
                    end                    
                end
            end
        end
    end 
    env.info("highste groupId = "..gId)

    env.info("otherFarps :"..serialize(otherFARPs))
    
    -- now add those helis to the other farps
    for coa, farps in pairs(otherFARPs) do
        env.info("coa: "..coa)
        for x, farp in pairs(farps) do
            env.info("adding units to farp '"..farp.farpName.."'")
            -- for every farp lets spawn alls helis in the masterHelis table
            if masterHelis[coa] then
                for cid, types in pairs(masterHelis[coa]) do
                    for type, helis in pairs(types) do
                        env.info("- adding "..type.."s")
                        for h, heli in pairs(helis) do
                            if heli.idx == farp.idx then
                                local heliPrefix = farp.farpName:gsub(coa, ""):sub(1, -3)
                                local groupName = heliPrefix .. heli.heliName               -- create the group name with formula "FARPNAME+HELINAME"

                                env.info("adding '"..heli.heliName.."' to '"..heliPrefix.."', heliName = '"..groupName.."'")
                                
                                -- create dictionary entries
                                local groupNameDictKey = 'DictKey_GroupName_'..dictIdx
                                dictIdx = dictIdx + 1
                                local unitNameDictKey = 'DictKey_UnitName_'..dictIdx
                                dictIdx = dictIdx + 1

                                dictionary[groupNameDictKey] = groupName    -- groupName and unitName are the same, but need their own dictkey (or not?)
                                dictionary[unitNameDictKey] = groupName

                                gId = gId + 1   -- increment unit-ID, as we're adding one
                                
                                -- now create a copy of the name, set start point and position and name
                                local heliGroup = deepCopy(heli.group)
                                heliGroup.x = farp.x
                                heliGroup.y = farp.y
                                heliGroup.route.points[1].x = farp.x
                                heliGroup.route.points[1].y = farp.y
                                heliGroup.name = groupNameDictKey
                                heliGroup.units[1].name = unitNameDictKey
                                heliGroup.units[1].x = farp.x
                                heliGroup.units[1].y = farp.y
                                heliGroup.units[1].unitId = gId
                                heliGroup.groupId = gId
                                heliGroup.route.points[1].linkUnit = farp.groupId
                                heliGroup.route.points[1].helipadId = farp.groupId

                                -- insert into mission
                                table.insert(mission.coalition[coa].country[heli.countryId][type].group, heliGroup)
                            end
                        end
                    end
                end
            else
                env.info("FATAL ERROR: No otherfarps for coalition")
            end
        end
    end

    -- important! update max dictId!!
    mission.maxDictId = dictIdx

    -- save the new mission file
    env.info("helicopters inserted, saving mission and dictionary file")

    local exportData = "mission = " .. serialize(mission)				--The second argument is the indent for the initial code line (which is zero)
    local exportFile = assert(io.open(missionFile, "w"))
	exportFile:write(exportData)
	exportFile:flush()
    exportFile:close()
    
    env.info("mission file saved, saving dictionary")

    exportData = "dictionary = "..serialize(dictionary)				--The second argument is the indent for the initial code line (which is zero)
    exportFile = assert(io.open(dictFile, "w"))
	exportFile:write(exportData)
	exportFile:flush()
    exportFile:close()
    exportFile = nil    --]]
    
    env.info("dictionary saved")
end
