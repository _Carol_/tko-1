MissionData = {
	['Aerodrome'] = {
		['Senaki-Kolkhi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Senaki-Kolkhi AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647412.7662372,
							['x'] = -279922.62279556,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 15,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647412.74999998,
							['type'] = 'M1045 HMMWV TOW',
							['name'] = 'Senaki-Kolkhi AT/1',
							['heading'] = 332.52557316103,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -279922.62499996,
						},
						[2] = {
							['y'] = 646959.31249994,
							['type'] = 'M-2 Bradley',
							['name'] = 'Senaki-Kolkhi AT/2',
							['heading'] = 332.52557534264,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -280020.18749989,
						},
						[3] = {
							['y'] = 649153.06249994,
							['type'] = 'M1134 Stryker ATGM',
							['name'] = 'Senaki-Kolkhi AT/3',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281976.84374989,
						},
						[4] = {
							['y'] = 646511.1875,
							['type'] = 'M-1 Abrams',
							['name'] = 'Senaki-Kolkhi AT/4',
							['heading'] = 332.52557467598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282721.8125,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi SR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647150.68949682,
							['x'] = -280216.09258889,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647150.6875,
							['type'] = 'M48 Chaparral',
							['name'] = 'Senaki-Kolkhi SR SAM/1',
							['heading'] = 309.70826406259,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -280216.09375,
						},
						[2] = {
							['y'] = 647718.0625,
							['type'] = 'M48 Chaparral',
							['name'] = 'Senaki-Kolkhi SR SAM/2',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282204.25,
						},
						[3] = {
							['y'] = 648440.5625,
							['type'] = 'M6 Linebacker',
							['name'] = 'Senaki-Kolkhi SR SAM/3',
							['heading'] = 309.70826125413,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -280390.3125,
						},
						[4] = {
							['y'] = 646213.06249996,
							['type'] = 'M1097 Avenger',
							['name'] = 'Senaki-Kolkhi SR SAM/4',
							['heading'] = 309.70826272632,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282377.21874997,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi SR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647034.89573249,
							['x'] = -281610.91290211,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 14,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647034.875,
							['type'] = 'Vulcan',
							['name'] = 'Senaki-Kolkhi AAA/1',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281610.90625,
						},
						[2] = {
							['y'] = 648521.75,
							['type'] = 'Vulcan',
							['name'] = 'Senaki-Kolkhi AAA/2',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281784.90625,
						},
						[3] = {
							['y'] = 646022.5625,
							['type'] = 'Vulcan',
							['name'] = 'Senaki-Kolkhi AAA/3',
							['heading'] = 336.00001083097,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281526.5625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi AAA',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi Patriot'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 645730.85360239,
							['x'] = -282071.17876779,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 645730.875,
							['type'] = 'Patriot AMG',
							['name'] = 'Senaki-Kolkhi Patriot/1',
							['heading'] = 130.9999948998,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282071.1875,
						},
						[2] = {
							['y'] = 645681.75,
							['type'] = 'Patriot cp',
							['name'] = 'Senaki-Kolkhi Patriot/2',
							['heading'] = 311.00000035867,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282029.25,
						},
						[3] = {
							['y'] = 645819.4375,
							['type'] = 'Patriot AMG',
							['name'] = 'Senaki-Kolkhi Patriot/3',
							['heading'] = 130.99999605699,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281942.09375,
						},
						[4] = {
							['y'] = 645736.4375,
							['type'] = 'Patriot ECS',
							['name'] = 'Senaki-Kolkhi Patriot/4',
							['heading'] = 311.00000087491,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281939.125,
						},
						[5] = {
							['y'] = 645657.6875,
							['type'] = 'Patriot EPP',
							['name'] = 'Senaki-Kolkhi Patriot/5',
							['heading'] = 131.00000155776,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282105.15625,
						},
						[6] = {
							['y'] = 645781.5,
							['type'] = 'Patriot EPP',
							['name'] = 'Senaki-Kolkhi Patriot/6',
							['heading'] = 130.99999605699,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282002.84375,
						},
						[7] = {
							['y'] = 645650.5625,
							['type'] = 'Patriot str',
							['name'] = 'Senaki-Kolkhi Patriot/7',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281972.40625,
						},
						[8] = {
							['y'] = 645495.1875,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/8',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281910.25,
						},
						[9] = {
							['y'] = 645611.3125,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/9',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281728.78125,
						},
						[10] = {
							['y'] = 645558.9375,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/10',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281790.75,
						},
						[11] = {
							['y'] = 645443.6875,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/11',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281926.8125,
						},
						[12] = {
							['y'] = 645499.75,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/12',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281859.6875,
						},
						[13] = {
							['y'] = 645555.5625,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/13',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281840.8125,
						},
						[14] = {
							['y'] = 645609.125,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/14',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281778.34375,
						},
						[15] = {
							['y'] = 645445.125,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/15',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281970.46875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi Patriot',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647837.26922637,
							['x'] = -281988.86575445,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 1,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647837.25000001,
							['type'] = 'Tor 9A331',
							['name'] = 'Senaki-Kolkhi MR SAM/1',
							['heading'] = 5.2790847451229,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281988.87499987,
						},
						[2] = {
							['y'] = 646731.18750001,
							['type'] = 'Tor 9A331',
							['name'] = 'Senaki-Kolkhi MR SAM/2',
							['heading'] = 5.2790851740873,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281881.24999987,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Beslan'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Beslan AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 542,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 843393.91394619,
							['x'] = -149576.53829381,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 843393.9375001,
							['type'] = 'T-90',
							['name'] = 'Beslan AT/1',
							['heading'] = 123.6900684732,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -149576.53125007,
						},
						[2] = {
							['y'] = 843985.9375001,
							['type'] = 'T-90',
							['name'] = 'Beslan AT/2',
							['heading'] = 123.69006847326,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -149265.75000007,
						},
						[3] = {
							['y'] = 843741.6250001,
							['type'] = 'T-72B',
							['name'] = 'Beslan AT/3',
							['heading'] = 123.69006942152,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148248.26562507,
						},
						[4] = {
							['y'] = 841884.56249995,
							['type'] = 'BRDM-2',
							['name'] = 'Beslan AT/4',
							['heading'] = 123.69006752598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148741.24999997,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan S300'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 520,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 843719.82270088,
							['x'] = -147619.1865489,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 843719.81249999,
							['type'] = 'S-300PS 54K6 cp',
							['name'] = 'Beslan S300/1',
							['heading'] = 177.46334081417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147619.18749987,
						},
						[2] = {
							['y'] = 843924.31249999,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Beslan S300/2',
							['heading'] = 177.46334097329,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147790.62499987,
						},
						[3] = {
							['y'] = 844124.93749999,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Beslan S300/3',
							['heading'] = 177.46334127111,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147555.32812487,
						},
						[4] = {
							['y'] = 843592.625,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Beslan S300/4',
							['heading'] = 177.46334112712,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147755.90625,
						},
						[5] = {
							['y'] = 843908.9375,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Beslan S300/5',
							['heading'] = 177.46334134035,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147331.59375,
						},
						[6] = {
							['y'] = 843217.43749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/6',
							['heading'] = 177.46334093534,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147579.68749987,
						},
						[7] = {
							['y'] = 843171.24999999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/7',
							['heading'] = 177.46334078268,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147424.99999987,
						},
						[8] = {
							['y'] = 843071.93749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/8',
							['heading'] = 177.46334100304,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147498.87499987,
						},
						[9] = {
							['y'] = 843299.56249999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/9',
							['heading'] = 177.46334096565,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147486.81249987,
						},
						[10] = {
							['y'] = 843185.93749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/10',
							['heading'] = 177.46334137401,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147298.04687487,
						},
						[11] = {
							['y'] = 843344.37499999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/11',
							['heading'] = 177.46334078268,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147339.56249987,
						},
						[12] = {
							['y'] = 843267.43749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/12',
							['heading'] = 177.46334070851,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147198.23437487,
						},
						[13] = {
							['y'] = 843413.68749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/13',
							['heading'] = 177.46334093342,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147228.74999987,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan S300',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 525,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 842351.67722797,
							['x'] = -148616.448312,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 842351.68750008,
							['type'] = 'Tor 9A331',
							['name'] = 'Beslan MR SAM/1',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148616.45312509,
						},
						[2] = {
							['y'] = 844287.25000008,
							['type'] = 'Tor 9A331',
							['name'] = 'Beslan MR SAM/2',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148742.23437509,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan IR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 842892.44684132,
							['x'] = -149650.24568468,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 842892.4375,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/1',
							['heading'] = 110.55604402046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -149650.25,
						},
						[2] = {
							['y'] = 842884.0625,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/2',
							['heading'] = 110.55604242163,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148946.984375,
						},
						[3] = {
							['y'] = 844217.875,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/3',
							['heading'] = 110.55604481954,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -150037.796875,
						},
						[4] = {
							['y'] = 845155.25,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/4',
							['heading'] = 110.55604402028,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148559.390625,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan IR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 314,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 842172.56204603,
							['x'] = -148397.85487275,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 842172.5625,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Beslan AAA/1',
							['heading'] = 32.307333137882,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148397.859375,
						},
						[2] = {
							['y'] = 843818.75,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Beslan AAA/2',
							['heading'] = 32.307333648384,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148716.703125,
						},
						[3] = {
							['y'] = 844997.75,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Beslan AAA/3',
							['heading'] = 32.307334200185,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148757.5,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan AAA',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Kutaisi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Kutaisi SR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 683250.4008675,
							['x'] = -284044.29114327,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 683250.375,
							['type'] = 'M48 Chaparral',
							['name'] = 'Kutaisi SR SAM/1',
							['heading'] = 309.708263519,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284044.28125,
						},
						[2] = {
							['y'] = 684603.9375,
							['type'] = 'M48 Chaparral',
							['name'] = 'Kutaisi SR SAM/2',
							['heading'] = 309.70826406259,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -283340.78125,
						},
						[3] = {
							['y'] = 684867.3125,
							['type'] = 'M6 Linebacker',
							['name'] = 'Kutaisi SR SAM/3',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285236.40625,
						},
						[4] = {
							['y'] = 683342.31249996,
							['type'] = 'M1097 Avenger',
							['name'] = 'Kutaisi SR SAM/4',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285860.71874997,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi SR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 682851.34910483,
							['x'] = -285346.97345295,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 14,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 682851.375,
							['type'] = 'Vulcan',
							['name'] = 'Kutaisi AAA/1',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285346.96875,
						},
						[2] = {
							['y'] = 683908.125,
							['type'] = 'Vulcan',
							['name'] = 'Kutaisi AAA/2',
							['heading'] = 336.00001213196,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284690.15625,
						},
						[3] = {
							['y'] = 684987.625,
							['type'] = 'Vulcan',
							['name'] = 'Kutaisi AAA/3',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284706.40625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi AAA',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 684371.70282448,
							['x'] = -284977.54518433,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 1,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 684371.68750001,
							['type'] = 'Tor 9A331',
							['name'] = 'Kutaisi MR SAM/1',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284977.53124987,
						},
						[2] = {
							['y'] = 683604.87500001,
							['type'] = 'Tor 9A331',
							['name'] = 'Kutaisi MR SAM/2',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285123.09374987,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 684437.73758922,
							['x'] = -283217.05701419,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 15,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 684437.74999998,
							['type'] = 'M1045 HMMWV TOW',
							['name'] = 'Kutaisi AT/1',
							['heading'] = 332.525575585,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -283217.06249996,
						},
						[2] = {
							['y'] = 683719.74999994,
							['type'] = 'M-2 Bradley',
							['name'] = 'Kutaisi AT/2',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285572.81249989,
						},
						[3] = {
							['y'] = 682379.43749994,
							['type'] = 'M1134 Stryker ATGM',
							['name'] = 'Kutaisi AT/3',
							['heading'] = 332.52557310041,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284341.93749989,
						},
						[4] = {
							['y'] = 685159.1875,
							['type'] = 'M-1 Abrams',
							['name'] = 'Kutaisi AT/4',
							['heading'] = 332.52557467598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285176.1875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi Patriot'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 43,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 684332.48022606,
							['x'] = -286228.98414672,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 684332.5,
							['type'] = 'Patriot AMG',
							['name'] = 'Kutaisi Patriot/1',
							['heading'] = 36.999996905837,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286228.96875,
						},
						[2] = {
							['y'] = 684321.6875,
							['type'] = 'Patriot cp',
							['name'] = 'Kutaisi Patriot/2',
							['heading'] = 217.00000168852,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286242.34375,
						},
						[3] = {
							['y'] = 684255.8125,
							['type'] = 'Patriot AMG',
							['name'] = 'Kutaisi Patriot/3',
							['heading'] = 37.000002321912,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286220.59375,
						},
						[4] = {
							['y'] = 684245.375,
							['type'] = 'Patriot ECS',
							['name'] = 'Kutaisi Patriot/4',
							['heading'] = 217.0000071046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286234.90625,
						},
						[5] = {
							['y'] = 684338.5625,
							['type'] = 'Patriot EPP',
							['name'] = 'Kutaisi Patriot/5',
							['heading'] = 36.999996905837,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286244.65625,
						},
						[6] = {
							['y'] = 684262.5625,
							['type'] = 'Patriot EPP',
							['name'] = 'Kutaisi Patriot/6',
							['heading'] = 37.000002321912,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286238.1875,
						},
						[7] = {
							['y'] = 684227.375,
							['type'] = 'Patriot str',
							['name'] = 'Kutaisi Patriot/7',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286259.25,
						},
						[8] = {
							['y'] = 684188.375,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/8',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286475.28125,
						},
						[9] = {
							['y'] = 683999.25,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/9',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286372.125,
						},
						[10] = {
							['y'] = 684064.75,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/10',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286420.03125,
						},
						[11] = {
							['y'] = 684208.5,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/11',
							['heading'] = 216.9999971515,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286525.5,
						},
						[12] = {
							['y'] = 684137.625,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/12',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286474.25,
						},
						[13] = {
							['y'] = 684114.875,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/13',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286419.90625,
						},
						[14] = {
							['y'] = 684048.8125,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/14',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286370.84375,
						},
						[15] = {
							['y'] = 684251.9375,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/15',
							['heading'] = 216.9999971515,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286521.03125,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi Patriot',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Sukhumi-Babushara'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Sukhumi-Babushara MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 564843.28629619,
							['x'] = -220936.39352667,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 1,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 564843.31250001,
							['type'] = 'Tor 9A331',
							['name'] = 'Sukhumi-Babushara MR SAM/1',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220936.39062487,
						},
						[2] = {
							['y'] = 563773.31250001,
							['type'] = 'Tor 9A331',
							['name'] = 'Sukhumi-Babushara MR SAM/2',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220421.37499987,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara Patriot'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 565890.83936804,
							['x'] = -220712.3698011,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 565890.8125,
							['type'] = 'Patriot AMG',
							['name'] = 'Sukhumi-Babushara Patriot/1',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220712.375,
						},
						[2] = {
							['y'] = 565944.6875,
							['type'] = 'Patriot cp',
							['name'] = 'Sukhumi-Babushara Patriot/2',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220739.90625,
						},
						[3] = {
							['y'] = 565835.3125,
							['type'] = 'Patriot AMG',
							['name'] = 'Sukhumi-Babushara Patriot/3',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220788.34375,
						},
						[4] = {
							['y'] = 565878.1875,
							['type'] = 'Patriot ECS',
							['name'] = 'Sukhumi-Babushara Patriot/4',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220847.0625,
						},
						[5] = {
							['y'] = 565921.75,
							['type'] = 'Patriot EPP',
							['name'] = 'Sukhumi-Babushara Patriot/5',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220663.109375,
						},
						[6] = {
							['y'] = 565895.125,
							['type'] = 'Patriot EPP',
							['name'] = 'Sukhumi-Babushara Patriot/6',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220782.75,
						},
						[7] = {
							['y'] = 565993.5,
							['type'] = 'Patriot str',
							['name'] = 'Sukhumi-Babushara Patriot/7',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220821.046875,
						},
						[8] = {
							['y'] = 566126.5,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/8',
							['heading'] = 130.9999917103,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220873.296875,
						},
						[9] = {
							['y'] = 566010.375,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/9',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221054.78125,
						},
						[10] = {
							['y'] = 566062.75,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/10',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220992.796875,
						},
						[11] = {
							['y'] = 566178,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/11',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220856.734375,
						},
						[12] = {
							['y'] = 566121.9375,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/12',
							['heading'] = 130.9999917103,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220923.84375,
						},
						[13] = {
							['y'] = 566066.125,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/13',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220942.734375,
						},
						[14] = {
							['y'] = 566012.5625,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/14',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221005.21875,
						},
						[15] = {
							['y'] = 566176.5625,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/15',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220813.09375,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara Patriot',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara SR AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 565107.79717522,
							['x'] = -219411.09393482,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 15,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 565107.81249998,
							['type'] = 'M1045 HMMWV TOW',
							['name'] = 'Sukhumi-Babushara SR AT/1',
							['heading'] = 332.52557279747,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219411.09374996,
						},
						[2] = {
							['y'] = 564115.43749994,
							['type'] = 'M-2 Bradley',
							['name'] = 'Sukhumi-Babushara SR AT/2',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219401.73437489,
						},
						[3] = {
							['y'] = 563319.06249994,
							['type'] = 'M1134 Stryker ATGM',
							['name'] = 'Sukhumi-Babushara SR AT/3',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219553.18749989,
						},
						[4] = {
							['y'] = 565553.5625,
							['type'] = 'M-1 Abrams',
							['name'] = 'Sukhumi-Babushara SR AT/4',
							['heading'] = 332.52557467598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220355.625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara SR AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara SR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 563903.6117348,
							['x'] = -218983.047397,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 563903.625,
							['type'] = 'M48 Chaparral',
							['name'] = 'Sukhumi-Babushara SR SAM/1',
							['heading'] = 309.70826219731,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -218983.046875,
						},
						[2] = {
							['y'] = 563855.25,
							['type'] = 'M48 Chaparral',
							['name'] = 'Sukhumi-Babushara SR SAM/2',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221891.8125,
						},
						[3] = {
							['y'] = 565779.8125,
							['type'] = 'M6 Linebacker',
							['name'] = 'Sukhumi-Babushara SR SAM/3',
							['heading'] = 309.70825905646,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219311.234375,
						},
						[4] = {
							['y'] = 566293.93749996,
							['type'] = 'M1097 Avenger',
							['name'] = 'Sukhumi-Babushara SR SAM/4',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220325.23437497,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara SR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara SR AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 564301.44979463,
							['x'] = -220349.54349926,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 14,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 564301.4375,
							['type'] = 'Vulcan',
							['name'] = 'Sukhumi-Babushara SR AAA/1',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220349.546875,
						},
						[2] = {
							['y'] = 562663.375,
							['type'] = 'Vulcan',
							['name'] = 'Sukhumi-Babushara SR AAA/2',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219887.859375,
						},
						[3] = {
							['y'] = 565867.75,
							['type'] = 'Vulcan',
							['name'] = 'Sukhumi-Babushara SR AAA/3',
							['heading'] = 336.00001036344,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221471.421875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara SR AAA',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Nalchik'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Nalchik AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 314,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 759176.60934685,
							['x'] = -125758.11683172,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 759176.625,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Nalchik AAA/1',
							['heading'] = 32.307333818172,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125758.1171875,
						},
						[2] = {
							['y'] = 760478.875,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Nalchik AAA/2',
							['heading'] = 32.307333648384,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124636.046875,
						},
						[3] = {
							['y'] = 761578,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Nalchik AAA/3',
							['heading'] = 32.307332926778,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123885.6640625,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik AAA',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 519,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 760197.13435023,
							['x'] = -125210.5637512,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 760197.12500008,
							['type'] = 'Tor 9A331',
							['name'] = 'Nalchik MR SAM/1',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125210.56250009,
						},
						[2] = {
							['y'] = 761082.06250008,
							['type'] = 'Tor 9A331',
							['name'] = 'Nalchik MR SAM/2',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124626.25000009,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 542,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 760201.89537913,
							['x'] = -125702.97325764,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 760201.8750001,
							['type'] = 'T-90',
							['name'] = 'Nalchik AT/1',
							['heading'] = 123.69006942033,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125702.97656257,
						},
						[2] = {
							['y'] = 761823.8125001,
							['type'] = 'T-90',
							['name'] = 'Nalchik AT/2',
							['heading'] = 123.69007131469,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124125.98437507,
						},
						[3] = {
							['y'] = 760779.4375001,
							['type'] = 'T-72B',
							['name'] = 'Nalchik AT/3',
							['heading'] = 123.69007036751,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123607.24218757,
						},
						[4] = {
							['y'] = 759074.49999995,
							['type'] = 'BRDM-2',
							['name'] = 'Nalchik AT/4',
							['heading'] = 123.69006847316,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125547.35156247,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik IR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 759640.77376426,
							['x'] = -124981.50469322,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 759640.75,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/1',
							['heading'] = 110.55604561929,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124981.5078125,
						},
						[2] = {
							['y'] = 760160.8125,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/2',
							['heading'] = 110.55604481988,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123336.609375,
						},
						[3] = {
							['y'] = 760963.375,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/3',
							['heading'] = 110.55604402046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125433.75,
						},
						[4] = {
							['y'] = 761185.9375,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/4',
							['heading'] = 110.55604561929,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124084.375,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik IR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik S300'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 759906.76150032,
							['x'] = -124109.86039959,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 759906.74999994,
							['type'] = 'S-300PS 54K6 cp',
							['name'] = 'Nalchik S300/1',
							['heading'] = 25.463346456722,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124109.85937512,
						},
						[2] = {
							['y'] = 759920.24999994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Nalchik S300/2',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123954.89062512,
						},
						[3] = {
							['y'] = 759669.81249994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Nalchik S300/3',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124106.17187512,
						},
						[4] = {
							['y'] = 760026.5,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Nalchik S300/4',
							['heading'] = 25.463344841632,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124020.0078125,
						},
						[5] = {
							['y'] = 759671.8125,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Nalchik S300/5',
							['heading'] = 25.463344841632,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124205.3671875,
						},
						[6] = {
							['y'] = 760308.06249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/6',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124409.35937512,
						},
						[7] = {
							['y'] = 760274.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/7',
							['heading'] = 25.463344988458,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124495.90625012,
						},
						[8] = {
							['y'] = 760372.62499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/8',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124481.23437512,
						},
						[9] = {
							['y'] = 760215.68749994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/9',
							['heading'] = 25.463346309896,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124424.03125012,
						},
						[10] = {
							['y'] = 760227.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/10',
							['heading'] = 25.463346897203,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124644.06250012,
						},
						[11] = {
							['y'] = 760139.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/11',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124556.04687512,
						},
						[12] = {
							['y'] = 760108.56249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/12',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124693.93750012,
						},
						[13] = {
							['y'] = 760068.99999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/13',
							['heading'] = 25.463346309896,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124611.78906262,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik S300',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Mineralnye Vody'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Mineralnye Vody AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 542,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 707000.31182306,
							['x'] = -53097.930409503,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 707000.3125001,
							['type'] = 'T-90',
							['name'] = 'Mineralnye Vody AT/1',
							['heading'] = 123.69006942033,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -53097.929687567,
						},
						[2] = {
							['y'] = 707378.0000001,
							['type'] = 'T-90',
							['name'] = 'Mineralnye Vody AT/2',
							['heading'] = 123.6900751034,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50844.156250067,
						},
						[3] = {
							['y'] = 705785.1250001,
							['type'] = 'T-72B',
							['name'] = 'Mineralnye Vody AT/3',
							['heading'] = 123.69006942033,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -53037.027343817,
						},
						[4] = {
							['y'] = 703510.68749995,
							['type'] = 'BRDM-2',
							['name'] = 'Mineralnye Vody AT/4',
							['heading'] = 123.69007036751,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51446.812499967,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody  MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 519,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 703975.2341117,
							['x'] = -51028.504921144,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 703975.25000008,
							['type'] = 'Tor 9A331',
							['name'] = 'Mineralnye Vody  MR SAM/1',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51028.503906343,
						},
						[2] = {
							['y'] = 707719.25000008,
							['type'] = 'Tor 9A331',
							['name'] = 'Mineralnye Vody  MR SAM/2',
							['heading'] = 137.72631145337,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -52393.011718843,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody  MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody S300'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 320,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 705992.64543785,
							['x'] = -50890.11999127,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 705992.62499994,
							['type'] = 'S-300PS 54K6 cp',
							['name'] = 'Mineralnye Vody S300/1',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50890.121093633,
						},
						[2] = {
							['y'] = 706115.49999994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Mineralnye Vody S300/2',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51080.894531133,
						},
						[3] = {
							['y'] = 706206.93749994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Mineralnye Vody S300/3',
							['heading'] = 151.99999953786,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50738.546874883,
						},
						[4] = {
							['y'] = 705856.75,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Mineralnye Vody S300/4',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51083.5078125,
						},
						[5] = {
							['y'] = 706008.3125,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Mineralnye Vody S300/5',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50568.67578125,
						},
						[6] = {
							['y'] = 705268.74999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/6',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50688.890624883,
						},
						[7] = {
							['y'] = 705401.99999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/7',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50843.078124883,
						},
						[8] = {
							['y'] = 705034.74999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/8',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50652.105468633,
						},
						[9] = {
							['y'] = 705582.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/9',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50793.425781133,
						},
						[10] = {
							['y'] = 705224.31249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/10',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50424.941406133,
						},
						[11] = {
							['y'] = 705425.56249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/11',
							['heading'] = 151.99999693267,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50351.769531133,
						},
						[12] = {
							['y'] = 705490.87499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/12',
							['heading'] = 151.99999693267,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50511.183593633,
						},
						[13] = {
							['y'] = 705663.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/13',
							['heading'] = 151.99999815442,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50453.687499883,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody S300',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody IR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 703616.49370942,
							['x'] = -51222.314386361,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 703616.5,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/1',
							['heading'] = 110.55604481988,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51222.3125,
						},
						[2] = {
							['y'] = 704601.875,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/2',
							['heading'] = 110.55604561929,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -49784.48828125,
						},
						[3] = {
							['y'] = 706599.3125,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/3',
							['heading'] = 110.55604442015,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -52496.3203125,
						},
						[4] = {
							['y'] = 707661.75,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/4',
							['heading'] = 110.55604402046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51594.44921875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody IR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 314,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 703646.77038064,
							['x'] = -50362.327171642,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 703646.75,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Mineralnye Vody AAA/1',
							['heading'] = 32.307339125294,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50362.328125,
						},
						[2] = {
							['y'] = 705682.1875,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Mineralnye Vody AAA/2',
							['heading'] = 32.3073350068,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -52445.609375,
						},
						[3] = {
							['y'] = 707979,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Mineralnye Vody AAA/3',
							['heading'] = 32.307333223845,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51889.82421875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody AAA',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['FARP'] = {
		['FARP Balkariya'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Inguri Valley'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Tsvirmi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Nigniy'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Elhotovo'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Tkvarcheli'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Progress'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Oche'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Elbrus'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Kodori'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Bylym'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Derchi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Makara'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Lenteni'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Oni'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Zugdidi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['Communication'] = {
		['Antenna Inguri'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna East Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Zugdidi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Oni'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Ambrolauri'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna 2'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Tkvarcheli'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna West Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Zvirmi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Elbrus'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Oche'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Balkariya'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Kodori'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Bulls'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['Main Targets'] = {
		['Inguri-Dam'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['Bunker'] = {
		['East Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['West Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['properties'] = {
		['serverName'] = '- DEADBEEFS LAIR -',
		['restartTime'] = 0,
		['ejectedPilots'] = {
		},
		['playerLimit'] = {
		},
		['startTimer'] = 0,
		['campaignSecs'] = 0,
		['droppedCrates'] = {
		},
		['droppedSAMs'] = {
		},
		['lifeLimit'] = 5,
		['numSpawnedConvoys'] = {
			['blue'] = 0,
			['red'] = 0,
		},
		['timer'] = 0,
		['spawnedConvoys'] = {
		},
		['cashPile'] = {
		},
		['missionRuntime'] = 0,
		['convoys'] = {
			['blue'] = {
				['Beslan'] = {
					['units'] = {
						[2] = {
							['y'] = 853181.44277847,
							['x'] = -167063.59233411,
							['heading'] = 3.517356019478,
						},
						[3] = {
							['y'] = 853179.69255781,
							['x'] = -167093.52152347,
							['heading'] = 4.6775914985762,
						},
						[1] = {
							['y'] = 853183.16864601,
							['x'] = -167033.71153391,
							['heading'] = 352.06530692305,
						},
						[4] = {
							['y'] = 853177.8789492,
							['x'] = -167123.47421609,
							['heading'] = 4.5975855170159,
						},
						[5] = {
							['y'] = 853176.12914591,
							['x'] = -167153.42018216,
							['heading'] = 4.5492574353341,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 853183.88793385,
							['x'] = -167035.94584686,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 843499.0625,
							['x'] = -149568.484375,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Mineralnye Vody'] = {
					['units'] = {
						[2] = {
							['y'] = 707304.08847543,
							['x'] = -73060.042526707,
							['heading'] = 33.159614198503,
						},
						[3] = {
							['y'] = 707278.65593689,
							['x'] = -73075.933370628,
							['heading'] = 33.159614198503,
						},
						[1] = {
							['y'] = 707329.35853428,
							['x'] = -73044.121070411,
							['heading'] = 21.84863715738,
						},
						[4] = {
							['y'] = 707253.24348258,
							['x'] = -73091.804397058,
							['heading'] = 34.48750305245,
						},
						[5] = {
							['y'] = 707227.79251384,
							['x'] = -73107.735964433,
							['heading'] = 33.1596123305,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 707328.89467602,
							['x'] = -73046.489001125,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 707150.00541469,
							['x'] = -52664.65359574,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Nalchik'] = {
					['units'] = {
						[2] = {
							['y'] = 745807.27341675,
							['x'] = -109159.96309067,
							['heading'] = 128.63050373005,
						},
						[3] = {
							['y'] = 745778.08746878,
							['x'] = -109167.02781023,
							['heading'] = 129.05473567917,
						},
						[1] = {
							['y'] = 745837.00927283,
							['x'] = -109153.16915311,
							['heading'] = 114.82498279797,
						},
						[4] = {
							['y'] = 745748.90800117,
							['x'] = -109174.08104042,
							['heading'] = 128.74274925838,
						},
						[5] = {
							['y'] = 745719.72328852,
							['x'] = -109181.14419264,
							['heading'] = 128.63446401085,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 745835.14510155,
							['x'] = -109151.62745825,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 760405.875,
							['x'] = -124761.375,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Senaki-Kolkhi'] = {
					['units'] = {
						[2] = {
							['y'] = 665098.96247274,
							['x'] = -286629.90957783,
							['heading'] = 301.46854286162,
						},
						[3] = {
							['y'] = 665121.1125619,
							['x'] = -286650.23731857,
							['heading'] = 301.33783236746,
						},
						[1] = {
							['y'] = 665076.93189571,
							['x'] = -286609.64743743,
							['heading'] = 301.45746480854,
						},
						[4] = {
							['y'] = 665143.13570076,
							['x'] = -286670.52597655,
							['heading'] = 301.41465333239,
						},
						[5] = {
							['y'] = 665165.19582778,
							['x'] = -286690.80579557,
							['heading'] = 301.40776664316,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 665078.17461514,
							['x'] = -286610.17646482,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 646650.9375,
							['x'] = -281127.84375,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
			},
			['red'] = {
				['Beslan'] = {
					['units'] = {
						[2] = {
							['y'] = 822960.88653647,
							['x'] = -144261.91155051,
							['heading'] = 104.89780306179,
						},
						[3] = {
							['y'] = 822931.06620475,
							['x'] = -144265.62727686,
							['heading'] = 104.70808917228,
						},
						[1] = {
							['y'] = 822990.76987316,
							['x'] = -144258.78879068,
							['heading'] = 104.93910090094,
						},
						[4] = {
							['y'] = 822901.2355541,
							['x'] = -144269.3405828,
							['heading'] = 104.5577187455,
						},
						[5] = {
							['y'] = 822871.50115747,
							['x'] = -144273.07897724,
							['heading'] = 104.51140918806,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 822990.54441832,
							['x'] = -144258.73188681,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 843499.0625,
							['x'] = -149568.484375,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
				},
				['Mineralnye Vody'] = {
					['units'] = {
						[2] = {
							['y'] = 726673.02462577,
							['x'] = -47984.764961293,
							['heading'] = 263.05685586119,
						},
						[3] = {
							['y'] = 726702.25472347,
							['x'] = -47991.712624067,
							['heading'] = 263.18618980169,
						},
						[1] = {
							['y'] = 726643.76734296,
							['x'] = -47978.179910675,
							['heading'] = 263.05685586119,
						},
						[4] = {
							['y'] = 726731.39112908,
							['x'] = -47999.004277248,
							['heading'] = 263.05685586119,
						},
						[5] = {
							['y'] = 726760.3899152,
							['x'] = -48006.637237572,
							['heading'] = 263.05685586119,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 726644.61541748,
							['x'] = -47978.044921875,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 707080.22792755,
							['x'] = -51567.133858085,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
				},
				['Nalchik'] = {
					['units'] = {
						[2] = {
							['y'] = 778140.69900446,
							['x'] = -128232.53010352,
							['heading'] = 284.77808371767,
						},
						[3] = {
							['y'] = 778163.76390292,
							['x'] = -128251.74948624,
							['heading'] = 284.77808371767,
						},
						[1] = {
							['y'] = 778117.6257105,
							['x'] = -128213.33194355,
							['heading'] = 284.77808371767,
						},
						[4] = {
							['y'] = 778186.82040632,
							['x'] = -128270.96665432,
							['heading'] = 284.77808371767,
						},
						[5] = {
							['y'] = 778209.88290632,
							['x'] = -128290.18540432,
							['heading'] = 284.77808371767,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 778118.51159538,
							['x'] = -128213.52939828,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 760352.23693848,
							['x'] = -124696.81885529,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
				},
				['Senaki-Kolkhi'] = {
					['units'] = {
						[2] = {
							['y'] = 627678.56757113,
							['x'] = -269789.09922862,
							['heading'] = 121.79042329823,
						},
						[3] = {
							['y'] = 627653.05371921,
							['x'] = -269773.34044392,
							['heading'] = 121.68416039645,
						},
						[1] = {
							['y'] = 627703.86761888,
							['x'] = -269805.15403882,
							['heading'] = 121.77362640904,
						},
						[4] = {
							['y'] = 627627.33715401,
							['x'] = -269757.85339636,
							['heading'] = 121.44016365917,
						},
						[5] = {
							['y'] = 627601.53105112,
							['x'] = -269742.47416664,
							['heading'] = 120.81687555956,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 20,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 627703.56091309,
							['x'] = -269804.98339844,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
						[2] = {
							['alt'] = 41,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 646650.9375,
							['x'] = -281127.84375,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
				},
			},
		},
	},
}
return t