{
	['restartTime'] = 41,
	['ejectedPilots'] = {
		[1] = {
			['visible'] = false,
			['side'] = 2,
			['country'] = 2,
			['playerName'] = 'Endmonster',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 673582.49506596,
					['x'] = -176534.52162428,
					['name'] = 'Wounded Pilot #403',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Soldier M4',
				},
			},
			['name'] = 'Endmonster Ejected AV8BNA #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 300,
				['massEmpty'] = 6715,
				['tankerType'] = 1,
				['range'] = 2200,
				['box'] = {
					['min'] = {
						['y'] = -2.0454246997833,
						['x'] = -7.5782270431519,
						['z'] = -4.5954065322876,
					},
					['max'] = {
						['y'] = 2.4198503494263,
						['x'] = 6.7410807609558,
						['z'] = 4.5801057815552,
					},
				},
				['Hmax'] = 16764,
				['Kmax'] = 0.69999998807907,
				['_origin'] = 'AV-8B N/A AI by RAZBAM Sims',
				['speedMax10K'] = 275,
				['NyMin'] = -2,
				['fuelMassMax'] = 3519.4230957031,
				['speedMax'] = 300,
				['NyMax'] = 7,
				['massMax'] = 14100,
				['RCS'] = 5,
				['displayName'] = 'AV8BNA',
				['life'] = 18,
				['category'] = 0,
				['Kab'] = 0,
				['attributes'] = {
					['Air'] = true,
					['Refuelable'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['NonArmoredUnits'] = true,
					['Bombers'] = true,
					['All'] = true,
					['Datalink'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'AV8BNA',
				['VyMax'] = 150,
			},
			['task'] = {
			},
			['spawnTime'] = 308810,
		},
		[2] = {
			['visible'] = false,
			['side'] = 1,
			['country'] = 0,
			['playerName'] = 'VORTEX 1-4 | TAW_Korenaga',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 673478.96182063,
					['x'] = -211659.31246841,
					['name'] = 'Wounded Pilot #403',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Infantry AK',
				},
			},
			['name'] = 'VORTEX 1-4 | TAW_Korenaga Ejected Ka-50 #2',
			['category'] = 2,
			['desc'] = {
				['massEmpty'] = 8030,
				['range'] = 450,
				['box'] = {
					['min'] = {
						['y'] = -1.1678450107574,
						['x'] = -8.1102180480957,
						['z'] = -6.7696857452393,
					},
					['max'] = {
						['y'] = 3.6971831321716,
						['x'] = 8.1102180480957,
						['z'] = 6.7696857452393,
					},
				},
				['Hmax'] = 6400,
				['Kmax'] = 0.30000001192093,
				['_origin'] = '',
				['NyMin'] = 0.5,
				['fuelMassMax'] = 1450,
				['speedMax'] = 350,
				['NyMax'] = 3.5,
				['massMax'] = 11900,
				['RCS'] = 5,
				['displayName'] = 'ka-50',
				['life'] = 15,
				['category'] = 1,
				['VyMax'] = 14.60000038147,
				['attributes'] = {
					['Air'] = true,
					['NonAndLightArmoredUnits'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Helicopters'] = true,
					['Attack helicopters'] = true,
				},
				['typeName'] = 'Ka-50',
				['HmaxStat'] = 4000,
			},
			['task'] = {
			},
			['spawnTime'] = 351152,
		},
		[3] = {
			['visible'] = false,
			['side'] = 1,
			['country'] = 0,
			['playerName'] = 'DAGGER 1-1 | TAW_Dugo',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 679701.28152755,
					['x'] = -203286.66780545,
					['name'] = 'Wounded Pilot #404',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Infantry AK',
				},
			},
			['name'] = 'DAGGER 1-1 | TAW_Dugo Ejected AJS37 #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 408,
				['massEmpty'] = 10749,
				['range'] = 2000,
				['box'] = {
					['min'] = {
						['y'] = -1.2390263080597,
						['x'] = -14.131652832031,
						['z'] = -5.2076072692871,
					},
					['max'] = {
						['y'] = 3.5302677154541,
						['x'] = 9.458854675293,
						['z'] = 5.2099537849426,
					},
				},
				['Hmax'] = 21000,
				['Kmax'] = 0.62000000476837,
				['_origin'] = 'AJS37 AI by Heatblur Simulations',
				['speedMax10K'] = 612,
				['NyMin'] = -2,
				['fuelMassMax'] = 4476,
				['speedMax'] = 612,
				['NyMax'] = 8,
				['massMax'] = 20000,
				['RCS'] = 3,
				['displayName'] = 'AJS37',
				['life'] = 18,
				['category'] = 0,
				['Kab'] = 2,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['Battleplanes'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'AJS37',
				['VyMax'] = 200,
			},
			['task'] = {
			},
			['spawnTime'] = 352721,
		},
		[4] = {
			['visible'] = false,
			['side'] = 2,
			['country'] = 2,
			['playerName'] = 'ULTRA 01',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 608704.12423814,
					['x'] = -182254.50311199,
					['name'] = 'Wounded Pilot #405',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Soldier M4',
				},
			},
			['name'] = 'ULTRA 01 Ejected F-15C #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 403,
				['massEmpty'] = 13380,
				['tankerType'] = 0,
				['range'] = 2540,
				['box'] = {
					['min'] = {
						['y'] = -1.0274813175201,
						['x'] = -8.578462600708,
						['z'] = -6.7097973823547,
					},
					['max'] = {
						['y'] = 3.5019924640656,
						['x'] = 11.160133361816,
						['z'] = 6.6517648696899,
					},
				},
				['Hmax'] = 18300,
				['Kmax'] = 0.91000002622604,
				['_origin'] = '',
				['speedMax10K'] = 736.10998535156,
				['NyMin'] = -3,
				['fuelMassMax'] = 6103,
				['speedMax'] = 736.10998535156,
				['NyMax'] = 8,
				['massMax'] = 30845,
				['RCS'] = 5,
				['displayName'] = 'F-15C',
				['life'] = 20,
				['category'] = 0,
				['Kab'] = 4,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['NonArmoredUnits'] = true,
					['Refuelable'] = true,
					['All'] = true,
					['Planes'] = true,
					['Fighters'] = true,
				},
				['typeName'] = 'F-15C',
				['VyMax'] = 275,
			},
			['task'] = {
			},
			['spawnTime'] = 353525,
		},
		[5] = {
			['visible'] = false,
			['side'] = 1,
			['country'] = 0,
			['playerName'] = 'SPIDER 1-4 | TAW_Badjoe',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 714302.46816185,
					['x'] = -200757.09189728,
					['name'] = 'Wounded Pilot #406',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Infantry AK',
				},
			},
			['name'] = 'SPIDER 1-4 | TAW_Badjoe Ejected AJS37 #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 408,
				['massEmpty'] = 10749,
				['range'] = 2000,
				['box'] = {
					['min'] = {
						['y'] = -1.2390263080597,
						['x'] = -14.131652832031,
						['z'] = -5.2076072692871,
					},
					['max'] = {
						['y'] = 3.5302677154541,
						['x'] = 9.458854675293,
						['z'] = 5.2099537849426,
					},
				},
				['Hmax'] = 21000,
				['Kmax'] = 0.62000000476837,
				['_origin'] = 'AJS37 AI by Heatblur Simulations',
				['speedMax10K'] = 612,
				['NyMin'] = -2,
				['fuelMassMax'] = 4476,
				['speedMax'] = 612,
				['NyMax'] = 8,
				['massMax'] = 20000,
				['RCS'] = 3,
				['displayName'] = 'AJS37',
				['life'] = 18,
				['category'] = 0,
				['Kab'] = 2,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['Battleplanes'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'AJS37',
				['VyMax'] = 200,
			},
			['task'] = {
			},
			['spawnTime'] = 356596,
		},
		[6] = {
			['visible'] = false,
			['side'] = 1,
			['country'] = 0,
			['playerName'] = 'SPIDER 1-4 | TAW_Badjoe',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 570159.86303724,
					['x'] = -206575.70525617,
					['name'] = 'Wounded Pilot #407',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Infantry AK',
				},
			},
			['name'] = 'SPIDER 1-4 | TAW_Badjoe Ejected AJS37 #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 408,
				['massEmpty'] = 10749,
				['range'] = 2000,
				['box'] = {
					['min'] = {
						['y'] = -1.2390263080597,
						['x'] = -14.131652832031,
						['z'] = -5.2076072692871,
					},
					['max'] = {
						['y'] = 3.5302677154541,
						['x'] = 9.458854675293,
						['z'] = 5.2099537849426,
					},
				},
				['Hmax'] = 21000,
				['Kmax'] = 0.62000000476837,
				['_origin'] = 'AJS37 AI by Heatblur Simulations',
				['speedMax10K'] = 612,
				['NyMin'] = -2,
				['fuelMassMax'] = 4476,
				['speedMax'] = 612,
				['NyMax'] = 8,
				['massMax'] = 20000,
				['RCS'] = 3,
				['displayName'] = 'AJS37',
				['life'] = 18,
				['category'] = 0,
				['Kab'] = 2,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['Battleplanes'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'AJS37',
				['VyMax'] = 200,
			},
			['task'] = {
			},
			['spawnTime'] = 361299,
		},
		[7] = {
			['visible'] = false,
			['side'] = 2,
			['country'] = 16,
			['playerName'] = 'Archangel',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 586437.19139327,
					['x'] = -207192.02301707,
					['name'] = 'Wounded Pilot #403',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Soldier M4',
				},
			},
			['name'] = 'Archangel Ejected Su-25T #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 292,
				['massEmpty'] = 11496.400390625,
				['range'] = 2250,
				['box'] = {
					['min'] = {
						['y'] = -2.304071187973,
						['x'] = -7.2611885070801,
						['z'] = -7.223217010498,
					},
					['max'] = {
						['y'] = 3.7840712070465,
						['x'] = 7.7011885643005,
						['z'] = 7.223217010498,
					},
				},
				['Hmax'] = 10000,
				['Kmax'] = 0.69999998807907,
				['_origin'] = '',
				['speedMax10K'] = 271,
				['NyMin'] = -3,
				['fuelMassMax'] = 3790,
				['speedMax'] = 292,
				['NyMax'] = 5.9000000953674,
				['massMax'] = 19500,
				['RCS'] = 7,
				['displayName'] = 'su-25T',
				['life'] = 32,
				['category'] = 0,
				['Kab'] = 0,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['Battleplanes'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'Su-25T',
				['VyMax'] = 60,
			},
			['task'] = {
			},
			['spawnTime'] = 440224,
		},
		[8] = {
			['visible'] = false,
			['side'] = 2,
			['country'] = 2,
			['playerName'] = 'Archangel.',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 623920.77377522,
					['x'] = -203863.62564584,
					['name'] = 'Wounded Pilot #403',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Soldier M4',
				},
			},
			['name'] = 'Archangel. Ejected Ka-50 #2',
			['category'] = 2,
			['desc'] = {
				['massEmpty'] = 8030,
				['range'] = 450,
				['box'] = {
					['min'] = {
						['y'] = -1.1678450107574,
						['x'] = -8.1102180480957,
						['z'] = -6.7696857452393,
					},
					['max'] = {
						['y'] = 3.6971831321716,
						['x'] = 8.1102180480957,
						['z'] = 6.7696857452393,
					},
				},
				['Hmax'] = 6400,
				['Kmax'] = 0.30000001192093,
				['_origin'] = '',
				['NyMin'] = 0.5,
				['fuelMassMax'] = 1450,
				['speedMax'] = 350,
				['NyMax'] = 3.5,
				['massMax'] = 11900,
				['RCS'] = 5,
				['displayName'] = 'ka-50',
				['life'] = 15,
				['category'] = 1,
				['VyMax'] = 14.60000038147,
				['attributes'] = {
					['Air'] = true,
					['NonAndLightArmoredUnits'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Helicopters'] = true,
					['Attack helicopters'] = true,
				},
				['typeName'] = 'Ka-50',
				['HmaxStat'] = 4000,
			},
			['task'] = {
			},
			['spawnTime'] = 451168,
		},
		[9] = {
			['visible'] = false,
			['side'] = 2,
			['country'] = 16,
			['playerName'] = 'Archangel.',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 605215.18631278,
					['x'] = -170340.59631173,
					['name'] = 'Wounded Pilot #404',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Soldier M4',
				},
			},
			['name'] = 'Archangel. Ejected Su-25T #2',
			['category'] = 2,
			['desc'] = {
				['speedMax0'] = 292,
				['massEmpty'] = 11496.400390625,
				['range'] = 2250,
				['box'] = {
					['min'] = {
						['y'] = -2.304071187973,
						['x'] = -7.2611885070801,
						['z'] = -7.223217010498,
					},
					['max'] = {
						['y'] = 3.7840712070465,
						['x'] = 7.7011885643005,
						['z'] = 7.223217010498,
					},
				},
				['Hmax'] = 10000,
				['Kmax'] = 0.69999998807907,
				['_origin'] = '',
				['speedMax10K'] = 271,
				['NyMin'] = -3,
				['fuelMassMax'] = 3790,
				['speedMax'] = 292,
				['NyMax'] = 5.9000000953674,
				['massMax'] = 19500,
				['RCS'] = 7,
				['displayName'] = 'su-25T',
				['life'] = 32,
				['category'] = 0,
				['Kab'] = 0,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['Battleplanes'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'Su-25T',
				['VyMax'] = 60,
			},
			['task'] = {
			},
			['spawnTime'] = 457991,
		},
		[10] = {
			['visible'] = false,
			['side'] = 2,
			['playerName'] = 'Archangel',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 672828.79081389,
					['type'] = 'Soldier M4',
					['name'] = 'Wounded Pilot #403',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['x'] = -167658.79538467,
				},
			},
			['name'] = 'Archangel Ejected Ka-50 #2',
			['category'] = 2,
			['country'] = 2,
			['desc'] = {
				['massEmpty'] = 8030,
				['range'] = 450,
				['box'] = {
					['min'] = {
						['y'] = -1.1678450107574,
						['x'] = -8.1102180480957,
						['z'] = -6.7696857452393,
					},
					['max'] = {
						['y'] = 3.6971831321716,
						['x'] = 8.1102180480957,
						['z'] = 6.7696857452393,
					},
				},
				['Hmax'] = 6400,
				['Kmax'] = 0.30000001192093,
				['_origin'] = '',
				['NyMin'] = 0.5,
				['fuelMassMax'] = 1450,
				['speedMax'] = 350,
				['NyMax'] = 3.5,
				['massMax'] = 11900,
				['RCS'] = 5,
				['displayName'] = 'ka-50',
				['life'] = 15,
				['category'] = 1,
				['VyMax'] = 14.60000038147,
				['attributes'] = {
					['Air'] = true,
					['NonAndLightArmoredUnits'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Helicopters'] = true,
					['Attack helicopters'] = true,
				},
				['typeName'] = 'Ka-50',
				['HmaxStat'] = 4000,
			},
			['task'] = {
			},
			['spawnTime'] = 469454,
		},
		[11] = {
			['visible'] = false,
			['side'] = 2,
			['playerName'] = 'Archangel',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 670819.21819013,
					['type'] = 'Soldier M4',
					['name'] = 'Wounded Pilot #404',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['x'] = -197923.3131885,
				},
			},
			['name'] = 'Archangel Ejected Su-25T #2',
			['category'] = 2,
			['country'] = 16,
			['desc'] = {
				['speedMax0'] = 292,
				['massEmpty'] = 11496.400390625,
				['range'] = 2250,
				['box'] = {
					['min'] = {
						['y'] = -2.304071187973,
						['x'] = -7.2611885070801,
						['z'] = -7.223217010498,
					},
					['max'] = {
						['y'] = 3.7840712070465,
						['x'] = 7.7011885643005,
						['z'] = 7.223217010498,
					},
				},
				['Hmax'] = 10000,
				['Kmax'] = 0.69999998807907,
				['_origin'] = '',
				['speedMax10K'] = 271,
				['NyMin'] = -3,
				['fuelMassMax'] = 3790,
				['speedMax'] = 292,
				['NyMax'] = 5.9000000953674,
				['massMax'] = 19500,
				['RCS'] = 7,
				['displayName'] = 'su-25T',
				['life'] = 32,
				['category'] = 0,
				['Kab'] = 0,
				['attributes'] = {
					['Air'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['Battleplanes'] = true,
					['NonArmoredUnits'] = true,
					['All'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'Su-25T',
				['VyMax'] = 60,
			},
			['task'] = {
			},
			['spawnTime'] = 472152,
		},
		[12] = {
			['visible'] = false,
			['side'] = 2,
			['playerName'] = '0xDEADBEEF',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 680550.18831137,
					['type'] = 'Soldier M4',
					['name'] = 'Wounded Pilot #405',
					['heading'] = 120,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['x'] = -204272.74324045,
				},
			},
			['name'] = '0xDEADBEEF Ejected AV8BNA #2',
			['category'] = 2,
			['country'] = 2,
			['desc'] = {
				['speedMax0'] = 300,
				['massEmpty'] = 6715,
				['tankerType'] = 1,
				['range'] = 2200,
				['box'] = {
					['min'] = {
						['y'] = -2.0454246997833,
						['x'] = -7.5782270431519,
						['z'] = -4.5954065322876,
					},
					['max'] = {
						['y'] = 2.4198503494263,
						['x'] = 6.7410807609558,
						['z'] = 4.5801057815552,
					},
				},
				['Hmax'] = 16764,
				['Kmax'] = 0.69999998807907,
				['_origin'] = 'AV-8B N/A AI by RAZBAM Sims',
				['speedMax10K'] = 275,
				['NyMin'] = -2,
				['fuelMassMax'] = 3519.4230957031,
				['speedMax'] = 300,
				['NyMax'] = 7,
				['massMax'] = 14100,
				['RCS'] = 5,
				['displayName'] = 'AV8BNA',
				['life'] = 18,
				['category'] = 0,
				['Kab'] = 0,
				['attributes'] = {
					['Air'] = true,
					['Refuelable'] = true,
					['Battle airplanes'] = true,
					['NonAndLightArmoredUnits'] = true,
					['NonArmoredUnits'] = true,
					['Bombers'] = true,
					['All'] = true,
					['Datalink'] = true,
					['Planes'] = true,
				},
				['typeName'] = 'AV8BNA',
				['VyMax'] = 150,
			},
			['task'] = {
			},
			['spawnTime'] = 473626,
		},
	},
	['playerLimit'] = {
		['Archangel'] = 5,
		['0xDEADBEEF'] = 1,
		['TAW_Adder'] = 0,
	},
	['startTimer'] = 9.001,
	['campaignSecs'] = 475574,
	['droppedSAMs'] = {
		[2] = {
			['playerCategory'] = 'HELICOPTER',
			['groupSpawnTable'] = {
				['visible'] = false,
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[1] = {
						['y'] = 599026.02400432,
						['x'] = -219836.35791349,
						['name'] = 'Unpacked SA-18 Igla manpad #11',
						['heading'] = 120,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['type'] = 'SA-18 Igla manpad',
					},
				},
				['coalition'] = 'red',
				['name'] = 'VORTEX 1-1 TAW_Koksys Ural-375 #67',
				['category'] = 2,
				['task'] = {
				},
			},
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['groupName'] = 'VORTEX 1-1 TAW_Koksys Ural-375 #67',
			['coalition'] = 1,
			['time'] = 347948,
			['playerUCID'] = '8e872bdbe8fb7ba1cfb5fd0da6b3f392',
			['playerType'] = 'Mi-8MT',
			['groupID'] = 1000724,
			['spawnedGroup'] = {
				['id_'] = 1000070,
			},
		},
		[3] = {
			['playerCategory'] = 'HELICOPTER',
			['groupSpawnTable'] = {
				['visible'] = false,
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[1] = {
						['y'] = 606096.58650432,
						['x'] = -228200.52978849,
						['name'] = 'Unpacked SA-18 Igla manpad #16',
						['heading'] = 120,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['type'] = 'SA-18 Igla manpad',
					},
				},
				['coalition'] = 'red',
				['name'] = 'VORTEX 1-1 TAW_Koksys Ural-375 #68',
				['category'] = 2,
				['task'] = {
				},
			},
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['groupName'] = 'VORTEX 1-1 TAW_Koksys Ural-375 #68',
			['coalition'] = 1,
			['time'] = 348546,
			['playerUCID'] = '8e872bdbe8fb7ba1cfb5fd0da6b3f392',
			['playerType'] = 'Mi-8MT',
			['groupID'] = 1000726,
			['spawnedGroup'] = {
				['id_'] = 1000071,
			},
		},
		[1] = {
			['playerCategory'] = 'HELICOPTER',
			['groupSpawnTable'] = {
				['visible'] = false,
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[1] = {
						['y'] = 626293.97576828,
						['x'] = -204739.49785532,
						['name'] = 'Unpacked Osa 9A33 ln #59',
						['type'] = 'Osa 9A33 ln',
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['heading'] = 120,
					},
				},
				['coalition'] = 'red',
				['name'] = 'VORTEX 1-1 TAW_Koksys Tor 9A331 #60',
				['category'] = 2,
				['task'] = {
				},
			},
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['groupName'] = 'VORTEX 1-1 TAW_Koksys Tor 9A331 #60',
			['coalition'] = 1,
			['time'] = 339217,
			['playerUCID'] = '8e872bdbe8fb7ba1cfb5fd0da6b3f392',
			['playerType'] = 'Mi-8MT',
			['groupID'] = 1000631,
			['spawnedGroup'] = {
				['id_'] = 1000072,
			},
		},
		[4] = {
			['playerCategory'] = 'HELICOPTER',
			['groupSpawnTable'] = {
				['visible'] = false,
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[3] = {
						['y'] = 467832.1733081,
						['x'] = -169651.57626754,
						['name'] = 'VORTEX 1-1 TAW_Koksys Kub 2P25 ln #88 #4',
						['type'] = 'Kub 2P25 ln',
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['heading'] = 120,
					},
					[2] = {
						['y'] = 467855.68808839,
						['x'] = -169631.43027001,
						['name'] = 'VORTEX 1-1 TAW_Koksys Kub 2P25 ln #88 #3',
						['type'] = 'Kub 2P25 ln',
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['heading'] = 120,
					},
					[4] = {
						['y'] = 467810.95924779,
						['x'] = -169672.67407188,
						['name'] = 'VORTEX 1-1 TAW_Koksys Kub 2P25 ln #88 #5',
						['type'] = 'Kub 2P25 ln',
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['heading'] = 120,
					},
					[1] = {
						['y'] = 467877.36513488,
						['x'] = -169607.36960617,
						['name'] = 'VORTEX 1-1 TAW_Koksys Kub 2P25 ln #88 #2',
						['type'] = 'Kub 2P25 ln',
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['heading'] = 120,
					},
				},
				['coalition'] = 'red',
				['name'] = 'VORTEX 1-1 TAW_Koksys SA-18 Igla manpad #88',
				['category'] = 2,
				['task'] = {
				},
			},
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['groupName'] = 'VORTEX 1-1 TAW_Koksys SA-18 Igla manpad #88',
			['coalition'] = 1,
			['time'] = 378521,
			['playerUCID'] = '8e872bdbe8fb7ba1cfb5fd0da6b3f392',
			['playerType'] = 'Mi-8MT',
			['groupID'] = 1000112,
			['spawnedGroup'] = {
				['id_'] = 1000073,
			},
		},
		[5] = {
			['playerCategory'] = 'HELICOPTER',
			['groupSpawnTable'] = {
				['visible'] = false,
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[1] = {
						['y'] = 569259.08346758,
						['x'] = -202858.25454048,
						['name'] = 'VORTEX 1-1 TAW_Koksys SKP-11 #93',
						['type'] = 'SKP-11',
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['heading'] = 120,
					},
				},
				['coalition'] = 'red',
				['name'] = 'VORTEX 1-1 TAW_Koksys SA-18 Igla manpad #93',
				['category'] = 2,
				['task'] = {
				},
			},
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['groupName'] = 'VORTEX 1-1 TAW_Koksys SA-18 Igla manpad #93',
			['coalition'] = 1,
			['time'] = 380230,
			['playerUCID'] = '8e872bdbe8fb7ba1cfb5fd0da6b3f392',
			['playerType'] = 'SA342M',
			['groupID'] = 1000118,
			['spawnedGroup'] = {
				['id_'] = 1000074,
			},
		},
	},
	['droppedCrates'] = {
		[1] = {
			['name'] = 'SA-15 Tor #3',
			['point'] = {
				['y'] = 662.01613370971,
				['x'] = -220861.68710186,
				['z'] = 640850.67760942,
			},
			['country'] = 2,
			['playerName'] = 'Archangel',
			['weight'] = 490,
			['side'] = 2,
			['spawnTime'] = 465232,
		},
		[2] = {
			['point'] = {
				['y'] = 632.60640911038,
				['x'] = -201275.14819095,
				['z'] = 650451.88431,
			},
			['name'] = 'SA-15 Tor #3',
			['country'] = 0,
			['spawnTime'] = 332988,
			['weight'] = 490,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[4] = {
			['point'] = {
				['y'] = 544.3252031213,
				['x'] = -223999.29562043,
				['z'] = 640015.93091081,
			},
			['name'] = '55G6 EWR #28',
			['country'] = 0,
			['spawnTime'] = 334615,
			['weight'] = 595,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[8] = {
			['point'] = {
				['y'] = 736.07217693026,
				['x'] = -187771.95248009,
				['z'] = 618335.59790873,
			},
			['name'] = 'Igla #54',
			['country'] = 0,
			['spawnTime'] = 338316,
			['weight'] = 55,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[16] = {
			['spawnTime'] = 450345,
			['name'] = 'SA-15 Tor #3',
			['country'] = 2,
			['playerName'] = 'Archangel.',
			['side'] = 2,
			['weight'] = 490,
			['point'] = {
				['y'] = 162.92968671137,
				['x'] = -216090.8223046,
				['z'] = 606870.43703163,
			},
		},
		[32] = {
			['spawnTime'] = 347651,
			['name'] = 'Groundcrew Fuel #6',
			['country'] = 0,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['side'] = 1,
			['weight'] = 325,
			['point'] = {
				['y'] = 163.64471348725,
				['x'] = -216187.79733467,
				['z'] = 606419.8341929,
			},
		},
		[33] = {
			['spawnTime'] = 347945,
			['name'] = 'Igla #10',
			['country'] = 0,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['side'] = 1,
			['weight'] = 55,
			['point'] = {
				['y'] = 146.64707996751,
				['x'] = -219847.02785439,
				['z'] = 599016.84593621,
			},
		},
		[34] = {
			['spawnTime'] = 348476,
			['name'] = 'Groundcrew ATC #13',
			['country'] = 0,
			['playerName'] = 'VORTEX 1-4 | TAW_Korenaga',
			['side'] = 1,
			['weight'] = 300,
			['point'] = {
				['y'] = 163.18372633218,
				['x'] = -216182.15685826,
				['z'] = 606451.93275189,
			},
		},
		[35] = {
			['spawnTime'] = 348537,
			['name'] = 'Igla #15',
			['country'] = 0,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['side'] = 1,
			['weight'] = 55,
			['point'] = {
				['y'] = 105.30078236291,
				['x'] = -228211.20319314,
				['z'] = 606087.4612053,
			},
		},
		[36] = {
			['spawnTime'] = 350978,
			['name'] = 'SA-15 Tor #19',
			['country'] = 0,
			['playerName'] = 'DAGGER 1-1 | TAW_Dugo',
			['side'] = 1,
			['weight'] = 490,
			['point'] = {
				['y'] = 3184.8710688309,
				['x'] = -206750.68097243,
				['z'] = 675362.90038761,
			},
		},
		[5] = {
			['point'] = {
				['y'] = 969.03023721102,
				['x'] = -225441.8638655,
				['z'] = 639297.86626985,
			},
			['name'] = 'Igla #31',
			['country'] = 0,
			['spawnTime'] = 334956,
			['weight'] = 55,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[38] = {
			['spawnTime'] = 355727,
			['name'] = 'SA-15 Tor #24',
			['country'] = 0,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['side'] = 1,
			['weight'] = 490,
			['point'] = {
				['y'] = 3174.9724763721,
				['x'] = -206694.6097828,
				['z'] = 675208.37261251,
			},
		},
		[39] = {
			['spawnTime'] = 460989,
			['name'] = 'SA-15 Tor #7',
			['country'] = 2,
			['playerName'] = 'Archangel',
			['side'] = 2,
			['weight'] = 490,
			['point'] = {
				['y'] = 744.04688445334,
				['x'] = -187890.84766015,
				['z'] = 618570.81318339,
			},
		},
		[40] = {
			['spawnTime'] = 461672,
			['name'] = 'SA-15 Tor #10',
			['country'] = 2,
			['playerName'] = 'Archangel',
			['side'] = 2,
			['weight'] = 490,
			['point'] = {
				['y'] = 632.07421514484,
				['x'] = -201080.57782357,
				['z'] = 650656.30705267,
			},
		},
		[41] = {
			['name'] = 'SA-15 Tor #6',
			['point'] = {
				['y'] = 631.53515058272,
				['x'] = -201263.07744839,
				['z'] = 650582.63571687,
			},
			['country'] = 2,
			['playerName'] = 'Archangel',
			['weight'] = 490,
			['side'] = 2,
			['spawnTime'] = 465727,
		},
		[11] = {
			['point'] = {
				['y'] = 3697.8864342078,
				['x'] = -173995.83829994,
				['z'] = 603576.24691837,
			},
			['name'] = '55G6 EWR #10',
			['country'] = 0,
			['spawnTime'] = 375340,
			['weight'] = 595,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[15] = {
			['playerName'] = 'Archangel',
			['point'] = {
				['y'] = 162.26953819689,
				['x'] = -216188.85032794,
				['z'] = 606179.14181995,
			},
			['country'] = 2,
			['spawnTime'] = 436860,
			['side'] = 2,
			['weight'] = 50,
			['name'] = 'Stinger #4',
		},
		[3] = {
			['point'] = {
				['y'] = 687.29025534009,
				['x'] = -220696.0445119,
				['z'] = 640584.51824072,
			},
			['name'] = 'SA-15 Tor #6',
			['country'] = 0,
			['spawnTime'] = 333552,
			['weight'] = 490,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[6] = {
			['point'] = {
				['y'] = 908.8029238791,
				['x'] = -225182.70557182,
				['z'] = 643182.39282488,
			},
			['name'] = 'Igla #34',
			['country'] = 0,
			['spawnTime'] = 335310,
			['weight'] = 55,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[12] = {
			['point'] = {
				['y'] = 32.275302943596,
				['x'] = -164339.19722329,
				['z'] = 463362.95360136,
			},
			['name'] = 'SA-15 Tor #13',
			['country'] = 0,
			['spawnTime'] = 377961,
			['weight'] = 490,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[10] = {
			['point'] = {
				['y'] = 3186.1142636978,
				['x'] = -206768.0081667,
				['z'] = 675351.03099805,
			},
			['name'] = 'SA-15 Tor #7',
			['country'] = 0,
			['spawnTime'] = 374484,
			['weight'] = 490,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[9] = {
			['point'] = {
				['y'] = 1191.2760297154,
				['x'] = -212358.94137008,
				['z'] = 676093.29750376,
			},
			['name'] = 'Igla #3',
			['country'] = 0,
			['spawnTime'] = 374031,
			['weight'] = 55,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[37] = {
			['spawnTime'] = 351867,
			['name'] = 'SA-8 Osa #21',
			['country'] = 2,
			['playerName'] = 'TAW_GoonerTC',
			['side'] = 2,
			['weight'] = 450,
			['point'] = {
				['y'] = 139.05022917323,
				['x'] = -218245.06731914,
				['z'] = 599170.96490833,
			},
		},
		[13] = {
			['point'] = {
				['y'] = 162.93750085239,
				['x'] = -216214.59599936,
				['z'] = 606424.13405806,
			},
			['name'] = 'Groundcrew Fuel #30',
			['country'] = 0,
			['spawnTime'] = 378595,
			['weight'] = 325,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[30] = {
			['point'] = {
				['y'] = 162.44262705849,
				['x'] = -216026.79739894,
				['z'] = 606248.24586478,
			},
			['name'] = 'SA-15 Tor #65',
			['country'] = 0,
			['spawnTime'] = 346582,
			['weight'] = 490,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[7] = {
			['point'] = {
				['y'] = 3143.3792268701,
				['x'] = -200651.43236192,
				['z'] = 623745.95272507,
			},
			['name'] = 'SA-15 Tor #51',
			['country'] = 0,
			['spawnTime'] = 337907,
			['weight'] = 490,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[27] = {
			['point'] = {
				['y'] = 917.72642405206,
				['x'] = -187847.07488112,
				['z'] = 609434.93786926,
			},
			['name'] = 'Igla #55',
			['country'] = 0,
			['spawnTime'] = 338532,
			['weight'] = 55,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[14] = {
			['point'] = {
				['y'] = 576.82775049301,
				['x'] = -202870.00446496,
				['z'] = 569249.0693707,
			},
			['name'] = 'JTAC SKP-11 #32',
			['country'] = 0,
			['spawnTime'] = 380225,
			['weight'] = 525,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[28] = {
			['point'] = {
				['y'] = 2164.96424053,
				['x'] = -204754.81708247,
				['z'] = 626280.7328417,
			},
			['name'] = 'SA-8 Osa #58',
			['country'] = 0,
			['spawnTime'] = 339213,
			['weight'] = 450,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[31] = {
			['spawnTime'] = 347598,
			['name'] = 'Groundcrew Ammo #4',
			['country'] = 0,
			['playerName'] = 'VORTEX 1-4 | TAW_Korenaga',
			['side'] = 1,
			['weight'] = 320,
			['point'] = {
				['y'] = 163.1836537531,
				['x'] = -216155.82933642,
				['z'] = 606434.49359737,
			},
		},
		[29] = {
			['point'] = {
				['y'] = 581.90705006953,
				['x'] = -214122.67849395,
				['z'] = 608617.11128944,
			},
			['name'] = 'JTAC SKP-11 #61',
			['country'] = 0,
			['spawnTime'] = 344928,
			['weight'] = 525,
			['side'] = 1,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
		},
		[42] = {
			['name'] = 'Igla #9',
			['point'] = {
				['y'] = 1038.4411497593,
				['x'] = -135805.0160115,
				['z'] = 712599.64474339,
			},
			['country'] = 0,
			['playerName'] = 'TAW_Adder',
			['weight'] = 55,
			['side'] = 1,
			['spawnTime'] = 474352,
		},
	},
	['numSpawnedConvoys'] = {
		['blue'] = 8,
		['red'] = 15,
	},
	['lifeLimit'] = 5,
	['serverName'] = 'KO Server 1',
	['timer'] = 550.001,
	['spawnedConvoys'] = {
		[1] = {
			['convoyType'] = 'defensive',
			['playerCategory'] = 'HELICOPTER',
			['route'] = {
				[1] = {
					['alt'] = 636,
					['type'] = 'Turning Point',
					['action'] = 'Off Road',
					['alt_type'] = 'BARO',
					['form'] = 'Off Road',
					['y'] = 695649.14285717,
					['x'] = -214919.42857143,
					['speed'] = 16.666666666667,
					['task'] = {
						['id'] = 'ComboTask',
						['params'] = {
							['tasks'] = {
							},
						},
					},
				},
				[2] = {
					['alt'] = 681,
					['type'] = 'Turning Point',
					['action'] = 'Off Road',
					['alt_type'] = 'BARO',
					['form'] = 'Off Road',
					['y'] = 676229.14285715,
					['x'] = -212480.85714286,
					['speed'] = 16.666666666667,
					['task'] = {
						['id'] = 'ComboTask',
						['params'] = {
							['tasks'] = {
							},
						},
					},
				},
			},
			['country'] = 0,
			['playerName'] = 'VORTEX 1-1 TAW_Koksy',
			['hidden'] = false,
			['units'] = {
				[1] = {
					['y'] = 682442.86294043,
					['x'] = -213634.34732808,
					['name'] = 'VORTEX 1-1 TAW_Koksys Convoy #79 T-90 #1',
					['heading'] = 299.58539170335,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'T-90',
				},
				[2] = {
					['y'] = 682409.24535043,
					['x'] = -213577.28602145,
					['name'] = 'VORTEX 1-1 TAW_Koksys Convoy #79 ZSU-23-4 Shilka #2',
					['heading'] = 299.58539170335,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'ZSU-23-4 Shilka',
				},
				[4] = {
					['y'] = 682081.71757603,
					['x'] = -213542.85426183,
					['name'] = 'VORTEX 1-1 TAW_Koksys Convoy #79 Tor 9A331 #4',
					['heading'] = 299.58539170335,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Tor 9A331',
				},
				[3] = {
					['y'] = 691895.94824179,
					['x'] = -214661.68015057,
					['name'] = 'VORTEX 1-1 TAW_Koksys Convoy #79 Strela-10M3 #3',
					['heading'] = 299.58539170335,
					['playerCanDrive'] = false,
					['skill'] = 'Excellent',
					['type'] = 'Strela-10M3',
				},
			},
			['coalition'] = 'red',
			['name'] = 'VORTEX 1-1 TAW_Koksys Convoy #79',
			['targetZone'] = 'East Bunker',
			['playerUCID'] = '8e872bdbe8fb7ba1cfb5fd0da6b3f392',
			['playerType'] = 'Convoy',
			['category'] = 2,
			['task'] = {
			},
			['visible'] = false,
		},
	},
	['cashPile'] = {
		['b62ffd872050a4c390fb1620911f6163'] = 17100,
		['bf9552dde778baf4461152cf2ff6acb6'] = 2500,
		['d5e3dcfcb94d9aa62e64c399e20c535d'] = 4300,
		['12a2d66cda57e5f5b4ac41f7d4b668e9'] = 1900,
		['b6340ffdb1196053b2f62739b7f97758'] = 500,
		['84b47e758bb1796f38d23be9f5cf5d97'] = 7500,
		['0e0c1afdb9a9b6493c8c905a0c4cb9a8'] = 24300,
		['AI'] = 4000,
		['d684348263b55ba85bc5212b6c0228b9'] = 8000,
		['6ee466ceda7195024e71d6a449f70615'] = 1200,
		['4b9e987a872e9c13f744a18ff3157182'] = 9000,
		['f10c00accc6b3f694490dbb307988e1d'] = 6100,
		['6a92b994e7a1e90653ce6e75ab1509d6'] = 4500,
		['3c4967fc0dc847e40bd55ac8f54919b4'] = 10100,
		['0ca14eb53bde69b3e54086070b5844d9'] = 2000,
		['21db51378372b7c510ab8cd0c1b5b8d5'] = 27200,
		['d5bea9c04a51210c765f5fcd991f1b68'] = 4600,
		['f168f82de2fcd1f90cad969dad2684ef'] = 7000,
		['435c48fda29c0e25461adbf6f31e184d'] = 0,
		['39d24d482e6915d7912417dae21e5e1b'] = 7400,
		['afd969ab06c3e1a0b442b2c8b53e0720'] = 2700,
		['ece009c941e20851c14159381043a6eb'] = 6500,
		['TAW_ZHeN'] = 2000,
		['d7f6161e6401a1c9c1f31ba7315e647c'] = 5100,
		['8e872bdbe8fb7ba1cfb5fd0da6b3f392'] = 29900,
		['185dc55ed3cc85fd9a08d4e58d6e80af'] = 200,
		['TAW_Smokey'] = 1000,
		['0377c9ccadc27a68b173fb4ca83efec5'] = 5000,
		['3ae5fa2261c90f0476b236854c74641b'] = 28600,
		['a06524f9bb718f4025de3f3728f609a8'] = 25300,
		['f7955f8e5f2a3ff48ceaf7c1aa117463'] = 21100,
	},
	['missionRuntime'] = 549.011,
	['convoys'] = {
		['blue'] = {
			['Gudauta'] = {
				['units'] = {
					[2] = {
						['y'] = 537598.95231407,
						['x'] = -198112.76018249,
						['heading'] = 281.69911237755,
					},
					[3] = {
						['y'] = 537628.91372116,
						['x'] = -198114.93659591,
						['heading'] = 281.51802828732,
					},
					[1] = {
						['y'] = 537568.96792231,
						['x'] = -198110.71656861,
						['heading'] = 282.40143716976,
					},
					[4] = {
						['y'] = 537658.64152906,
						['x'] = -198117.48881397,
						['heading'] = 283.97020050518,
					},
					[5] = {
						['y'] = 537688.41102827,
						['x'] = -198120.49419929,
						['heading'] = 286.72968304379,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 537570.0642021,
						['x'] = -198110.86336215,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 515804.9375,
						['x'] = -196891.921875,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
			['FARP Sukhumi'] = {
				['visible'] = false,
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 575068.27832031,
						['x'] = -224836.54162598,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 561748.7238086,
						['x'] = -209090.86890821,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
				['hidden'] = false,
				['units'] = {
					[2] = {
						['y'] = 575050.68679981,
						['type'] = 'AAV7',
						['name'] = 'FARP Sukhumi Convoy/2',
						['unitId'] = 16786432,
						['heading'] = 275.1271465054,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -224832.47992458,
					},
					[3] = {
						['y'] = 575098.92026712,
						['type'] = 'AAV7',
						['name'] = 'FARP Sukhumi Convoy/3',
						['unitId'] = 16786688,
						['heading'] = 275.33165322965,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -224837.05687719,
					},
					[1] = {
						['y'] = 574965.24913983,
						['type'] = 'AAV7',
						['name'] = 'FARP Sukhumi Convoy/1',
						['unitId'] = 16786176,
						['heading'] = 275.29585282828,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -224824.60005586,
					},
					[4] = {
						['y'] = 575133.6256542,
						['type'] = 'AAV7',
						['name'] = 'FARP Sukhumi Convoy/4',
						['unitId'] = 16786944,
						['heading'] = 275.36624601959,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -224840.41292094,
					},
					[5] = {
						['y'] = 575164.82081971,
						['type'] = 'AAV7',
						['name'] = 'FARP Sukhumi Convoy/5',
						['unitId'] = 16787200,
						['heading'] = 275.44201378269,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -224843.41419216,
					},
				},
				['coalition'] = 'blue',
				['name'] = 'FARP Sukhumi Convoy',
				['category'] = 2,
				['task'] = {
				},
				['groupId'] = 1408,
			},
			['Inguri-Dam Fortification'] = {
				['country'] = 16,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 634100.34631348,
						['x'] = -243824.24871826,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 680,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 640780.35400391,
						['x'] = -220838.50823975,
						['speed'] = 18.611111111111,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[2] = {
						['y'] = 634082.5625,
						['x'] = -243848.375,
						['heading'] = 16.2048657066,
					},
					[3] = {
						['y'] = 634064.25,
						['x'] = -243872.078125,
						['heading'] = 16.2048657066,
					},
					[1] = {
						['y'] = 634100.375,
						['x'] = -243824.25,
						['heading'] = 16.2048657066,
					},
					[4] = {
						['y'] = 634045.3125,
						['x'] = -243895.359375,
						['heading'] = 16.204755856014,
					},
					[5] = {
						['y'] = 634025.875,
						['x'] = -243918.15625,
						['heading'] = 16.204755856014,
					},
				},
			},
			['FARP Elbrus'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 689712.41931152,
						['x'] = -158018.29638672,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['enabled'] = true,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['number'] = 1,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 2153,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 672532.29287939,
						['x'] = -164441.59430847,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 689712.4375,
						['x'] = -158018.296875,
						['heading'] = 249.50025665991,
					},
					[2] = {
						['y'] = 689735.625,
						['x'] = -157999.328125,
						['heading'] = 249.50025665991,
					},
					[4] = {
						['y'] = 689782.9375,
						['x'] = -157962.40625,
						['heading'] = 249.50026186159,
					},
					[3] = {
						['y'] = 689759.125,
						['x'] = -157980.703125,
						['heading'] = 249.50025665991,
					},
				},
			},
			['East Bunker'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 695796.41649548,
						['x'] = -215255.28977853,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 1182,
						['type'] = 'Turning Point',
						['action'] = 'Off Road',
						['alt_type'] = 'BARO',
						['form'] = 'Off Road',
						['y'] = 676160.57142859,
						['x'] = -212466.57142858,
						['speed'] = 18.611111111111,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 695796.4375,
						['x'] = -215255.296875,
						['heading'] = 308.18035154861,
					},
					[2] = {
						['y'] = 695799.0625,
						['x'] = -215276.59375,
						['heading'] = 308.2121247178,
					},
					[4] = {
						['y'] = 695806.4375,
						['x'] = -215336.140625,
						['heading'] = 308.2121247178,
					},
					[3] = {
						['y'] = 695802.75,
						['x'] = -215306.359375,
						['heading'] = 308.2121247178,
					},
				},
			},
			['FARP Inguri Valley'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 638681.75137329,
						['x'] = -210516.00152588,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 638,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 650699.86897852,
						['x'] = -200745.57600159,
						['speed'] = 13.888888888889,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 638681.75,
						['x'] = -210516,
						['heading'] = 50.968484996347,
					},
					[2] = {
						['y'] = 638659.6875,
						['x'] = -210536.3125,
						['heading'] = 50.902798135514,
					},
					[4] = {
						['y'] = 638619.125,
						['x'] = -210580.234375,
						['heading'] = 50.902798135514,
					},
					[3] = {
						['y'] = 638638.8125,
						['x'] = -210557.71875,
						['heading'] = 50.902798135514,
					},
				},
			},
			['Nalchik'] = {
				['units'] = {
					[2] = {
						['y'] = 745807.27341675,
						['x'] = -109159.96309067,
						['heading'] = 128.63050373005,
					},
					[3] = {
						['y'] = 745778.08746878,
						['x'] = -109167.02781023,
						['heading'] = 129.05473567917,
					},
					[1] = {
						['y'] = 745837.00927283,
						['x'] = -109153.16915311,
						['heading'] = 114.82498279797,
					},
					[4] = {
						['y'] = 745748.90800117,
						['x'] = -109174.08104042,
						['heading'] = 128.74274925838,
					},
					[5] = {
						['y'] = 745719.72328852,
						['x'] = -109181.14419264,
						['heading'] = 128.63446401085,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 745835.14510155,
						['x'] = -109151.62745825,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 760405.875,
						['x'] = -124761.375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
			['Mineralnye Vody'] = {
				['units'] = {
					[2] = {
						['y'] = 707304.08847543,
						['x'] = -73060.042526707,
						['heading'] = 33.159614198503,
					},
					[3] = {
						['y'] = 707278.65593689,
						['x'] = -73075.933370628,
						['heading'] = 33.159614198503,
					},
					[1] = {
						['y'] = 707329.35853428,
						['x'] = -73044.121070411,
						['heading'] = 21.84863715738,
					},
					[4] = {
						['y'] = 707253.24348258,
						['x'] = -73091.804397058,
						['heading'] = 34.48750305245,
					},
					[5] = {
						['y'] = 707227.79251384,
						['x'] = -73107.735964433,
						['heading'] = 33.1596123305,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 707328.89467602,
						['x'] = -73046.489001125,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 707150.00541469,
						['x'] = -52664.65359574,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
			['West Bunker'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 608020.48501587,
						['x'] = -150621.0692749,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['enabled'] = true,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['number'] = 1,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 1560,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 600349.87844849,
						['x'] = -168599.7689209,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 608020.5,
						['x'] = -150621.0625,
						['heading'] = 203.10550036429,
					},
					[2] = {
						['y'] = 608007.875,
						['x'] = -150596.828125,
						['heading'] = 203.1160246921,
					},
					[4] = {
						['y'] = 607975.1875,
						['x'] = -150546.734375,
						['heading'] = 203.1160246921,
					},
					[3] = {
						['y'] = 607991.6875,
						['x'] = -150571.765625,
						['heading'] = 203.1160246921,
					},
				},
			},
			['Sochi-Adler'] = {
				['units'] = {
					[2] = {
						['y'] = 476731.95399534,
						['x'] = -172126.1624534,
						['heading'] = 301.73278138759,
					},
					[3] = {
						['y'] = 476758.2752883,
						['x'] = -172140.6055498,
						['heading'] = 301.4965154425,
					},
					[1] = {
						['y'] = 476705.70311665,
						['x'] = -172111.75944713,
						['heading'] = 301.38416411068,
					},
					[4] = {
						['y'] = 476784.60783763,
						['x'] = -172155.03980771,
						['heading'] = 301.36229556391,
					},
					[5] = {
						['y'] = 476810.87061898,
						['x'] = -172169.46581169,
						['heading'] = 301.61109518022,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 476706.16449915,
						['x'] = -172112.00967845,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 30,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 461910.90625,
						['x'] = -164263.875,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
			['Russian Factory'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 617624.44759531,
						['x'] = -87501.633282991,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['enabled'] = true,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['number'] = 1,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 836,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 616504.36800084,
						['x'] = -106224.10431743,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[3] = {
						['alt'] = 800,
						['type'] = 'Turning Point',
						['action'] = 'Off Road',
						['alt_type'] = 'BARO',
						['form'] = 'Off Road',
						['y'] = 615950.57142858,
						['x'] = -106248.57142858,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 617624.4375,
						['x'] = -87501.6328125,
						['heading'] = 183.42366886286,
					},
					[2] = {
						['y'] = 617643.8125,
						['x'] = -87478.703125,
						['heading'] = 183.42366886286,
					},
					[4] = {
						['y'] = 617682.5,
						['x'] = -87432.859375,
						['heading'] = 183.42366886286,
					},
					[3] = {
						['y'] = 617663.125,
						['x'] = -87455.796875,
						['heading'] = 183.42366886286,
					},
				},
			},
			['FARP Bylym'] = {
				['visible'] = false,
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 697215.14285714,
						['x'] = -152807.7142857,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 712862.839098,
						['x'] = -136133.00115623,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
				['hidden'] = false,
				['units'] = {
					[2] = {
						['y'] = 697228.39620738,
						['type'] = 'AAV7',
						['name'] = 'FARP Bylym Convoy Blue/2',
						['unitId'] = 16787712,
						['heading'] = 35.647160483923,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -152823.63710655,
					},
					[3] = {
						['y'] = 697227.25791614,
						['type'] = 'AAV7',
						['name'] = 'FARP Bylym Convoy Blue/3',
						['unitId'] = 16787968,
						['heading'] = 34.551330846915,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -152826.61072984,
					},
					[1] = {
						['y'] = 697243.74196329,
						['type'] = 'AAV7',
						['name'] = 'FARP Bylym Convoy Blue/1',
						['unitId'] = 16787456,
						['heading'] = 76.084616292228,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -152796.70305483,
					},
					[4] = {
						['y'] = 697252.16487316,
						['type'] = 'AAV7',
						['name'] = 'FARP Bylym Convoy Blue/4',
						['unitId'] = 16788224,
						['heading'] = 279.49357349941,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -152809.85927299,
					},
					[5] = {
						['y'] = 697274.02997396,
						['type'] = 'AAV7',
						['name'] = 'FARP Bylym Convoy Blue/5',
						['unitId'] = 16788480,
						['heading'] = 291.99010681677,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -152795.66311306,
					},
				},
				['coalition'] = 'blue',
				['name'] = 'FARP Bylym Convoy Blue',
				['category'] = 2,
				['task'] = {
				},
				['groupId'] = 1411,
			},
			['FARP Tkvarcheli'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 595992.8878886,
						['x'] = -232106.56925023,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 160,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 607229.75896921,
						['x'] = -216225.6384142,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 595992.875,
						['x'] = -232106.5625,
						['heading'] = 35.28251736063,
					},
					[2] = {
						['y'] = 595980.8125,
						['x'] = -232129.71875,
						['heading'] = 35.282524828363,
					},
					[4] = {
						['y'] = 595953.125,
						['x'] = -232182.96875,
						['heading'] = 35.282524828363,
					},
					[3] = {
						['y'] = 595966.875,
						['x'] = -232156.328125,
						['heading'] = 35.282524828363,
					},
				},
			},
			['Kobuleti'] = {
				['units'] = {
					[2] = {
						['y'] = 629961.91280544,
						['x'] = -331830.99273972,
						['heading'] = 37.571972319732,
					},
					[3] = {
						['y'] = 629947.74998755,
						['x'] = -331857.50613601,
						['heading'] = 36.785955580729,
					},
					[1] = {
						['y'] = 629976.09617177,
						['x'] = -331804.4427799,
						['heading'] = 36.940562436485,
					},
					[4] = {
						['y'] = 629933.73679315,
						['x'] = -331883.91420568,
						['heading'] = 37.646826816129,
					},
					[5] = {
						['y'] = 629919.57685154,
						['x'] = -331910.42407779,
						['heading'] = 36.912721079618,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 629975.33767748,
						['x'] = -331805.78509051,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 636407.1875,
						['x'] = -317121.9375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
			['Beslan'] = {
				['units'] = {
					[2] = {
						['y'] = 853181.44277847,
						['x'] = -167063.59233411,
						['heading'] = 3.517356019478,
					},
					[3] = {
						['y'] = 853179.69255781,
						['x'] = -167093.52152347,
						['heading'] = 4.6775914985762,
					},
					[1] = {
						['y'] = 853183.16864601,
						['x'] = -167033.71153391,
						['heading'] = 352.06530692305,
					},
					[4] = {
						['y'] = 853177.8789492,
						['x'] = -167123.47421609,
						['heading'] = 4.5975855170159,
					},
					[5] = {
						['y'] = 853176.12914591,
						['x'] = -167153.42018216,
						['heading'] = 4.5492574353341,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 853183.88793385,
						['x'] = -167035.94584686,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 843499.0625,
						['x'] = -149568.484375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
			['FARP Kodori'] = {
				['country'] = 2,
				['route'] = {
					[1] = {
						['alt'] = 205,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 600107.00100402,
						['x'] = -194215.5774295,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 26,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 723,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 617910.48265841,
						['x'] = -188072.50409595,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 600107,
						['x'] = -194215.578125,
						['heading'] = 71.142265408715,
					},
					[2] = {
						['y'] = 600078.4375,
						['x'] = -194224.515625,
						['heading'] = 71.206726831374,
					},
					[4] = {
						['y'] = 600025.8125,
						['x'] = -194251.40625,
						['heading'] = 71.692555070786,
					},
					[3] = {
						['y'] = 600051.3125,
						['x'] = -194236.40625,
						['heading'] = 71.19861495626,
					},
				},
			},
			['Senaki-Kolkhi'] = {
				['units'] = {
					[2] = {
						['y'] = 665098.96247274,
						['x'] = -286629.90957783,
						['heading'] = 301.46854286162,
					},
					[3] = {
						['y'] = 665121.1125619,
						['x'] = -286650.23731857,
						['heading'] = 301.33783236746,
					},
					[1] = {
						['y'] = 665076.93189571,
						['x'] = -286609.64743743,
						['heading'] = 301.45746480854,
					},
					[4] = {
						['y'] = 665143.13570076,
						['x'] = -286670.52597655,
						['heading'] = 301.41465333239,
					},
					[5] = {
						['y'] = 665165.19582778,
						['x'] = -286690.80579557,
						['heading'] = 301.40776664316,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 665078.17461514,
						['x'] = -286610.17646482,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 646650.9375,
						['x'] = -281127.84375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 2,
			},
		},
		['red'] = {
			['Gudauta'] = {
				['units'] = {
					[2] = {
						['y'] = 500337.96552068,
						['x'] = -185086.58339821,
						['heading'] = 128.89074206505,
					},
					[3] = {
						['y'] = 500310.34437512,
						['x'] = -185074.89902439,
						['heading'] = 128.89934525908,
					},
					[1] = {
						['y'] = 500365.62373722,
						['x'] = -185098.27249637,
						['heading'] = 128.89591861292,
					},
					[4] = {
						['y'] = 500282.7152828,
						['x'] = -185063.22384124,
						['heading'] = 128.89546921363,
					},
					[5] = {
						['y'] = 500255.09512593,
						['x'] = -185051.55586683,
						['heading'] = 128.89162464135,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 500365.14411944,
						['x'] = -185097.90063812,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 515804.9375,
						['x'] = -196891.921875,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
			['FARP Sukhumi'] = {
				['visible'] = false,
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 544665.6875,
						['x'] = -202280.59375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 561746.26916974,
						['x'] = -209092.9922325,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[2] = {
						['y'] = 544667.11655247,
						['type'] = 'BTR-80',
						['name'] = 'FARP Sukhumi Convoy #001/2',
						['unitId'] = 16783872,
						['heading'] = 114.38456517198,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -202281.2460163,
					},
					[3] = {
						['y'] = 544701.6533248,
						['type'] = 'BTR-80',
						['name'] = 'FARP Sukhumi Convoy #001/3',
						['unitId'] = 16784128,
						['heading'] = 162.10100148535,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -202286.38694888,
					},
					[1] = {
						['y'] = 544693.74779042,
						['type'] = 'BTR-80',
						['name'] = 'FARP Sukhumi Convoy #001/1',
						['unitId'] = 16783616,
						['heading'] = 82.196827320232,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -202279.51854121,
					},
					[4] = {
						['y'] = 544727.16608372,
						['type'] = 'BTR-80',
						['name'] = 'FARP Sukhumi Convoy #001/4',
						['unitId'] = 16784384,
						['heading'] = 229.6437669449,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -202290.19534934,
					},
					[5] = {
						['y'] = 544753.91013993,
						['type'] = 'BTR-80',
						['name'] = 'FARP Sukhumi Convoy #001/5',
						['unitId'] = 16784640,
						['heading'] = 251.40855932103,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -202289.50263567,
					},
				},
				['coalition'] = 'red',
				['name'] = 'FARP Sukhumi Convoy #001',
				['category'] = 2,
				['task'] = {
				},
				['groupId'] = 1409,
			},
			['Inguri-Dam Fortification'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 636,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 638412.16297064,
						['x'] = -211014.99802481,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 641414.51321411,
						['x'] = -221244.9232254,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[2] = {
						['y'] = 638423.6875,
						['x'] = -210987.296875,
						['heading'] = 163.64091146063,
					},
					[3] = {
						['y'] = 638435.1875,
						['x'] = -210959.609375,
						['heading'] = 163.64091146063,
					},
					[1] = {
						['y'] = 638412.1875,
						['x'] = -211015,
						['heading'] = 163.64091146063,
					},
					[4] = {
						['y'] = 638446.75,
						['x'] = -210931.90625,
						['heading'] = 163.64091146063,
					},
					[5] = {
						['y'] = 638458.25,
						['x'] = -210904.203125,
						['heading'] = 163.64091146063,
					},
				},
			},
			['FARP Elbrus'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 1567,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 689451.26114339,
						['x'] = -158277.97991716,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 2155,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 672883.58489227,
						['x'] = -164430.52256775,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 689451.25,
						['x'] = -158277.984375,
						['heading'] = 249.65832088515,
					},
					[2] = {
						['y'] = 689471.125,
						['x'] = -158255.484375,
						['heading'] = 249.65832088515,
					},
					[4] = {
						['y'] = 689511,
						['x'] = -158210.65625,
						['heading'] = 249.65832088515,
					},
					[3] = {
						['y'] = 689491,
						['x'] = -158232.96875,
						['heading'] = 249.65832088515,
					},
				},
			},
			['East Bunker'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 636,
						['type'] = 'Turning Point',
						['action'] = 'Off Road',
						['alt_type'] = 'BARO',
						['form'] = 'Off Road',
						['y'] = 695649.14285717,
						['x'] = -214919.42857143,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'Off Road',
						['alt_type'] = 'BARO',
						['form'] = 'Off Road',
						['y'] = 676229.14285715,
						['x'] = -212480.85714286,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 695649.125,
						['x'] = -214919.421875,
						['heading'] = 299.58539170335,
					},
					[2] = {
						['y'] = 695657.9375,
						['x'] = -214932.421875,
						['heading'] = 299.58539170335,
					},
					[4] = {
						['y'] = 695691.625,
						['x'] = -214982.078125,
						['heading'] = 299.58539170335,
					},
					[3] = {
						['y'] = 695674.8125,
						['x'] = -214957.234375,
						['heading'] = 299.58539170335,
					},
				},
			},
			['FARP Inguri Valley'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 636,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 664548.70776367,
						['x'] = -189140.42462158,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 650627.0992151,
						['x'] = -201621.75965279,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 664548.6875,
						['x'] = -189140.421875,
						['heading'] = 228.24213701006,
					},
					[2] = {
						['y'] = 664552,
						['x'] = -189110.6875,
						['heading'] = 228.14827551731,
					},
					[4] = {
						['y'] = 664552.1875,
						['x'] = -189051.15625,
						['heading'] = 228.14656700607,
					},
					[3] = {
						['y'] = 664553.9375,
						['x'] = -189080.796875,
						['heading'] = 228.14827551731,
					},
				},
			},
			['Nalchik'] = {
				['units'] = {
					[2] = {
						['y'] = 778140.69900446,
						['x'] = -128232.53010352,
						['heading'] = 284.77808371767,
					},
					[3] = {
						['y'] = 778163.76390292,
						['x'] = -128251.74948624,
						['heading'] = 284.77808371767,
					},
					[1] = {
						['y'] = 778117.6257105,
						['x'] = -128213.33194355,
						['heading'] = 284.77808371767,
					},
					[4] = {
						['y'] = 778186.82040632,
						['x'] = -128270.96665432,
						['heading'] = 284.77808371767,
					},
					[5] = {
						['y'] = 778209.88290632,
						['x'] = -128290.18540432,
						['heading'] = 284.77808371767,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 778118.51159538,
						['x'] = -128213.52939828,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 760352.23693848,
						['x'] = -124696.81885529,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
			['Mineralnye Vody'] = {
				['units'] = {
					[2] = {
						['y'] = 726673.02462577,
						['x'] = -47984.764961293,
						['heading'] = 263.05685586119,
					},
					[3] = {
						['y'] = 726702.25472347,
						['x'] = -47991.712624067,
						['heading'] = 263.18618980169,
					},
					[1] = {
						['y'] = 726643.76734296,
						['x'] = -47978.179910675,
						['heading'] = 263.05685586119,
					},
					[4] = {
						['y'] = 726731.39112908,
						['x'] = -47999.004277248,
						['heading'] = 263.05685586119,
					},
					[5] = {
						['y'] = 726760.3899152,
						['x'] = -48006.637237572,
						['heading'] = 263.05685586119,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 726644.61541748,
						['x'] = -47978.044921875,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 707080.22792755,
						['x'] = -51567.133858085,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
			['West Bunker'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 636,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 608043.65618896,
						['x'] = -150979.86461151,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 600332.18206787,
						['x'] = -168617.17297363,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 608043.625,
						['x'] = -150979.859375,
						['heading'] = 203.68141631861,
					},
					[2] = {
						['y'] = 608043.5,
						['x'] = -150949.90625,
						['heading'] = 203.64159511968,
					},
					[4] = {
						['y'] = 608043.3125,
						['x'] = -150889.90625,
						['heading'] = 203.72373121148,
					},
					[3] = {
						['y'] = 608043.4375,
						['x'] = -150919.90625,
						['heading'] = 203.64509267706,
					},
				},
			},
			['Sochi-Adler'] = {
				['units'] = {
					[2] = {
						['y'] = 444259.13797801,
						['x'] = -151529.77534672,
						['heading'] = 127.05295204418,
					},
					[3] = {
						['y'] = 444235.68565538,
						['x'] = -151511.1159574,
						['heading'] = 127.1375053988,
					},
					[1] = {
						['y'] = 444282.83801662,
						['x'] = -151548.08933558,
						['heading'] = 126.9834227217,
					},
					[4] = {
						['y'] = 444212.51537129,
						['x'] = -151492.09787886,
						['heading'] = 127.26892145676,
					},
					[5] = {
						['y'] = 444189.61337489,
						['x'] = -151472.72616087,
						['heading'] = 127.28752343414,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 444282.72485352,
						['x'] = -151547.99365234,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 459818.30497595,
						['x'] = -163133.26492087,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
			['Kobuleti'] = {
				['units'] = {
					[2] = {
						['y'] = 630055.41647382,
						['x'] = -306968.62056041,
						['heading'] = 153.19115659841,
					},
					[3] = {
						['y'] = 630070.85646235,
						['x'] = -306942.87548499,
						['heading'] = 153.19115538052,
					},
					[1] = {
						['y'] = 630039.97319954,
						['x'] = -306994.35913368,
						['heading'] = 153.1911551064,
					},
					[4] = {
						['y'] = 630086.29396235,
						['x'] = -306917.12548499,
						['heading'] = 153.19115538052,
					},
					[5] = {
						['y'] = 630101.66896235,
						['x'] = -306891.37548499,
						['heading'] = 153.19115538052,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 630039.37133418,
						['x'] = -306993.31292065,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 636407.1875,
						['x'] = -317121.9375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
			['FARP Bylym'] = {
				['visible'] = false,
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 727979.25056035,
						['x'] = -119289.12112345,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 712856.46758464,
						['x'] = -136136.41969436,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
				['hidden'] = false,
				['units'] = {
					[2] = {
						['y'] = 727986.24174927,
						['type'] = 'BTR-80',
						['name'] = 'FARP Bylym Convoy Red/2',
						['unitId'] = 16785152,
						['heading'] = 241.57304554366,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -119282.2748576,
					},
					[3] = {
						['y'] = 728014.4594253,
						['type'] = 'BTR-80',
						['name'] = 'FARP Bylym Convoy Red/3',
						['unitId'] = 16785408,
						['heading'] = 243.30566125682,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -119264.50516271,
					},
					[1] = {
						['y'] = 727947.85913745,
						['type'] = 'BTR-80',
						['name'] = 'FARP Bylym Convoy Red/1',
						['unitId'] = 16784896,
						['heading'] = 239.6092578897,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -119305.28855244,
					},
					[4] = {
						['y'] = 728040.30982302,
						['type'] = 'BTR-80',
						['name'] = 'FARP Bylym Convoy Red/4',
						['unitId'] = 16785664,
						['heading'] = 244.40175005779,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -119248.43594806,
					},
					[5] = {
						['y'] = 728065.83077415,
						['type'] = 'BTR-80',
						['name'] = 'FARP Bylym Convoy Red/5',
						['unitId'] = 16785920,
						['heading'] = 244.58649441413,
						['playerCanDrive'] = false,
						['skill'] = 'Excellent',
						['x'] = -119232.61037106,
					},
				},
				['coalition'] = 'red',
				['name'] = 'FARP Bylym Convoy Red',
				['category'] = 2,
				['task'] = {
				},
				['groupId'] = 1410,
			},
			['FARP Tkvarcheli'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 636,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 595874.85714286,
						['x'] = -232404.28571429,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 606940.52275167,
						['x'] = -216230.48804298,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 595874.875,
						['x'] = -232404.28125,
						['heading'] = 34.379370991091,
					},
					[2] = {
						['y'] = 595883.1875,
						['x'] = -232404.9375,
						['heading'] = 34.379370991091,
					},
					[4] = {
						['y'] = 595895.25,
						['x'] = -232364.1875,
						['heading'] = 34.379370991091,
					},
					[3] = {
						['y'] = 595886.75,
						['x'] = -232392.96875,
						['heading'] = 34.379370991091,
					},
				},
			},
			['FARP Kodori'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 636,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 600414.006073,
						['x'] = -194123.74597168,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 617949.88404083,
						['x'] = -188132.67982435,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 600414,
						['x'] = -194123.75,
						['heading'] = 71.158871560621,
					},
					[2] = {
						['y'] = 600385.75,
						['x'] = -194133.75,
						['heading'] = 71.158871560621,
					},
					[4] = {
						['y'] = 600328.5,
						['x'] = -194151.65625,
						['heading'] = 71.154177167589,
					},
					[3] = {
						['y'] = 600357.25,
						['x'] = -194143.0625,
						['heading'] = 71.154177167589,
					},
				},
			},
			['Beslan'] = {
				['units'] = {
					[2] = {
						['y'] = 822960.88653647,
						['x'] = -144261.91155051,
						['heading'] = 104.89780306179,
					},
					[3] = {
						['y'] = 822931.06620475,
						['x'] = -144265.62727686,
						['heading'] = 104.70808917228,
					},
					[1] = {
						['y'] = 822990.76987316,
						['x'] = -144258.78879068,
						['heading'] = 104.93910090094,
					},
					[4] = {
						['y'] = 822901.2355541,
						['x'] = -144269.3405828,
						['heading'] = 104.5577187455,
					},
					[5] = {
						['y'] = 822871.50115747,
						['x'] = -144273.07897724,
						['heading'] = 104.51140918806,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 822990.54441832,
						['x'] = -144258.73188681,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 843499.0625,
						['x'] = -149568.484375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
			['Russian Factory'] = {
				['country'] = 0,
				['route'] = {
					[1] = {
						['alt'] = 705,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 617490.60426053,
						['x'] = -87660.201151486,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 681,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 616488.83919602,
						['x'] = -106173.56403636,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[3] = {
						['alt'] = 800,
						['type'] = 'Turning Point',
						['action'] = 'Off Road',
						['alt_type'] = 'BARO',
						['form'] = 'Off Road',
						['y'] = 615966.85714286,
						['x'] = -106206.57142857,
						['speed'] = 16.666666666667,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['units'] = {
					[1] = {
						['y'] = 617490.625,
						['x'] = -87660.203125,
						['heading'] = 183.09728158224,
					},
					[2] = {
						['y'] = 617509.9375,
						['x'] = -87637.2890625,
						['heading'] = 183.09728158224,
					},
					[4] = {
						['y'] = 617548.625,
						['x'] = -87591.4453125,
						['heading'] = 183.09728158224,
					},
					[3] = {
						['y'] = 617529.3125,
						['x'] = -87614.3515625,
						['heading'] = 183.09728158224,
					},
				},
			},
			['Senaki-Kolkhi'] = {
				['units'] = {
					[2] = {
						['y'] = 627678.56757113,
						['x'] = -269789.09922862,
						['heading'] = 121.79042329823,
					},
					[3] = {
						['y'] = 627653.05371921,
						['x'] = -269773.34044392,
						['heading'] = 121.68416039645,
					},
					[1] = {
						['y'] = 627703.86761888,
						['x'] = -269805.15403882,
						['heading'] = 121.77362640904,
					},
					[4] = {
						['y'] = 627627.33715401,
						['x'] = -269757.85339636,
						['heading'] = 121.44016365917,
					},
					[5] = {
						['y'] = 627601.53105112,
						['x'] = -269742.47416664,
						['heading'] = 120.81687555956,
					},
				},
				['route'] = {
					[1] = {
						['alt'] = 20,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 627703.56091309,
						['x'] = -269804.98339844,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
					[2] = {
						['alt'] = 41,
						['type'] = 'Turning Point',
						['action'] = 'On Road',
						['alt_type'] = 'BARO',
						['form'] = 'On Road',
						['y'] = 646650.9375,
						['x'] = -281127.84375,
						['speed'] = 20,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
								},
							},
						},
					},
				},
				['country'] = 0,
			},
		},
	},
}
