--------------------------------------------
-- debugHandler() 
--------------------------------------------
--
-- saves events into a file as lua tables
debugHandler = {}
debugHandler.unitCategoryTable = {
	[0] = "AIRPLANE",
	[1] = "HELICOPTER",
	[2] = "GROUND_UNIT",
	[3] = "SHIP",
	[4] = "STRUCTURE",
}

debugHandler.objectCatTable = {
    [1] = "UNIT",
    [2] = "WEAPON",
    [3] = "STATIC",
    [4] = "SCENERY",
    [5] = "BASE",
    [6] = "Cargo",
}

debugHandler.eventTable = {
	[0] = "S_EVENT_INVALID",
	[1] = "S_EVENT_SHOT",
	[2] = "S_EVENT_HIT",
	[3] = "S_EVENT_TAKEOFF",
	[4] = "S_EVENT_LAND",
	[5] = "S_EVENT_CRASH",
	[6] = "S_EVENT_EJECTION",
	[7] = "S_EVENT_REFUELING",
	[8] = "S_EVENT_DEAD",
	[9] = "S_EVENT_PILOT_DEAD",
	[10] = "S_EVENT_BASE_CAPTURED",
	[11] = "S_EVENT_MISSION_START",
	[12] = "S_EVENT_MISSION_END",
	[13] = "S_EVENT_TOOK_CONTROL",
	[14] = "S_EVENT_REFUELING_STOP",
	[15] = "S_EVENT_BIRTH",
	[16] = "S_EVENT_HUMAN_FAILURE",
	[17] = "S_EVENT_ENGINE_STARTUP",
	[18] = "S_EVENT_ENGINE_SHUTDOWN",
	[19] = "S_EVENT_PLAYER_ENTER_UNIT",
	[20] = "S_EVENT_PLAYER_LEAVE_UNIT",
	[21] = "S_EVENT_PLAYER_COMMENT",
	[22] = "S_EVENT_SHOOTING_START",
	[23] = "S_EVENT_SHOOTING_END",
	[24] = "S_EVENT_MARK_ADDED", 
	[25] = "S_EVENT_MARK_CHANGE",
  	[26] = "S_EVENT_MARK_REMOVED",
  	[27] = "S_EVENT_MAX",
}

debugHandler.i = 0
function debugHandler:onEvent(event)
    if event.id == world.event.S_EVENT_BASE_CAPTURED then
		return
	end
    
    koEngine.debugText("DEBUG HANDLER STARTED" )
    
    local info = {}
    info.name = debugHandler.eventTable[event.id]
    
    if event.initiator then
        env.info(koEngine.TableSerialization(event.initiator))
        info.category = debugHandler.objectCatTable[event.initiator:getCategory()]
        if info.category == "UNIT" then
            info.playerName = event.initiator:getPlayerName()
        end
        info.groupName = event.initiator:getName()
        info.typeName = event.initiator:getTypeName()
        info.initiatorDesc = event.initiator:getDesc()
    end

    if event.weapon then
        info.weaponTarget = Weapon.getTarget(event.weapon)
        if info.weaponTarget then
            env.info(koEngine.TableSerialization(info.weaponTarget))
            --info.weaponTargetDesc = Object.getDesc(info.weaponTarget)
            info.weaponTargetDesc = info.weaponTarget:getDesc()
        end

        info.weaponLauncher = Weapon.getLauncher(event.weapon)
        if info.weaponLauncher then
            env.info(koEngine.TableSerialization(info.weaponLauncher))
            --info.weaponLauncherDesc = Object.getDesc(info.weaponLauncher)
            info.weaponLauncherDesc = info.weaponLauncher:getDesc(info.weaponLauncher)
        end

        info.weaponDesc = event.weapon:getDesc()
    end
    if event.target then
        info.targetDesc = event.target:getDesc()
    end

    info.event = event

    --debugHandler.log("debugHandler:onEvent()\n"..koEngine.TableSerialization(event))
    info.timestamp = timer.getTime()
    if debugHandler.logFile then
        debugHandler.logFile:write("["..debugHandler.i.."] = "..koEngine.TableSerialization(info))
        debugHandler.logFile:flush()
        env.info("event saved at timestamp: "..info.timestamp)
    else 
        env.info("logfile no accessible")
    end

    debugHandler.i = debugHandler.i + 1
end

-- setup logfile
debugHandler.logFile = io.open(koEngine.baseDir..[[event_session]]..koEngine.sessionID..[[.lua]], "w")
function debugHandler.log(txt)
    local logTime = timer.getTime()
	if debugHandler.logFile then
	 	debugHandler.logFile:write(string.format("%09.3f", tostring(logTime)).."\t"..txt.."\n")
	    debugHandler.logFile:flush()
    else
    	env.info("LOG FAILED")
    end
    env.info("Event logged at time: "..tostring(logTime))
end


world.addEventHandler(debugHandler)