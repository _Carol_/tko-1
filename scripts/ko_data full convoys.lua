MissionData = {
	['Aerodrome'] = {
		['Senaki-Kolkhi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'blue',
			['groundUnits'] = {
			},
			['groups'] = {
				['Senaki-Kolkhi AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647412.7662372,
							['x'] = -279922.62279556,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 15,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647412.74999998,
							['type'] = 'M1045 HMMWV TOW',
							['name'] = 'Senaki-Kolkhi AT/1',
							['heading'] = 332.52557316103,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -279922.62499996,
						},
						[2] = {
							['y'] = 646959.31249994,
							['type'] = 'M-2 Bradley',
							['name'] = 'Senaki-Kolkhi AT/2',
							['heading'] = 332.52557534264,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -280020.18749989,
						},
						[3] = {
							['y'] = 649153.06249994,
							['type'] = 'M1134 Stryker ATGM',
							['name'] = 'Senaki-Kolkhi AT/3',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281976.84374989,
						},
						[4] = {
							['y'] = 646511.1875,
							['type'] = 'M-1 Abrams',
							['name'] = 'Senaki-Kolkhi AT/4',
							['heading'] = 332.52557467598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282721.8125,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi SR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647150.68949682,
							['x'] = -280216.09258889,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647150.6875,
							['type'] = 'M48 Chaparral',
							['name'] = 'Senaki-Kolkhi SR SAM/1',
							['heading'] = 309.70826406259,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -280216.09375,
						},
						[2] = {
							['y'] = 647718.0625,
							['type'] = 'M48 Chaparral',
							['name'] = 'Senaki-Kolkhi SR SAM/2',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282204.25,
						},
						[3] = {
							['y'] = 648440.5625,
							['type'] = 'M6 Linebacker',
							['name'] = 'Senaki-Kolkhi SR SAM/3',
							['heading'] = 309.70826125413,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -280390.3125,
						},
						[4] = {
							['y'] = 646213.06249996,
							['type'] = 'M1097 Avenger',
							['name'] = 'Senaki-Kolkhi SR SAM/4',
							['heading'] = 309.70826272632,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282377.21874997,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi SR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647034.89573249,
							['x'] = -281610.91290211,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 14,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647034.875,
							['type'] = 'Vulcan',
							['name'] = 'Senaki-Kolkhi AAA/1',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281610.90625,
						},
						[2] = {
							['y'] = 648521.75,
							['type'] = 'Vulcan',
							['name'] = 'Senaki-Kolkhi AAA/2',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281784.90625,
						},
						[3] = {
							['y'] = 646022.5625,
							['type'] = 'Vulcan',
							['name'] = 'Senaki-Kolkhi AAA/3',
							['heading'] = 336.00001083097,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281526.5625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi AAA',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi Patriot'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 645730.85360239,
							['x'] = -282071.17876779,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 645730.875,
							['type'] = 'Patriot AMG',
							['name'] = 'Senaki-Kolkhi Patriot/1',
							['heading'] = 130.9999948998,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282071.1875,
						},
						[2] = {
							['y'] = 645681.75,
							['type'] = 'Patriot cp',
							['name'] = 'Senaki-Kolkhi Patriot/2',
							['heading'] = 311.00000035867,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282029.25,
						},
						[3] = {
							['y'] = 645819.4375,
							['type'] = 'Patriot AMG',
							['name'] = 'Senaki-Kolkhi Patriot/3',
							['heading'] = 130.99999605699,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281942.09375,
						},
						[4] = {
							['y'] = 645736.4375,
							['type'] = 'Patriot ECS',
							['name'] = 'Senaki-Kolkhi Patriot/4',
							['heading'] = 311.00000087491,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281939.125,
						},
						[5] = {
							['y'] = 645657.6875,
							['type'] = 'Patriot EPP',
							['name'] = 'Senaki-Kolkhi Patriot/5',
							['heading'] = 131.00000155776,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282105.15625,
						},
						[6] = {
							['y'] = 645781.5,
							['type'] = 'Patriot EPP',
							['name'] = 'Senaki-Kolkhi Patriot/6',
							['heading'] = 130.99999605699,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -282002.84375,
						},
						[7] = {
							['y'] = 645650.5625,
							['type'] = 'Patriot str',
							['name'] = 'Senaki-Kolkhi Patriot/7',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281972.40625,
						},
						[8] = {
							['y'] = 645495.1875,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/8',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281910.25,
						},
						[9] = {
							['y'] = 645611.3125,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/9',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281728.78125,
						},
						[10] = {
							['y'] = 645558.9375,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/10',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281790.75,
						},
						[11] = {
							['y'] = 645443.6875,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/11',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281926.8125,
						},
						[12] = {
							['y'] = 645499.75,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/12',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281859.6875,
						},
						[13] = {
							['y'] = 645555.5625,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/13',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281840.8125,
						},
						[14] = {
							['y'] = 645609.125,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/14',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281778.34375,
						},
						[15] = {
							['y'] = 645445.125,
							['type'] = 'Patriot ln',
							['name'] = 'Senaki-Kolkhi Patriot/15',
							['heading'] = 311.00000134612,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281970.46875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi Patriot',
					['category'] = 2,
					['task'] = {
					},
				},
				['Senaki-Kolkhi MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647837.26922637,
							['x'] = -281988.86575445,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647837.25000001,
							['type'] = 'Tor 9A331',
							['name'] = 'Senaki-Kolkhi MR SAM/1',
							['heading'] = 5.2790847451229,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281988.87499987,
						},
						[2] = {
							['y'] = 646731.18750001,
							['type'] = 'Tor 9A331',
							['name'] = 'Senaki-Kolkhi MR SAM/2',
							['heading'] = 5.2790851740873,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -281881.24999987,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Senaki-Kolkhi MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'open',
			['priority'] = 'strategic',
		},
		['Beslan'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'red',
			['groundUnits'] = {
			},
			['groups'] = {
				['Beslan AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 542,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 843393.91394619,
							['x'] = -149576.53829381,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 843393.9375001,
							['type'] = 'T-90',
							['name'] = 'Beslan AT/1',
							['heading'] = 123.6900684732,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -149576.53125007,
						},
						[2] = {
							['y'] = 843985.9375001,
							['type'] = 'T-90',
							['name'] = 'Beslan AT/2',
							['heading'] = 123.69006847326,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -149265.75000007,
						},
						[3] = {
							['y'] = 843741.6250001,
							['type'] = 'T-72B',
							['name'] = 'Beslan AT/3',
							['heading'] = 123.69006942152,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148248.26562507,
						},
						[4] = {
							['y'] = 841884.56249995,
							['type'] = 'BRDM-2',
							['name'] = 'Beslan AT/4',
							['heading'] = 123.69006752598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148741.24999997,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan S300'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 520,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 843719.82270088,
							['x'] = -147619.1865489,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 843719.81249999,
							['type'] = 'S-300PS 54K6 cp',
							['name'] = 'Beslan S300/1',
							['heading'] = 177.46334081417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147619.18749987,
						},
						[2] = {
							['y'] = 843924.31249999,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Beslan S300/2',
							['heading'] = 177.46334097329,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147790.62499987,
						},
						[3] = {
							['y'] = 844124.93749999,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Beslan S300/3',
							['heading'] = 177.46334127111,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147555.32812487,
						},
						[4] = {
							['y'] = 843592.625,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Beslan S300/4',
							['heading'] = 177.46334112712,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147755.90625,
						},
						[5] = {
							['y'] = 843908.9375,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Beslan S300/5',
							['heading'] = 177.46334134035,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147331.59375,
						},
						[6] = {
							['y'] = 843217.43749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/6',
							['heading'] = 177.46334093534,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147579.68749987,
						},
						[7] = {
							['y'] = 843171.24999999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/7',
							['heading'] = 177.46334078268,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147424.99999987,
						},
						[8] = {
							['y'] = 843071.93749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/8',
							['heading'] = 177.46334100304,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147498.87499987,
						},
						[9] = {
							['y'] = 843299.56249999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/9',
							['heading'] = 177.46334096565,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147486.81249987,
						},
						[10] = {
							['y'] = 843185.93749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/10',
							['heading'] = 177.46334137401,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147298.04687487,
						},
						[11] = {
							['y'] = 843344.37499999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/11',
							['heading'] = 177.46334078268,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147339.56249987,
						},
						[12] = {
							['y'] = 843267.43749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/12',
							['heading'] = 177.46334070851,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147198.23437487,
						},
						[13] = {
							['y'] = 843413.68749999,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Beslan S300/13',
							['heading'] = 177.46334093342,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -147228.74999987,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan S300',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 525,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 842351.67722797,
							['x'] = -148616.448312,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 842351.68750008,
							['type'] = 'Tor 9A331',
							['name'] = 'Beslan MR SAM/1',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148616.45312509,
						},
						[2] = {
							['y'] = 844287.25000008,
							['type'] = 'Tor 9A331',
							['name'] = 'Beslan MR SAM/2',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148742.23437509,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan IR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 842892.44684132,
							['x'] = -149650.24568468,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 842892.4375,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/1',
							['heading'] = 110.55604402046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -149650.25,
						},
						[2] = {
							['y'] = 842884.0625,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/2',
							['heading'] = 110.55604242163,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148946.984375,
						},
						[3] = {
							['y'] = 844217.875,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/3',
							['heading'] = 110.55604481954,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -150037.796875,
						},
						[4] = {
							['y'] = 845155.25,
							['type'] = 'Strela-10M3',
							['name'] = 'Beslan IR SAM/4',
							['heading'] = 110.55604402028,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148559.390625,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan IR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Beslan AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 314,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 842172.56204603,
							['x'] = -148397.85487275,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 842172.5625,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Beslan AAA/1',
							['heading'] = 32.307333137882,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148397.859375,
						},
						[2] = {
							['y'] = 843818.75,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Beslan AAA/2',
							['heading'] = 32.307333648384,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148716.703125,
						},
						[3] = {
							['y'] = 844997.75,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Beslan AAA/3',
							['heading'] = 32.307334200185,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -148757.5,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Beslan AAA',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'open',
			['priority'] = 'strategic',
		},
		['Kutaisi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'blue',
			['groundUnits'] = {
			},
			['groups'] = {
				['Kutaisi SR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 683250.4008675,
							['x'] = -284044.29114327,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 683250.375,
							['type'] = 'M48 Chaparral',
							['name'] = 'Kutaisi SR SAM/1',
							['heading'] = 309.708263519,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284044.28125,
						},
						[2] = {
							['y'] = 684603.9375,
							['type'] = 'M48 Chaparral',
							['name'] = 'Kutaisi SR SAM/2',
							['heading'] = 309.70826406259,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -283340.78125,
						},
						[3] = {
							['y'] = 684867.3125,
							['type'] = 'M6 Linebacker',
							['name'] = 'Kutaisi SR SAM/3',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285236.40625,
						},
						[4] = {
							['y'] = 683342.31249996,
							['type'] = 'M1097 Avenger',
							['name'] = 'Kutaisi SR SAM/4',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285860.71874997,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi SR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 682851.34910483,
							['x'] = -285346.97345295,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 14,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 682851.375,
							['type'] = 'Vulcan',
							['name'] = 'Kutaisi AAA/1',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285346.96875,
						},
						[2] = {
							['y'] = 683908.125,
							['type'] = 'Vulcan',
							['name'] = 'Kutaisi AAA/2',
							['heading'] = 336.00001213196,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284690.15625,
						},
						[3] = {
							['y'] = 684987.625,
							['type'] = 'Vulcan',
							['name'] = 'Kutaisi AAA/3',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284706.40625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi AAA',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 684371.70282448,
							['x'] = -284977.54518433,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 684371.68750001,
							['type'] = 'Tor 9A331',
							['name'] = 'Kutaisi MR SAM/1',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284977.53124987,
						},
						[2] = {
							['y'] = 683604.87500001,
							['type'] = 'Tor 9A331',
							['name'] = 'Kutaisi MR SAM/2',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285123.09374987,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 684437.73758922,
							['x'] = -283217.05701419,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 15,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 684437.74999998,
							['type'] = 'M1045 HMMWV TOW',
							['name'] = 'Kutaisi AT/1',
							['heading'] = 332.525575585,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -283217.06249996,
						},
						[2] = {
							['y'] = 683719.74999994,
							['type'] = 'M-2 Bradley',
							['name'] = 'Kutaisi AT/2',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285572.81249989,
						},
						[3] = {
							['y'] = 682379.43749994,
							['type'] = 'M1134 Stryker ATGM',
							['name'] = 'Kutaisi AT/3',
							['heading'] = 332.52557310041,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -284341.93749989,
						},
						[4] = {
							['y'] = 685159.1875,
							['type'] = 'M-1 Abrams',
							['name'] = 'Kutaisi AT/4',
							['heading'] = 332.52557467598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -285176.1875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Kutaisi Patriot'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 43,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 684332.48022606,
							['x'] = -286228.98414672,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 684332.5,
							['type'] = 'Patriot AMG',
							['name'] = 'Kutaisi Patriot/1',
							['heading'] = 36.999996905837,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286228.96875,
						},
						[2] = {
							['y'] = 684321.6875,
							['type'] = 'Patriot cp',
							['name'] = 'Kutaisi Patriot/2',
							['heading'] = 217.00000168852,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286242.34375,
						},
						[3] = {
							['y'] = 684255.8125,
							['type'] = 'Patriot AMG',
							['name'] = 'Kutaisi Patriot/3',
							['heading'] = 37.000002321912,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286220.59375,
						},
						[4] = {
							['y'] = 684245.375,
							['type'] = 'Patriot ECS',
							['name'] = 'Kutaisi Patriot/4',
							['heading'] = 217.0000071046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286234.90625,
						},
						[5] = {
							['y'] = 684338.5625,
							['type'] = 'Patriot EPP',
							['name'] = 'Kutaisi Patriot/5',
							['heading'] = 36.999996905837,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286244.65625,
						},
						[6] = {
							['y'] = 684262.5625,
							['type'] = 'Patriot EPP',
							['name'] = 'Kutaisi Patriot/6',
							['heading'] = 37.000002321912,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286238.1875,
						},
						[7] = {
							['y'] = 684227.375,
							['type'] = 'Patriot str',
							['name'] = 'Kutaisi Patriot/7',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286259.25,
						},
						[8] = {
							['y'] = 684188.375,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/8',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286475.28125,
						},
						[9] = {
							['y'] = 683999.25,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/9',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286372.125,
						},
						[10] = {
							['y'] = 684064.75,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/10',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286420.03125,
						},
						[11] = {
							['y'] = 684208.5,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/11',
							['heading'] = 216.9999971515,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286525.5,
						},
						[12] = {
							['y'] = 684137.625,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/12',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286474.25,
						},
						[13] = {
							['y'] = 684114.875,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/13',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286419.90625,
						},
						[14] = {
							['y'] = 684048.8125,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/14',
							['heading'] = 217.00000193417,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286370.84375,
						},
						[15] = {
							['y'] = 684251.9375,
							['type'] = 'Patriot ln',
							['name'] = 'Kutaisi Patriot/15',
							['heading'] = 216.9999971515,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -286521.03125,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Kutaisi Patriot',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'open',
			['priority'] = 'strategic',
		},
		['Sukhumi-Babushara'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'blue',
			['groundUnits'] = {
			},
			['groups'] = {
				['Sukhumi-Babushara MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 564843.28629619,
							['x'] = -220936.39352667,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 564843.31250001,
							['type'] = 'Tor 9A331',
							['name'] = 'Sukhumi-Babushara MR SAM/1',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220936.39062487,
						},
						[2] = {
							['y'] = 563773.31250001,
							['type'] = 'Tor 9A331',
							['name'] = 'Sukhumi-Babushara MR SAM/2',
							['heading'] = 5.2790847607376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220421.37499987,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara Patriot'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 565890.83936804,
							['x'] = -220712.3698011,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 565890.8125,
							['type'] = 'Patriot AMG',
							['name'] = 'Sukhumi-Babushara Patriot/1',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220712.375,
						},
						[2] = {
							['y'] = 565944.6875,
							['type'] = 'Patriot cp',
							['name'] = 'Sukhumi-Babushara Patriot/2',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220739.90625,
						},
						[3] = {
							['y'] = 565835.3125,
							['type'] = 'Patriot AMG',
							['name'] = 'Sukhumi-Babushara Patriot/3',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220788.34375,
						},
						[4] = {
							['y'] = 565878.1875,
							['type'] = 'Patriot ECS',
							['name'] = 'Sukhumi-Babushara Patriot/4',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220847.0625,
						},
						[5] = {
							['y'] = 565921.75,
							['type'] = 'Patriot EPP',
							['name'] = 'Sukhumi-Babushara Patriot/5',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220663.109375,
						},
						[6] = {
							['y'] = 565895.125,
							['type'] = 'Patriot EPP',
							['name'] = 'Sukhumi-Babushara Patriot/6',
							['heading'] = 310.99998689239,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220782.75,
						},
						[7] = {
							['y'] = 565993.5,
							['type'] = 'Patriot str',
							['name'] = 'Sukhumi-Babushara Patriot/7',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220821.046875,
						},
						[8] = {
							['y'] = 566126.5,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/8',
							['heading'] = 130.9999917103,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220873.296875,
						},
						[9] = {
							['y'] = 566010.375,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/9',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221054.78125,
						},
						[10] = {
							['y'] = 566062.75,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/10',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220992.796875,
						},
						[11] = {
							['y'] = 566178,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/11',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220856.734375,
						},
						[12] = {
							['y'] = 566121.9375,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/12',
							['heading'] = 130.9999917103,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220923.84375,
						},
						[13] = {
							['y'] = 566066.125,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/13',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220942.734375,
						},
						[14] = {
							['y'] = 566012.5625,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/14',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221005.21875,
						},
						[15] = {
							['y'] = 566176.5625,
							['type'] = 'Patriot ln',
							['name'] = 'Sukhumi-Babushara Patriot/15',
							['heading'] = 130.99999462461,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220813.09375,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara Patriot',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara SR AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 565107.79717522,
							['x'] = -219411.09393482,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 15,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 565107.81249998,
							['type'] = 'M1045 HMMWV TOW',
							['name'] = 'Sukhumi-Babushara SR AT/1',
							['heading'] = 332.52557279747,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219411.09374996,
						},
						[2] = {
							['y'] = 564115.43749994,
							['type'] = 'M-2 Bradley',
							['name'] = 'Sukhumi-Babushara SR AT/2',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219401.73437489,
						},
						[3] = {
							['y'] = 563319.06249994,
							['type'] = 'M1134 Stryker ATGM',
							['name'] = 'Sukhumi-Babushara SR AT/3',
							['heading'] = 332.52557461538,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219553.18749989,
						},
						[4] = {
							['y'] = 565553.5625,
							['type'] = 'M-1 Abrams',
							['name'] = 'Sukhumi-Babushara SR AT/4',
							['heading'] = 332.52557467598,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220355.625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara SR AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara SR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 563903.6117348,
							['x'] = -218983.047397,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 563903.625,
							['type'] = 'M48 Chaparral',
							['name'] = 'Sukhumi-Babushara SR SAM/1',
							['heading'] = 309.70826219731,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -218983.046875,
						},
						[2] = {
							['y'] = 563855.25,
							['type'] = 'M48 Chaparral',
							['name'] = 'Sukhumi-Babushara SR SAM/2',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221891.8125,
						},
						[3] = {
							['y'] = 565779.8125,
							['type'] = 'M6 Linebacker',
							['name'] = 'Sukhumi-Babushara SR SAM/3',
							['heading'] = 309.70825905646,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219311.234375,
						},
						[4] = {
							['y'] = 566293.93749996,
							['type'] = 'M1097 Avenger',
							['name'] = 'Sukhumi-Babushara SR SAM/4',
							['heading'] = 309.70826232619,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220325.23437497,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara SR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Sukhumi-Babushara SR AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 13,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 564301.44979463,
							['x'] = -220349.54349926,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 14,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 564301.4375,
							['type'] = 'Vulcan',
							['name'] = 'Sukhumi-Babushara SR AAA/1',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -220349.546875,
						},
						[2] = {
							['y'] = 562663.375,
							['type'] = 'Vulcan',
							['name'] = 'Sukhumi-Babushara SR AAA/2',
							['heading'] = 336.00000997658,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -219887.859375,
						},
						[3] = {
							['y'] = 565867.75,
							['type'] = 'Vulcan',
							['name'] = 'Sukhumi-Babushara SR AAA/3',
							['heading'] = 336.00001036344,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -221471.421875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Sukhumi-Babushara SR AAA',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'open',
			['priority'] = 'strategic',
		},
		['Nalchik'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'red',
			['groundUnits'] = {
			},
			['groups'] = {
				['Nalchik AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 314,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 759176.60934685,
							['x'] = -125758.11683172,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 759176.625,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Nalchik AAA/1',
							['heading'] = 32.307333818172,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125758.1171875,
						},
						[2] = {
							['y'] = 760478.875,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Nalchik AAA/2',
							['heading'] = 32.307333648384,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124636.046875,
						},
						[3] = {
							['y'] = 761578,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Nalchik AAA/3',
							['heading'] = 32.307332926778,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123885.6640625,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik AAA',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 519,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 760197.13435023,
							['x'] = -125210.5637512,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 760197.12500008,
							['type'] = 'Tor 9A331',
							['name'] = 'Nalchik MR SAM/1',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125210.56250009,
						},
						[2] = {
							['y'] = 761082.06250008,
							['type'] = 'Tor 9A331',
							['name'] = 'Nalchik MR SAM/2',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124626.25000009,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 542,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 760201.89537913,
							['x'] = -125702.97325764,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 760201.8750001,
							['type'] = 'T-90',
							['name'] = 'Nalchik AT/1',
							['heading'] = 123.69006942033,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125702.97656257,
						},
						[2] = {
							['y'] = 761823.8125001,
							['type'] = 'T-90',
							['name'] = 'Nalchik AT/2',
							['heading'] = 123.69007131469,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124125.98437507,
						},
						[3] = {
							['y'] = 760779.4375001,
							['type'] = 'T-72B',
							['name'] = 'Nalchik AT/3',
							['heading'] = 123.69007036751,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123607.24218757,
						},
						[4] = {
							['y'] = 759074.49999995,
							['type'] = 'BRDM-2',
							['name'] = 'Nalchik AT/4',
							['heading'] = 123.69006847316,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125547.35156247,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik IR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 759640.77376426,
							['x'] = -124981.50469322,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 759640.75,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/1',
							['heading'] = 110.55604561929,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124981.5078125,
						},
						[2] = {
							['y'] = 760160.8125,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/2',
							['heading'] = 110.55604481988,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123336.609375,
						},
						[3] = {
							['y'] = 760963.375,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/3',
							['heading'] = 110.55604402046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -125433.75,
						},
						[4] = {
							['y'] = 761185.9375,
							['type'] = 'Strela-10M3',
							['name'] = 'Nalchik IR SAM/4',
							['heading'] = 110.55604561929,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124084.375,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik IR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Nalchik S300'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 759906.76150032,
							['x'] = -124109.86039959,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 759906.74999994,
							['type'] = 'S-300PS 54K6 cp',
							['name'] = 'Nalchik S300/1',
							['heading'] = 25.463346456722,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124109.85937512,
						},
						[2] = {
							['y'] = 759920.24999994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Nalchik S300/2',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -123954.89062512,
						},
						[3] = {
							['y'] = 759669.81249994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Nalchik S300/3',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124106.17187512,
						},
						[4] = {
							['y'] = 760026.5,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Nalchik S300/4',
							['heading'] = 25.463344841632,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124020.0078125,
						},
						[5] = {
							['y'] = 759671.8125,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Nalchik S300/5',
							['heading'] = 25.463344841632,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124205.3671875,
						},
						[6] = {
							['y'] = 760308.06249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/6',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124409.35937512,
						},
						[7] = {
							['y'] = 760274.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/7',
							['heading'] = 25.463344988458,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124495.90625012,
						},
						[8] = {
							['y'] = 760372.62499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/8',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124481.23437512,
						},
						[9] = {
							['y'] = 760215.68749994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/9',
							['heading'] = 25.463346309896,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124424.03125012,
						},
						[10] = {
							['y'] = 760227.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/10',
							['heading'] = 25.463346897203,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124644.06250012,
						},
						[11] = {
							['y'] = 760139.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/11',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124556.04687512,
						},
						[12] = {
							['y'] = 760108.56249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/12',
							['heading'] = 25.463344768219,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124693.93750012,
						},
						[13] = {
							['y'] = 760068.99999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Nalchik S300/13',
							['heading'] = 25.463346309896,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -124611.78906262,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Nalchik S300',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'open',
			['priority'] = 'strategic',
		},
		['Mineralnye Vody'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'red',
			['groundUnits'] = {
			},
			['groups'] = {
				['Mineralnye Vody AT'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 542,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 707000.31182306,
							['x'] = -53097.930409503,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 707000.3125001,
							['type'] = 'T-90',
							['name'] = 'Mineralnye Vody AT/1',
							['heading'] = 123.69006942033,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -53097.929687567,
						},
						[2] = {
							['y'] = 707378.0000001,
							['type'] = 'T-90',
							['name'] = 'Mineralnye Vody AT/2',
							['heading'] = 123.6900751034,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50844.156250067,
						},
						[3] = {
							['y'] = 705785.1250001,
							['type'] = 'T-72B',
							['name'] = 'Mineralnye Vody AT/3',
							['heading'] = 123.69006942033,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -53037.027343817,
						},
						[4] = {
							['y'] = 703510.68749995,
							['type'] = 'BRDM-2',
							['name'] = 'Mineralnye Vody AT/4',
							['heading'] = 123.69007036751,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51446.812499967,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody AT',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody  MR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 519,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 703975.2341117,
							['x'] = -51028.504921144,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 703975.25000008,
							['type'] = 'Tor 9A331',
							['name'] = 'Mineralnye Vody  MR SAM/1',
							['heading'] = 137.72631168308,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51028.503906343,
						},
						[2] = {
							['y'] = 707719.25000008,
							['type'] = 'Tor 9A331',
							['name'] = 'Mineralnye Vody  MR SAM/2',
							['heading'] = 137.72631145337,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -52393.011718843,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody  MR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody S300'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 320,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 705992.64543785,
							['x'] = -50890.11999127,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 705992.62499994,
							['type'] = 'S-300PS 54K6 cp',
							['name'] = 'Mineralnye Vody S300/1',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50890.121093633,
						},
						[2] = {
							['y'] = 706115.49999994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Mineralnye Vody S300/2',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51080.894531133,
						},
						[3] = {
							['y'] = 706206.93749994,
							['type'] = 'S-300PS 64H6E sr',
							['name'] = 'Mineralnye Vody S300/3',
							['heading'] = 151.99999953786,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50738.546874883,
						},
						[4] = {
							['y'] = 705856.75,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Mineralnye Vody S300/4',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51083.5078125,
						},
						[5] = {
							['y'] = 706008.3125,
							['type'] = 'S-300PS 40B6M tr',
							['name'] = 'Mineralnye Vody S300/5',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50568.67578125,
						},
						[6] = {
							['y'] = 705268.74999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/6',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50688.890624883,
						},
						[7] = {
							['y'] = 705401.99999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/7',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50843.078124883,
						},
						[8] = {
							['y'] = 705034.74999994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/8',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50652.105468633,
						},
						[9] = {
							['y'] = 705582.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/9',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50793.425781133,
						},
						[10] = {
							['y'] = 705224.31249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/10',
							['heading'] = 151.99999885963,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50424.941406133,
						},
						[11] = {
							['y'] = 705425.56249994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/11',
							['heading'] = 151.99999693267,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50351.769531133,
						},
						[12] = {
							['y'] = 705490.87499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/12',
							['heading'] = 151.99999693267,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50511.183593633,
						},
						[13] = {
							['y'] = 705663.37499994,
							['type'] = 'S-300PS 5P85D ln',
							['name'] = 'Mineralnye Vody S300/13',
							['heading'] = 151.99999815442,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50453.687499883,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody S300',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody IR SAM'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 430,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 703616.49370942,
							['x'] = -51222.314386361,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 703616.5,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/1',
							['heading'] = 110.55604481988,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51222.3125,
						},
						[2] = {
							['y'] = 704601.875,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/2',
							['heading'] = 110.55604561929,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -49784.48828125,
						},
						[3] = {
							['y'] = 706599.3125,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/3',
							['heading'] = 110.55604442015,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -52496.3203125,
						},
						[4] = {
							['y'] = 707661.75,
							['type'] = 'Strela-10M3',
							['name'] = 'Mineralnye Vody IR SAM/4',
							['heading'] = 110.55604402046,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51594.44921875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody IR SAM',
					['category'] = 2,
					['task'] = {
					},
				},
				['Mineralnye Vody AAA'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 314,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 703646.77038064,
							['x'] = -50362.327171642,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 703646.75,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Mineralnye Vody AAA/1',
							['heading'] = 32.307339125294,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -50362.328125,
						},
						[2] = {
							['y'] = 705682.1875,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Mineralnye Vody AAA/2',
							['heading'] = 32.3073350068,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -52445.609375,
						},
						[3] = {
							['y'] = 707979,
							['type'] = 'ZSU-23-4 Shilka',
							['name'] = 'Mineralnye Vody AAA/3',
							['heading'] = 32.307333223845,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -51889.82421875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Mineralnye Vody AAA',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'open',
			['priority'] = 'strategic',
		},
	},
	['FARP'] = {
		['FARP Balkariya'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Inguri Valley'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['FARP Inguri Valley Groundcrew Ammo'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647807.23435052,
							['x'] = -203158.4649719,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647807.25,
							['type'] = 'M 818',
							['name'] = 'FARP Inguri Valley Groundcrew Ammo/1',
							['heading'] = 100.08059587015,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203158.46875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Inguri Valley Groundcrew Ammo',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Inguri Valley Fortification'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647749.69049604,
							['x'] = -203280.52712451,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647749.6875,
							['type'] = 'Sandbox',
							['name'] = 'FARP Inguri Valley Fortification/1',
							['heading'] = 100.08059684807,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203280.53125,
						},
						[2] = {
							['y'] = 648573.4375,
							['type'] = 'outpost_road',
							['name'] = 'FARP Inguri Valley Fortification/2',
							['heading'] = 75.999997189002,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -202985.921875,
						},
						[3] = {
							['y'] = 647503.75,
							['type'] = 'outpost_road',
							['name'] = 'FARP Inguri Valley Fortification/3',
							['heading'] = 250.00000532235,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203421.515625,
						},
						[4] = {
							['y'] = 648593.1875,
							['type'] = 'house2arm',
							['name'] = 'FARP Inguri Valley Fortification/4',
							['heading'] = 100.08059432629,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -202917.34375,
						},
						[5] = {
							['y'] = 647434,
							['type'] = 'house2arm',
							['name'] = 'FARP Inguri Valley Fortification/5',
							['heading'] = 100.08059684807,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203376.125,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Inguri Valley Fortification',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Inguri Valley Air Defense'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647944.3808922,
							['x'] = -203537.20493286,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647944.37500012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Inguri Valley Air Defense/1',
							['heading'] = 100.08059585322,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203537.20312502,
						},
						[2] = {
							['y'] = 647485.68750012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Inguri Valley Air Defense/2',
							['heading'] = 100.08059546182,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -202875.84375002,
						},
						[3] = {
							['y'] = 648176.81250012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Inguri Valley Air Defense/3',
							['heading'] = 100.08059404092,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -202399.42187502,
						},
						[4] = {
							['y'] = 647436.68750012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Inguri Valley Air Defense/4',
							['heading'] = 100.08059602491,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203192.04687502,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Inguri Valley Air Defense',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Inguri Valley Groundcrew Fuel'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647878.14119178,
							['x'] = -203130.51082783,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647878.125,
							['type'] = 'M978 HEMTT Tanker',
							['name'] = 'FARP Inguri Valley Groundcrew Fuel/1',
							['heading'] = 100.08059808123,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203130.515625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Inguri Valley Groundcrew Fuel',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Inguri Valley Groundcrew ATC'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 647878.76789118,
							['x'] = -203164.42610028,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 17,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 647878.75000005,
							['type'] = 'M1025 HMMWV',
							['name'] = 'FARP Inguri Valley Groundcrew ATC/1',
							['heading'] = 100.08059547624,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203164.42187501,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Inguri Valley Groundcrew ATC',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Inguri Valley SR IR Defense'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 648101.31668432,
							['x'] = -203265.76528288,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 648101.3125,
							['type'] = 'M48 Chaparral',
							['name'] = 'FARP Inguri Valley SR IR Defense/1',
							['heading'] = 309.70826209515,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203265.765625,
						},
						[2] = {
							['y'] = 647787.375,
							['type'] = 'M48 Chaparral',
							['name'] = 'FARP Inguri Valley SR IR Defense/2',
							['heading'] = 309.7082622643,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -202703.4375,
						},
						[3] = {
							['y'] = 648399.375,
							['type'] = 'M6 Linebacker',
							['name'] = 'FARP Inguri Valley SR IR Defense/3',
							['heading'] = 309.70826255287,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203007.453125,
						},
						[4] = {
							['y'] = 647999.99999996,
							['type'] = 'M1097 Avenger',
							['name'] = 'FARP Inguri Valley SR IR Defense/4',
							['heading'] = 309.70826114329,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -203108.78124997,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Inguri Valley SR IR Defense',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Tsvirmi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Nigniy'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Elhotovo'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Tkvarcheli'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['FARP Tkvarcheli Groundcrew Ammo'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 606697.20804462,
							['x'] = -216335.22473696,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 606697.1875,
							['type'] = 'M 818',
							['name'] = 'FARP Tkvarcheli Groundcrew Ammo/1',
							['heading'] = 100.08059391268,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216335.21875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Tkvarcheli Groundcrew Ammo',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Tkvarcheli Groundcrew Fuel'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 606871.49130037,
							['x'] = -216286.34121959,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 606871.5,
							['type'] = 'M978 HEMTT Tanker',
							['name'] = 'FARP Tkvarcheli Groundcrew Fuel/1',
							['heading'] = 100.08059150343,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216286.34375,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Tkvarcheli Groundcrew Fuel',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Tkvarcheli Air Defense'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 607124.60847108,
							['x'] = -216905.79525917,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 607124.62500012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Tkvarcheli Air Defense/1',
							['heading'] = 100.08059746435,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216905.79687502,
						},
						[2] = {
							['y'] = 607093.25000012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Tkvarcheli Air Defense/2',
							['heading'] = 100.08059673527,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -215427.48437502,
						},
						[3] = {
							['y'] = 607289.31250012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Tkvarcheli Air Defense/3',
							['heading'] = 100.08059675457,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216062.71875002,
						},
						[4] = {
							['y'] = 605669.81250012,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Tkvarcheli Air Defense/4',
							['heading'] = 100.08059585803,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216427.40625002,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Tkvarcheli Air Defense',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Tkvarcheli Fortification'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 607161.93891897,
							['x'] = -216101.190784,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 607161.9375,
							['type'] = 'Sandbox',
							['name'] = 'FARP Tkvarcheli Fortification/1',
							['heading'] = 100.08059768866,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216101.1875,
						},
						[2] = {
							['y'] = 607391.1875,
							['type'] = 'outpost_road',
							['name'] = 'FARP Tkvarcheli Fortification/2',
							['heading'] = 86.999999133901,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216225.546875,
						},
						[3] = {
							['y'] = 605732.5,
							['type'] = 'outpost_road',
							['name'] = 'FARP Tkvarcheli Fortification/3',
							['heading'] = 228.99999607648,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216480.9375,
						},
						[4] = {
							['y'] = 607388.1875,
							['type'] = 'house2arm',
							['name'] = 'FARP Tkvarcheli Fortification/4',
							['heading'] = 100.08059684807,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216273.8125,
						},
						[5] = {
							['y'] = 605741.125,
							['type'] = 'house2arm',
							['name'] = 'FARP Tkvarcheli Fortification/5',
							['heading'] = 100.08059684807,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216515.65625,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Tkvarcheli Fortification',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Tkvarcheli Groundcrew ATC'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 606773.98320966,
							['x'] = -216282.91988308,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 17,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 606774.00000005,
							['type'] = 'M1025 HMMWV',
							['name'] = 'FARP Tkvarcheli Groundcrew ATC/1',
							['heading'] = 100.08059245784,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216282.92187501,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Tkvarcheli Groundcrew ATC',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Tkvarcheli SR IR Defense'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 606664.75865552,
							['x'] = -217067.51862527,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 13,
													},
												},
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 606664.75,
							['type'] = 'M48 Chaparral',
							['name'] = 'FARP Tkvarcheli SR IR Defense/1',
							['heading'] = 309.70826115455,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -217067.515625,
						},
						[2] = {
							['y'] = 605986.4375,
							['type'] = 'M48 Chaparral',
							['name'] = 'FARP Tkvarcheli SR IR Defense/2',
							['heading'] = 309.70826098991,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216317.453125,
						},
						[3] = {
							['y'] = 607274.375,
							['type'] = 'M6 Linebacker',
							['name'] = 'FARP Tkvarcheli SR IR Defense/3',
							['heading'] = 309.70826713496,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216302.515625,
						},
						[4] = {
							['y'] = 606619.93749996,
							['type'] = 'M1097 Avenger',
							['name'] = 'FARP Tkvarcheli SR IR Defense/4',
							['heading'] = 309.70826243204,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -216451.92187497,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'FARP Tkvarcheli SR IR Defense',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Progress'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Oche'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Elbrus'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['FARP Elbrus Groundcrew Ammo'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 672718.47620849,
							['x'] = -164719.87896411,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 672718.5,
							['type'] = 'Ural-375',
							['name'] = 'FARP Elbrus Groundcrew Ammo/1',
							['heading'] = 100.08059437893,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164719.875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Elbrus Groundcrew Ammo',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Elbrus Air Defense'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 1023,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 672399.23965952,
							['x'] = -165053.03652343,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 672399.24999992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Elbrus Air Defense/1',
							['heading'] = 217.46055113519,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -165053.0312501,
						},
						[2] = {
							['y'] = 672021.81249992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Elbrus Air Defense/2',
							['heading'] = 217.46055169171,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164752.2500001,
						},
						[3] = {
							['y'] = 672987.06249992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Elbrus Air Defense/3',
							['heading'] = 217.46055280684,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164329.5781251,
						},
						[4] = {
							['y'] = 673171.81249992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Elbrus Air Defense/4',
							['heading'] = 217.46055116392,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -165023.5468751,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Elbrus Air Defense',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Elbrus Groundcrew Fuel'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 672739.77310976,
							['x'] = -164754.41447967,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 672739.75,
							['type'] = 'ATZ-10',
							['name'] = 'FARP Elbrus Groundcrew Fuel/1',
							['heading'] = 100.0805946968,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164754.421875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Elbrus Groundcrew Fuel',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Elbrus Fortification'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 1053,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 672938.72998989,
							['x'] = -164818.83186395,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 672938.75,
							['type'] = 'Sandbox',
							['name'] = 'FARP Elbrus Fortification/1',
							['heading'] = 75.999997189002,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164818.828125,
						},
						[2] = {
							['y'] = 673205.625,
							['type'] = 'outpost_road',
							['name'] = 'FARP Elbrus Fortification/2',
							['heading'] = 135.99998385951,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164759.796875,
						},
						[3] = {
							['y'] = 672194.375,
							['type'] = 'outpost_road',
							['name'] = 'FARP Elbrus Fortification/3',
							['heading'] = 61.999994145376,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164338.953125,
						},
						[4] = {
							['y'] = 672148.3125,
							['type'] = 'house2arm',
							['name'] = 'FARP Elbrus Fortification/4',
							['heading'] = 75.999997189002,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164330.03125,
						},
						[5] = {
							['y'] = 673250,
							['type'] = 'house2arm',
							['name'] = 'FARP Elbrus Fortification/5',
							['heading'] = 75.999998017415,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164760.390625,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Elbrus Fortification',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Elbrus Air Defense IR'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 1023,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 673159.26301213,
							['x'] = -164840.79284552,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 673159.25,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Elbrus Air Defense IR/1',
							['heading'] = 217.46055697297,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164840.796875,
						},
						[2] = {
							['y'] = 672851.75,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Elbrus Air Defense IR/2',
							['heading'] = 217.4605635709,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164289.078125,
						},
						[3] = {
							['y'] = 672688.0625,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Elbrus Air Defense IR/3',
							['heading'] = 217.46055907038,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -165135.421875,
						},
						[4] = {
							['y'] = 672129.1875,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Elbrus Air Defense IR/4',
							['heading'] = 217.46056197196,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164476.71875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Elbrus Air Defense IR',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Elbrus Groundcrew ATC'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 672703.51081841,
							['x'] = -164781.46730019,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 672703.5,
							['type'] = 'SKP-11',
							['name'] = 'FARP Elbrus Groundcrew ATC/1',
							['heading'] = 100.08059340668,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -164781.46875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Elbrus Groundcrew ATC',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Kodori'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Bylym'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['FARP Bylym Groundcrew ATC'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 712772.88320054,
							['x'] = -136170.09264038,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 712772.875,
							['type'] = 'SKP-11',
							['name'] = 'FARP Bylym Groundcrew ATC/1',
							['heading'] = 100.08059761392,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136170.09375,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Bylym Groundcrew ATC',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Bylym Groundcrew Ammo'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 712752.61000131,
							['x'] = -136192.83818097,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 712752.625,
							['type'] = 'Ural-375',
							['name'] = 'FARP Bylym Groundcrew Ammo/1',
							['heading'] = 100.08059718427,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136192.84375,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Bylym Groundcrew Ammo',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Bylym Air Defense IR'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 1023,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 711877.04980987,
							['x'] = -136338.80356007,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 711877.0625,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Bylym Air Defense IR/1',
							['heading'] = 217.46056123402,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136338.796875,
						},
						[2] = {
							['y'] = 712844.375,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Bylym Air Defense IR/2',
							['heading'] = 217.46056286349,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136642.578125,
						},
						[3] = {
							['y'] = 712062,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Bylym Air Defense IR/3',
							['heading'] = 217.46055718696,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136583.171875,
						},
						[4] = {
							['y'] = 713071.75,
							['type'] = 'Strela-10M3',
							['name'] = 'FARP Bylym Air Defense IR/4',
							['heading'] = 217.46055355762,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -135953.578125,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Bylym Air Defense IR',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Bylym Fortification'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 1053,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 711910.14331669,
							['x'] = -136483.89701919,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 711910.125,
							['type'] = 'Sandbox',
							['name'] = 'FARP Bylym Fortification/1',
							['heading'] = 75.999999674241,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136483.890625,
						},
						[2] = {
							['y'] = 711840.5625,
							['type'] = 'outpost_road',
							['name'] = 'FARP Bylym Fortification/2',
							['heading'] = 231.00000538672,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136641.75,
						},
						[3] = {
							['y'] = 713297.8125,
							['type'] = 'outpost_road',
							['name'] = 'FARP Bylym Fortification/3',
							['heading'] = 61.999997160726,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -135898.25,
						},
						[4] = {
							['y'] = 713295.8125,
							['type'] = 'house2arm',
							['name'] = 'FARP Bylym Fortification/4',
							['heading'] = 75.999999674241,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -135846.640625,
						},
						[5] = {
							['y'] = 711806.0625,
							['type'] = 'house2arm',
							['name'] = 'FARP Bylym Fortification/5',
							['heading'] = 75.999998845828,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136611.96875,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Bylym Fortification',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Bylym Groundcrew Fuel'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 164,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 712724.91977799,
							['x'] = -136192.83818097,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 712724.9375,
							['type'] = 'ATZ-10',
							['name'] = 'FARP Bylym Groundcrew Fuel/1',
							['heading'] = 100.08059219646,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136192.84375,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Bylym Groundcrew Fuel',
					['category'] = 2,
					['task'] = {
					},
				},
				['FARP Bylym Air Defense'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 1023,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 713229.81963444,
							['x'] = -135768.18262443,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 713229.81249992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Bylym Air Defense/1',
							['heading'] = 217.46055415595,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -135768.1875001,
						},
						[2] = {
							['y'] = 711719.49999992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Bylym Air Defense/2',
							['heading'] = 217.46055411975,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136563.3281251,
						},
						[3] = {
							['y'] = 711988.43749992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Bylym Air Defense/3',
							['heading'] = 217.46056022801,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -136035.9375001,
						},
						[4] = {
							['y'] = 712976.74999992,
							['type'] = 'Tor 9A331',
							['name'] = 'FARP Bylym Air Defense/4',
							['heading'] = 217.46055535341,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -135532.7031251,
						},
					},
					['coalition'] = 'red',
					['name'] = 'FARP Bylym Air Defense',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Derchi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Makara'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Lentehi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Oni'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['FARP Zugdidi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['Communication'] = {
		['Antenna Inguri'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Antenna Inguri Radar'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 4566,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 654363.24200426,
							['x'] = -217250.3772553,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'EWR',
											['enabled'] = true,
											['params'] = {
											},
										},
									},
								},
							},
						},
					},
					['country'] = 2,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 654363.25,
							['type'] = '55G6 EWR',
							['name'] = 'Antenna Inguri Radar/1',
							['heading'] = 246.03751379942,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -217250.375,
						},
						[2] = {
							['y'] = 654380.68749988,
							['type'] = 'Tor 9A331',
							['name'] = 'Antenna Inguri Radar/2',
							['heading'] = 246.03751518981,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -217299.01562505,
						},
						[3] = {
							['y'] = 654403.375,
							['type'] = 'SA-18 Igla-S comm',
							['name'] = 'Antenna Inguri Radar/3',
							['heading'] = 246.03751223905,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -217214.96875,
						},
					},
					['coalition'] = 'blue',
					['name'] = 'Antenna Inguri Radar',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna East Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Zugdidi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Oni'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Ambrolauri'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna 2'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Tkvarcheli'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna West Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Zvirmi'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Elbrus'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Oche'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Balkariya'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
				['Antenna Balkariya Radar'] = {
					['visible'] = false,
					['route'] = {
						[1] = {
							['alt'] = 4566,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 732730.51806015,
							['x'] = -177572.79144537,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'EWR',
											['enabled'] = true,
											['params'] = {
											},
										},
									},
								},
							},
						},
					},
					['country'] = 0,
					['hidden'] = false,
					['units'] = {
						[1] = {
							['y'] = 732730.5,
							['type'] = '55G6 EWR',
							['name'] = 'Antenna Balkariya Radar/1',
							['heading'] = 246.03751067867,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -177572.796875,
						},
						[2] = {
							['y'] = 732885.37499988,
							['type'] = 'Tor 9A331',
							['name'] = 'Antenna Balkariya Radar/2',
							['heading'] = 246.03751030595,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -177482.96875005,
						},
						[3] = {
							['y'] = 732774.5,
							['type'] = 'SA-18 Igla-S manpad',
							['name'] = 'Antenna Balkariya Radar/3',
							['heading'] = 246.03751223905,
							['playerCanDrive'] = false,
							['skill'] = 'Excellent',
							['x'] = -177518.78125,
						},
					},
					['coalition'] = 'red',
					['name'] = 'Antenna Balkariya Radar',
					['category'] = 2,
					['task'] = {
					},
				},
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Kodori'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['Antenna Bulls'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['Main Targets'] = {
		['Inguri-Dam'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['Bunker'] = {
		['East Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
		['West Bunker'] = {
			['underAttack'] = false,
			['slotIDs'] = {
			},
			['coa'] = 'neutral',
			['groundUnits'] = {
			},
			['groups'] = {
			},
			['status'] = 'closed',
			['priority'] = 'strategic',
		},
	},
	['properties'] = {
		['serverName'] = '- DEADBEEFS LAIR -',
		['restartTime'] = 0,
		['ejectedPilots'] = {
		},
		['playerLimit'] = {
		},
		['startTimer'] = 0,
		['campaignSecs'] = 0,
		['droppedCrates'] = {
		},
		['droppedSAMs'] = {
		},
		['lifeLimit'] = 5,
		['numSpawnedConvoys'] = {
			['blue'] = 0,
			['red'] = 0,
		},
		['timer'] = 0,
		['spawnedConvoys'] = {
		},
		['cashPile'] = {
		},
		['missionRuntime'] = 0,
		['convoys'] = {
			['blue'] = {
				['FARP Derchi'] = {
					['units'] = {
						[1] = {
							['y'] = 698389.75000001,
							['x'] = -263530.21874995,
							['heading'] = 15.665335318442,
						},
						[2] = {
							['y'] = 698367.62500001,
							['x'] = -263550.18749995,
							['heading'] = 15.665333345683,
						},
						[3] = {
							['y'] = 698346.43750001,
							['x'] = -263571.28124995,
							['heading'] = 15.665331694774,
						},
						[4] = {
							['y'] = 698326.50000001,
							['x'] = -263593.56249995,
							['heading'] = 15.665335319308,
						},
						[5] = {
							['y'] = 698307.31250001,
							['x'] = -263616.56249995,
							['heading'] = 15.665332731625,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 698389.74608955,
							['x'] = -263530.22758513,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 702517.12862396,
							['x'] = -248812.41955321,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Inguri Valley'] = {
					['units'] = {
						[1] = {
							['y'] = 640265.75000002,
							['x'] = -218865.51562496,
							['heading'] = 25.422529406398,
						},
						[2] = {
							['y'] = 640261.68750002,
							['x'] = -218895.26562496,
							['heading'] = 25.422529637716,
						},
						[3] = {
							['y'] = 640256.75000002,
							['x'] = -218924.76562496,
							['heading'] = 25.42253029015,
						},
						[4] = {
							['y'] = 640249.68750002,
							['x'] = -218953.81249996,
							['heading'] = 25.422530110924,
						},
						[5] = {
							['y'] = 640240.68750002,
							['x'] = -218982.24999996,
							['heading'] = 25.422528459148,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 669,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 640265.72931338,
							['x'] = -218865.51657182,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 647625.64730499,
							['x'] = -203381.28116333,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Oche'] = {
					['units'] = {
						[1] = {
							['y'] = 657874.37500002,
							['x'] = -265213.87499995,
							['heading'] = 22.117667156001,
						},
						[2] = {
							['y'] = 657848.75000002,
							['x'] = -265229.46874995,
							['heading'] = 22.117668737895,
						},
						[3] = {
							['y'] = 657823.12500002,
							['x'] = -265245.09374995,
							['heading'] = 22.117668247255,
						},
						[4] = {
							['y'] = 657797.56250002,
							['x'] = -265260.84374995,
							['heading'] = 22.117669050895,
						},
						[5] = {
							['y'] = 657772.18750002,
							['x'] = -265276.84374995,
							['heading'] = 22.117668788666,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 657874.36184205,
							['x'] = -265213.87308429,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 665559.26487747,
							['x'] = -246304.96878966,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Balkariya'] = {
					['units'] = {
						[1] = {
							['y'] = 756112.49999998,
							['x'] = -158843.04687504,
							['heading'] = 204.48485670932,
						},
						[2] = {
							['y'] = 756121.99999998,
							['x'] = -158814.70312504,
							['heading'] = 204.48485631322,
						},
						[3] = {
							['y'] = 756131.62499998,
							['x'] = -158786.18750004,
							['heading'] = 204.48485665024,
						},
						[4] = {
							['y'] = 756141.18749998,
							['x'] = -158757.73437504,
							['heading'] = 204.48485804778,
						},
						[5] = {
							['y'] = 756150.74999998,
							['x'] = -158729.23437504,
							['heading'] = 204.48485812356,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 756112.47486195,
							['x'] = -158843.05067316,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 749959.16479367,
							['x'] = -172354.7165627,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Tsvirmi'] = {
					['units'] = {
						[1] = {
							['y'] = 690759.12500005,
							['x'] = -187021.60937499,
							['heading'] = 79.556505211862,
						},
						[2] = {
							['y'] = 690737.12500005,
							['x'] = -187037.46874999,
							['heading'] = 79.556505695793,
						},
						[3] = {
							['y'] = 690712.81250005,
							['x'] = -187055.01562499,
							['heading'] = 79.556505211862,
						},
						[4] = {
							['y'] = 690688.50000005,
							['x'] = -187072.54687499,
							['heading'] = 79.55650669129,
						},
						[5] = {
							['y'] = 690664.12500005,
							['x'] = -187090.09374999,
							['heading'] = 79.55650669129,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 690759.14245663,
							['x'] = -187021.61233791,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 701941.04347771,
							['x'] = -184960.5744603,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Inguri-Dam'] = {
					['units'] = {
						[1] = {
							['y'] = 633375.81250002,
							['x'] = -244597.92187495,
							['heading'] = 18.386232345949,
						},
						[2] = {
							['y'] = 633354.93750002,
							['x'] = -244619.54687495,
							['heading'] = 18.386232925885,
						},
						[3] = {
							['y'] = 633334.00000002,
							['x'] = -244641.12499995,
							['heading'] = 18.386231369731,
						},
						[4] = {
							['y'] = 633312.93750002,
							['x'] = -244662.53124995,
							['heading'] = 18.386230365969,
						},
						[5] = {
							['y'] = 633291.68750002,
							['x'] = -244683.74999995,
							['heading'] = 18.386232630611,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 633375.81704651,
							['x'] = -244597.91928341,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 640169.97556623,
							['x'] = -224157.52718451,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Elbrus'] = {
					['units'] = {
						[1] = {
							['y'] = 692983.49999996,
							['x'] = -155766.14062502,
							['heading'] = 245.38849726581,
						},
						[2] = {
							['y'] = 693005.74999996,
							['x'] = -155746.00000002,
							['heading'] = 245.38849792477,
						},
						[3] = {
							['y'] = 693027.93749996,
							['x'] = -155725.93750002,
							['heading'] = 245.38849680053,
						},
						[4] = {
							['y'] = 693050.18749996,
							['x'] = -155705.84375002,
							['heading'] = 245.38849603964,
						},
						[5] = {
							['y'] = 693072.49999996,
							['x'] = -155685.78125002,
							['heading'] = 245.3884960898,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 692983.51094315,
							['x'] = -155766.14089796,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 673254.85314264,
							['x'] = -164803.41446128,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['East Bunker'] = {
					['units'] = {
						[1] = {
							['y'] = 806267.43749999,
							['x'] = -193627.06250005,
							['heading'] = 194.02240087466,
						},
						[2] = {
							['y'] = 806295.24999999,
							['x'] = -193615.93750005,
							['heading'] = 194.02239884854,
						},
						[3] = {
							['y'] = 806322.99999999,
							['x'] = -193604.81250005,
							['heading'] = 194.02240043482,
						},
						[4] = {
							['y'] = 806350.87499999,
							['x'] = -193593.65625005,
							['heading'] = 194.02240043482,
						},
						[5] = {
							['y'] = 806378.68749999,
							['x'] = -193582.50000005,
							['heading'] = 194.02240125825,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 806267.46676788,
							['x'] = -193627.06771752,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 801500.3951913,
							['x'] = -212714.95740077,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Zugdidi'] = {
					['units'] = {
						[1] = {
							['y'] = 625822.37500001,
							['x'] = -268160.12499995,
							['heading'] = 13.64011269893,
						},
						[2] = {
							['y'] = 625843.56250001,
							['x'] = -268181.31249995,
							['heading'] = 13.64011269893,
						},
						[3] = {
							['y'] = 625864.75000001,
							['x'] = -268202.46874995,
							['heading'] = 13.640112601579,
						},
						[4] = {
							['y'] = 625885.87500001,
							['x'] = -268223.68749995,
							['heading'] = 13.640114163617,
						},
						[5] = {
							['y'] = 625907.06250001,
							['x'] = -268244.90624995,
							['heading'] = 13.640114066267,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 625822.34694212,
							['x'] = -268160.12139438,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 630819.81970017,
							['x'] = -247566.14059679,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Bylym'] = {
					['units'] = {
						[1] = {
							['y'] = 728922.74999997,
							['x'] = -118704.74218754,
							['heading'] = 222.39500600081,
						},
						[2] = {
							['y'] = 728948.68749997,
							['x'] = -118689.85937504,
							['heading'] = 222.39500571443,
						},
						[3] = {
							['y'] = 728974.99999997,
							['x'] = -118675.42968754,
							['heading'] = 222.39500571443,
						},
						[4] = {
							['y'] = 729001.62499997,
							['x'] = -118661.54687504,
							['heading'] = 222.39500492131,
						},
						[5] = {
							['y'] = 729028.43749997,
							['x'] = -118648.18750004,
							['heading'] = 222.39500743441,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 728922.77251898,
							['x'] = -118704.74268652,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 713155.26523472,
							['x'] = -135975.39265291,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Elhotovo'] = {
					['units'] = {
						[1] = {
							['y'] = 821336.18749997,
							['x'] = -155217.23437496,
							['heading'] = 327.13941408017,
						},
						[2] = {
							['y'] = 821361.99999997,
							['x'] = -155232.34374996,
							['heading'] = 327.13941491757,
						},
						[3] = {
							['y'] = 821387.87499997,
							['x'] = -155247.48437496,
							['heading'] = 327.13941491757,
						},
						[4] = {
							['y'] = 821413.74999997,
							['x'] = -155262.64062496,
							['heading'] = 327.13941491757,
						},
						[5] = {
							['y'] = 821439.56249997,
							['x'] = -155277.76562496,
							['heading'] = 327.13941408017,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 821336.21698913,
							['x'] = -155217.22969356,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 809821.52098811,
							['x'] = -137391.34308655,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Lentehi'] = {
					['units'] = {
						[1] = {
							['y'] = 701209.68749998,
							['x'] = -228103.03124995,
							['heading'] = 337.19507859115,
						},
						[2] = {
							['y'] = 701219.49999998,
							['x'] = -228131.78124995,
							['heading'] = 337.19507788341,
						},
						[3] = {
							['y'] = 701224.18749998,
							['x'] = -228160.93749995,
							['heading'] = 337.19507853757,
						},
						[4] = {
							['y'] = 701228.87499998,
							['x'] = -228190.59374995,
							['heading'] = 337.19507798613,
						},
						[5] = {
							['y'] = 701233.68749998,
							['x'] = -228220.29687495,
							['heading'] = 337.19507746892,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 701209.68790749,
							['x'] = -228103.03859794,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 695790.86547505,
							['x'] = -215215.26889425,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Makara'] = {
					['units'] = {
						[1] = {
							['y'] = 577550.75000005,
							['x'] = -199591.875,
							['heading'] = 85.967608516485,
						},
						[2] = {
							['y'] = 577535.62500005,
							['x'] = -199565.953125,
							['heading'] = 85.967609188131,
						},
						[3] = {
							['y'] = 577520.25000005,
							['x'] = -199540.1875,
							['heading'] = 85.96760928238,
						},
						[4] = {
							['y'] = 577501.50000005,
							['x'] = -199519.484375,
							['heading'] = 85.967609620153,
						},
						[5] = {
							['y'] = 577480.50000005,
							['x'] = -199513.875,
							['heading'] = 85.967609600166,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 577550.72143375,
							['x'] = -199591.88219849,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 595738.45508921,
							['x'] = -198309.73817562,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Oni'] = {
					['units'] = {
						[1] = {
							['y'] = 735212.68750005,
							['x'] = -239405.28124998,
							['heading'] = 68.033377758226,
						},
						[2] = {
							['y'] = 735186.12500005,
							['x'] = -239419.15624998,
							['heading'] = 68.033377274848,
						},
						[3] = {
							['y'] = 735158.87500005,
							['x'] = -239431.46874998,
							['heading'] = 68.03337633136,
						},
						[4] = {
							['y'] = 735131.00000005,
							['x'] = -239442.24999998,
							['heading'] = 68.033373740008,
						},
						[5] = {
							['y'] = 735102.50000005,
							['x'] = -239451.45312498,
							['heading'] = 68.033375233331,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 735212.6982621,
							['x'] = -239405.28100135,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 756582.61605167,
							['x'] = -230785.75071567,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Kodori'] = {
					['units'] = {
						[1] = {
							['y'] = 599080.37500005,
							['x'] = -195442.53124998,
							['heading'] = 69.932128447929,
						},
						[2] = {
							['y'] = 599063.12500005,
							['x'] = -195467.07812498,
							['heading'] = 69.932129562035,
						},
						[3] = {
							['y'] = 599045.81250005,
							['x'] = -195491.65624998,
							['heading'] = 69.932129531879,
						},
						[4] = {
							['y'] = 599028.50000005,
							['x'] = -195516.26562498,
							['heading'] = 69.932127810874,
						},
						[5] = {
							['y'] = 599011.25000005,
							['x'] = -195540.81249998,
							['heading'] = 69.932128072692,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 599080.36355327,
							['x'] = -195442.52911498,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 618829.55013878,
							['x'] = -188227.90668146,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['West Bunker'] = {
					['units'] = {
						[1] = {
							['y'] = 607570.74999998,
							['x'] = -146830.67187505,
							['heading'] = 198.32931347806,
						},
						[2] = {
							['y'] = 607571.93749998,
							['x'] = -146800.84375005,
							['heading'] = 198.32931141429,
						},
						[3] = {
							['y'] = 607572.43749998,
							['x'] = -146770.90625005,
							['heading'] = 198.32931141429,
						},
						[4] = {
							['y'] = 607572.56249998,
							['x'] = -146740.90625005,
							['heading'] = 198.32931302745,
						},
						[5] = {
							['y'] = 607572.56249998,
							['x'] = -146710.85937505,
							['heading'] = 198.32931335437,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 1290,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 607570.77697125,
							['x'] = -146830.67164356,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 600363.32776176,
							['x'] = -168586.63623601,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Progress'] = {
					['units'] = {
						[1] = {
							['y'] = 750100.87499996,
							['x'] = -107782.89062497,
							['heading'] = 307.69052281887,
						},
						[2] = {
							['y'] = 750137.68749996,
							['x'] = -107796.97656247,
							['heading'] = 307.69052193519,
						},
						[3] = {
							['y'] = 750143.62499996,
							['x'] = -107815.03906247,
							['heading'] = 307.69052058087,
						},
						[4] = {
							['y'] = 750153.06249996,
							['x'] = -107843.58593747,
							['heading'] = 307.69052340254,
						},
						[5] = {
							['y'] = 750162.43749996,
							['x'] = -107872.13281247,
							['heading'] = 307.69052106999,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 750100.85886328,
							['x'] = -107782.8907986,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 732579.50829088,
							['x'] = -94245.481705176,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Kutaisi'] = {
					['units'] = {
						[1] = {
							['y'] = 701534.56249996,
							['x'] = -269228.53125003,
							['heading'] = 230.48192385167,
						},
						[2] = {
							['y'] = 701520.43749996,
							['x'] = -269202.12500003,
							['heading'] = 230.48192865925,
						},
						[3] = {
							['y'] = 701507.37499996,
							['x'] = -269175.21875003,
							['heading'] = 230.48191594821,
						},
						[4] = {
							['y'] = 701494.99999996,
							['x'] = -269147.96875003,
							['heading'] = 230.48192246751,
						},
						[5] = {
							['y'] = 701483.12499996,
							['x'] = -269120.46875003,
							['heading'] = 230.48192075579,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 701534.55800202,
							['x'] = -269228.54206753,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 684564.71844622,
							['x'] = -283226.39381996,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Mineralnye Vody'] = {
					['units'] = {
						[1] = {
							['y'] = 687359.81250004,
							['x'] = -68831.890624968,
							['heading'] = 49.620155104423,
						},
						[2] = {
							['y'] = 687331.12500004,
							['x'] = -68840.624999968,
							['heading'] = 49.620155104423,
						},
						[3] = {
							['y'] = 687302.43750004,
							['x'] = -68849.554687468,
							['heading'] = 49.620149681603,
						},
						[4] = {
							['y'] = 687273.93750004,
							['x'] = -68858.820312468,
							['heading'] = 49.620153115077,
						},
						[5] = {
							['y'] = 687245.56250004,
							['x'] = -68868.453124968,
							['heading'] = 49.620153723591,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 687359.83007263,
							['x'] = -68831.890944291,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 705325.70643398,
							['x'] = -53552.624709593,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Tkvarcheli'] = {
					['units'] = {
						[1] = {
							['y'] = 593618.87500003,
							['x'] = -234304.70312496,
							['heading'] = 34.924054892507,
						},
						[2] = {
							['y'] = 593601.81250003,
							['x'] = -234280.04687496,
							['heading'] = 34.924054892507,
						},
						[3] = {
							['y'] = 593584.68750003,
							['x'] = -234255.39062496,
							['heading'] = 34.924054892507,
						},
						[4] = {
							['y'] = 593567.62500003,
							['x'] = -234230.73437496,
							['heading'] = 34.924054892507,
						},
						[5] = {
							['y'] = 593550.50000003,
							['x'] = -234206.09374996,
							['heading'] = 34.924054892507,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 593618.8815868,
							['x'] = -234304.69742559,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 606184.4963353,
							['x'] = -216308.41611721,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['FARP Nigniy'] = {
					['units'] = {
						[1] = {
							['y'] = 815403.87499999,
							['x'] = -168349.75000005,
							['heading'] = 189.5070596223,
						},
						[2] = {
							['y'] = 815418.37499999,
							['x'] = -168323.54687505,
							['heading'] = 189.50705906632,
						},
						[3] = {
							['y'] = 815432.87499999,
							['x'] = -168297.29687505,
							['heading'] = 189.50705878022,
						},
						[4] = {
							['y'] = 815447.37499999,
							['x'] = -168271.04687505,
							['heading'] = 189.50705906632,
						},
						[5] = {
							['y'] = 815461.87499999,
							['x'] = -168244.79687505,
							['heading'] = 189.50705906632,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 815403.87293336,
							['x'] = -168349.75229978,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 811531.10910929,
							['x'] = -191474.98849755,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Beslan'] = {
					['units'] = {
						[1] = {
							['y'] = 853356.12499997,
							['x'] = -166253.09374996,
							['heading'] = 326.02830709922,
						},
						[2] = {
							['y'] = 853383.87499997,
							['x'] = -166264.46874996,
							['heading'] = 326.02830235873,
						},
						[3] = {
							['y'] = 853411.56249997,
							['x'] = -166276.01562496,
							['heading'] = 326.02831085529,
						},
						[4] = {
							['y'] = 853426.81249997,
							['x'] = -166273.03124996,
							['heading'] = 326.02830625189,
						},
						[5] = {
							['y'] = 853432.06249997,
							['x'] = -166259.23437496,
							['heading'] = 326.02830708681,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 853356.14357801,
							['x'] = -166253.08811546,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 842175.67705662,
							['x'] = -149659.6886232,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Nalchik'] = {
					['units'] = {
						[1] = {
							['y'] = 781582.68749995,
							['x'] = -131289.46874999,
							['heading'] = 281.95159207995,
						},
						[2] = {
							['y'] = 781627.62499995,
							['x'] = -131284.10937499,
							['heading'] = 281.95159207178,
						},
						[3] = {
							['y'] = 781642.49999995,
							['x'] = -131297.34374999,
							['heading'] = 281.95159290705,
						},
						[4] = {
							['y'] = 781664.87499995,
							['x'] = -131317.29687499,
							['heading'] = 281.95159290705,
						},
						[5] = {
							['y'] = 781687.24999995,
							['x'] = -131337.26562499,
							['heading'] = 281.95159290705,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 781582.67311933,
							['x'] = -131289.47178043,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 760450.90425213,
							['x'] = -126816.43389751,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Senaki-Kolkhi'] = {
					['units'] = {
						[1] = {
							['y'] = 669284.24999995,
							['x'] = -288916.15624998,
							['heading'] = 292.23385003543,
						},
						[2] = {
							['y'] = 669312.87499995,
							['x'] = -288925.06249998,
							['heading'] = 292.23384998503,
						},
						[3] = {
							['y'] = 669341.18749995,
							['x'] = -288934.81249998,
							['heading'] = 292.2338496853,
						},
						[4] = {
							['y'] = 669369.31249995,
							['x'] = -288945.31249998,
							['heading'] = 292.23384945016,
						},
						[5] = {
							['y'] = 669397.56249995,
							['x'] = -288955.46874998,
							['heading'] = 292.23384952409,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 669284.23959373,
							['x'] = -288916.16190523,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 648356.1095875,
							['x'] = -280361.12331916,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
				['Sukhumi-Babushara'] = {
					['units'] = {
						[1] = {
							['y'] = 586952.81249995,
							['x'] = -226803.81249998,
							['heading'] = 289.31013555517,
						},
						[2] = {
							['y'] = 586982.18749995,
							['x'] = -226809.59374998,
							['heading'] = 289.31013926032,
						},
						[3] = {
							['y'] = 587011.68749995,
							['x'] = -226814.87499998,
							['heading'] = 289.3101370017,
						},
						[4] = {
							['y'] = 587041.24999995,
							['x'] = -226819.68749998,
							['heading'] = 289.31013408327,
						},
						[5] = {
							['y'] = 587070.87499995,
							['x'] = -226824.09374998,
							['heading'] = 289.31013569475,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 586952.78205935,
							['x'] = -226803.81436081,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 565593.86511857,
							['x'] = -219319.78579534,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 2,
				},
			},
			['red'] = {
				['FARP Derchi'] = {
					['units'] = {
						[1] = {
							['y'] = 698389.75000001,
							['x'] = -263530.21874995,
							['heading'] = 15.665335318442,
						},
						[2] = {
							['y'] = 698367.62500001,
							['x'] = -263550.18749995,
							['heading'] = 15.665333345683,
						},
						[3] = {
							['y'] = 698346.43750001,
							['x'] = -263571.28124995,
							['heading'] = 15.665331694774,
						},
						[4] = {
							['y'] = 698326.50000001,
							['x'] = -263593.56249995,
							['heading'] = 15.665335319308,
						},
						[5] = {
							['y'] = 698307.31250001,
							['x'] = -263616.56249995,
							['heading'] = 15.665332731625,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 698389.74608955,
							['x'] = -263530.22758513,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 702517.12862396,
							['x'] = -248812.41955321,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
				},
				['FARP Inguri Valley'] = {
					['units'] = {
						[1] = {
							['y'] = 640265.75000002,
							['x'] = -218865.51562496,
							['heading'] = 25.422529406398,
						},
						[2] = {
							['y'] = 640261.68750002,
							['x'] = -218895.26562496,
							['heading'] = 25.422529637716,
						},
						[3] = {
							['y'] = 640256.75000002,
							['x'] = -218924.76562496,
							['heading'] = 25.42253029015,
						},
						[4] = {
							['y'] = 640249.68750002,
							['x'] = -218953.81249996,
							['heading'] = 25.422530110924,
						},
						[5] = {
							['y'] = 640240.68750002,
							['x'] = -218982.24999996,
							['heading'] = 25.422528459148,
						},
					},
					['route'] = {
						[1] = {
							['alt'] = 669,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 640265.72931338,
							['x'] = -218865.51657182,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 647625.64730499,
							['x'] = -203381.28116333,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['country'] = 0,
				},
				['FARP Oche'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 657874.36184205,
							['x'] = -265213.87308429,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 665559.26487747,
							['x'] = -246304.96878966,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 657874.37500002,
							['x'] = -265213.87499995,
							['heading'] = 22.117667156001,
						},
						[2] = {
							['y'] = 657848.75000002,
							['x'] = -265229.46874995,
							['heading'] = 22.117668737895,
						},
						[3] = {
							['y'] = 657823.12500002,
							['x'] = -265245.09374995,
							['heading'] = 22.117668247255,
						},
						[4] = {
							['y'] = 657797.56250002,
							['x'] = -265260.84374995,
							['heading'] = 22.117669050895,
						},
						[5] = {
							['y'] = 657772.18750002,
							['x'] = -265276.84374995,
							['heading'] = 22.117668788666,
						},
					},
				},
				['FARP Balkariya'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 756112.47486195,
							['x'] = -158843.05067316,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 749959.16479367,
							['x'] = -172354.7165627,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 756112.49999998,
							['x'] = -158843.04687504,
							['heading'] = 204.48485670932,
						},
						[2] = {
							['y'] = 756121.99999998,
							['x'] = -158814.70312504,
							['heading'] = 204.48485631322,
						},
						[3] = {
							['y'] = 756131.62499998,
							['x'] = -158786.18750004,
							['heading'] = 204.48485665024,
						},
						[4] = {
							['y'] = 756141.18749998,
							['x'] = -158757.73437504,
							['heading'] = 204.48485804778,
						},
						[5] = {
							['y'] = 756150.74999998,
							['x'] = -158729.23437504,
							['heading'] = 204.48485812356,
						},
					},
				},
				['FARP Tsvirmi'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 690759.14245663,
							['x'] = -187021.61233791,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'Off Road',
							['alt_type'] = 'BARO',
							['form'] = 'Off Road',
							['y'] = 701941.04347771,
							['x'] = -184960.5744603,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 690759.12500005,
							['x'] = -187021.60937499,
							['heading'] = 79.556505211862,
						},
						[2] = {
							['y'] = 690737.12500005,
							['x'] = -187037.46874999,
							['heading'] = 79.556505695793,
						},
						[3] = {
							['y'] = 690712.81250005,
							['x'] = -187055.01562499,
							['heading'] = 79.556505211862,
						},
						[4] = {
							['y'] = 690688.50000005,
							['x'] = -187072.54687499,
							['heading'] = 79.55650669129,
						},
						[5] = {
							['y'] = 690664.12500005,
							['x'] = -187090.09374999,
							['heading'] = 79.55650669129,
						},
					},
				},
				['Inguri-Dam'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 633375.81704651,
							['x'] = -244597.91928341,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 640169.97556623,
							['x'] = -224157.52718451,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 633375.81250002,
							['x'] = -244597.92187495,
							['heading'] = 18.386232345949,
						},
						[2] = {
							['y'] = 633354.93750002,
							['x'] = -244619.54687495,
							['heading'] = 18.386232925885,
						},
						[3] = {
							['y'] = 633334.00000002,
							['x'] = -244641.12499995,
							['heading'] = 18.386231369731,
						},
						[4] = {
							['y'] = 633312.93750002,
							['x'] = -244662.53124995,
							['heading'] = 18.386230365969,
						},
						[5] = {
							['y'] = 633291.68750002,
							['x'] = -244683.74999995,
							['heading'] = 18.386232630611,
						},
					},
				},
				['FARP Elbrus'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 692983.51094315,
							['x'] = -155766.14089796,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 673254.85314264,
							['x'] = -164803.41446128,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 692983.49999996,
							['x'] = -155766.14062502,
							['heading'] = 245.38849726581,
						},
						[2] = {
							['y'] = 693005.74999996,
							['x'] = -155746.00000002,
							['heading'] = 245.38849792477,
						},
						[3] = {
							['y'] = 693027.93749996,
							['x'] = -155725.93750002,
							['heading'] = 245.38849680053,
						},
						[4] = {
							['y'] = 693050.18749996,
							['x'] = -155705.84375002,
							['heading'] = 245.38849603964,
						},
						[5] = {
							['y'] = 693072.49999996,
							['x'] = -155685.78125002,
							['heading'] = 245.3884960898,
						},
					},
				},
				['East Bunker'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 806267.46676788,
							['x'] = -193627.06771752,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 801500.3951913,
							['x'] = -212714.95740077,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 806267.43749999,
							['x'] = -193627.06250005,
							['heading'] = 194.02240087466,
						},
						[2] = {
							['y'] = 806295.24999999,
							['x'] = -193615.93750005,
							['heading'] = 194.02239884854,
						},
						[3] = {
							['y'] = 806322.99999999,
							['x'] = -193604.81250005,
							['heading'] = 194.02240043482,
						},
						[4] = {
							['y'] = 806350.87499999,
							['x'] = -193593.65625005,
							['heading'] = 194.02240043482,
						},
						[5] = {
							['y'] = 806378.68749999,
							['x'] = -193582.50000005,
							['heading'] = 194.02240125825,
						},
					},
				},
				['FARP Zugdidi'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 625822.34694212,
							['x'] = -268160.12139438,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 630819.81970017,
							['x'] = -247566.14059679,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 625822.37500001,
							['x'] = -268160.12499995,
							['heading'] = 13.64011269893,
						},
						[2] = {
							['y'] = 625843.56250001,
							['x'] = -268181.31249995,
							['heading'] = 13.64011269893,
						},
						[3] = {
							['y'] = 625864.75000001,
							['x'] = -268202.46874995,
							['heading'] = 13.640112601579,
						},
						[4] = {
							['y'] = 625885.87500001,
							['x'] = -268223.68749995,
							['heading'] = 13.640114163617,
						},
						[5] = {
							['y'] = 625907.06250001,
							['x'] = -268244.90624995,
							['heading'] = 13.640114066267,
						},
					},
				},
				['Senaki-Kolkhi'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 669284.23959373,
							['x'] = -288916.16190523,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 648356.1095875,
							['x'] = -280361.12331916,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 669284.24999995,
							['x'] = -288916.15624998,
							['heading'] = 292.23385003543,
						},
						[2] = {
							['y'] = 669312.87499995,
							['x'] = -288925.06249998,
							['heading'] = 292.23384998503,
						},
						[3] = {
							['y'] = 669341.18749995,
							['x'] = -288934.81249998,
							['heading'] = 292.2338496853,
						},
						[4] = {
							['y'] = 669369.31249995,
							['x'] = -288945.31249998,
							['heading'] = 292.23384945016,
						},
						[5] = {
							['y'] = 669397.56249995,
							['x'] = -288955.46874998,
							['heading'] = 292.23384952409,
						},
					},
				},
				['FARP Elhotovo'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 821336.21698913,
							['x'] = -155217.22969356,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 809821.52098811,
							['x'] = -137391.34308655,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 821336.18749997,
							['x'] = -155217.23437496,
							['heading'] = 327.13941408017,
						},
						[2] = {
							['y'] = 821361.99999997,
							['x'] = -155232.34374996,
							['heading'] = 327.13941491757,
						},
						[3] = {
							['y'] = 821387.87499997,
							['x'] = -155247.48437496,
							['heading'] = 327.13941491757,
						},
						[4] = {
							['y'] = 821413.74999997,
							['x'] = -155262.64062496,
							['heading'] = 327.13941491757,
						},
						[5] = {
							['y'] = 821439.56249997,
							['x'] = -155277.76562496,
							['heading'] = 327.13941408017,
						},
					},
				},
				['FARP Makara'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 577550.72143375,
							['x'] = -199591.88219849,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 595738.45508921,
							['x'] = -198309.73817562,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 577550.75000005,
							['x'] = -199591.875,
							['heading'] = 85.967608516485,
						},
						[2] = {
							['y'] = 577535.62500005,
							['x'] = -199565.953125,
							['heading'] = 85.967609188131,
						},
						[3] = {
							['y'] = 577520.25000005,
							['x'] = -199540.1875,
							['heading'] = 85.96760928238,
						},
						[4] = {
							['y'] = 577501.50000005,
							['x'] = -199519.484375,
							['heading'] = 85.967609620153,
						},
						[5] = {
							['y'] = 577480.50000005,
							['x'] = -199513.875,
							['heading'] = 85.967609600166,
						},
					},
				},
				['FARP Lentehi'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 701209.68790749,
							['x'] = -228103.03859794,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 695790.86547505,
							['x'] = -215215.26889425,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 701209.68749998,
							['x'] = -228103.03124995,
							['heading'] = 337.19507859115,
						},
						[2] = {
							['y'] = 701219.49999998,
							['x'] = -228131.78124995,
							['heading'] = 337.19507788341,
						},
						[3] = {
							['y'] = 701224.18749998,
							['x'] = -228160.93749995,
							['heading'] = 337.19507853757,
						},
						[4] = {
							['y'] = 701228.87499998,
							['x'] = -228190.59374995,
							['heading'] = 337.19507798613,
						},
						[5] = {
							['y'] = 701233.68749998,
							['x'] = -228220.29687495,
							['heading'] = 337.19507746892,
						},
					},
				},
				['FARP Nigniy'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 815403.87293336,
							['x'] = -168349.75229978,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 811531.10910929,
							['x'] = -191474.98849755,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 815403.87499999,
							['x'] = -168349.75000005,
							['heading'] = 189.5070596223,
						},
						[2] = {
							['y'] = 815418.37499999,
							['x'] = -168323.54687505,
							['heading'] = 189.50705906632,
						},
						[3] = {
							['y'] = 815432.87499999,
							['x'] = -168297.29687505,
							['heading'] = 189.50705878022,
						},
						[4] = {
							['y'] = 815447.37499999,
							['x'] = -168271.04687505,
							['heading'] = 189.50705906632,
						},
						[5] = {
							['y'] = 815461.87499999,
							['x'] = -168244.79687505,
							['heading'] = 189.50705906632,
						},
					},
				},
				['FARP Kodori'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 599080.36355327,
							['x'] = -195442.52911498,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 618829.55013878,
							['x'] = -188227.90668146,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 599080.37500005,
							['x'] = -195442.53124998,
							['heading'] = 69.932128447929,
						},
						[2] = {
							['y'] = 599063.12500005,
							['x'] = -195467.07812498,
							['heading'] = 69.932129562035,
						},
						[3] = {
							['y'] = 599045.81250005,
							['x'] = -195491.65624998,
							['heading'] = 69.932129531879,
						},
						[4] = {
							['y'] = 599028.50000005,
							['x'] = -195516.26562498,
							['heading'] = 69.932127810874,
						},
						[5] = {
							['y'] = 599011.25000005,
							['x'] = -195540.81249998,
							['heading'] = 69.932128072692,
						},
					},
				},
				['West Bunker'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 1290,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 607570.77697125,
							['x'] = -146830.67164356,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 600363.32776176,
							['x'] = -168586.63623601,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 607570.74999998,
							['x'] = -146830.67187505,
							['heading'] = 198.32931347806,
						},
						[2] = {
							['y'] = 607571.93749998,
							['x'] = -146800.84375005,
							['heading'] = 198.32931141429,
						},
						[3] = {
							['y'] = 607572.43749998,
							['x'] = -146770.90625005,
							['heading'] = 198.32931141429,
						},
						[4] = {
							['y'] = 607572.56249998,
							['x'] = -146740.90625005,
							['heading'] = 198.32931302745,
						},
						[5] = {
							['y'] = 607572.56249998,
							['x'] = -146710.85937505,
							['heading'] = 198.32931335437,
						},
					},
				},
				['FARP Tkvarcheli'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 593618.8815868,
							['x'] = -234304.69742559,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 606184.4963353,
							['x'] = -216308.41611721,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 593618.87500003,
							['x'] = -234304.70312496,
							['heading'] = 34.924054892507,
						},
						[2] = {
							['y'] = 593601.81250003,
							['x'] = -234280.04687496,
							['heading'] = 34.924054892507,
						},
						[3] = {
							['y'] = 593584.68750003,
							['x'] = -234255.39062496,
							['heading'] = 34.924054892507,
						},
						[4] = {
							['y'] = 593567.62500003,
							['x'] = -234230.73437496,
							['heading'] = 34.924054892507,
						},
						[5] = {
							['y'] = 593550.50000003,
							['x'] = -234206.09374996,
							['heading'] = 34.924054892507,
						},
					},
				},
				['Kutaisi'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 701534.55800202,
							['x'] = -269228.54206753,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 684564.71844622,
							['x'] = -283226.39381996,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 701534.56249996,
							['x'] = -269228.53125003,
							['heading'] = 230.48192385167,
						},
						[2] = {
							['y'] = 701520.43749996,
							['x'] = -269202.12500003,
							['heading'] = 230.48192865925,
						},
						[3] = {
							['y'] = 701507.37499996,
							['x'] = -269175.21875003,
							['heading'] = 230.48191594821,
						},
						[4] = {
							['y'] = 701494.99999996,
							['x'] = -269147.96875003,
							['heading'] = 230.48192246751,
						},
						[5] = {
							['y'] = 701483.12499996,
							['x'] = -269120.46875003,
							['heading'] = 230.48192075579,
						},
					},
				},
				['Mineralnye Vody'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 687359.83007263,
							['x'] = -68831.890944291,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 705325.70643398,
							['x'] = -53552.624709593,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 687359.81250004,
							['x'] = -68831.890624968,
							['heading'] = 49.620155104423,
						},
						[2] = {
							['y'] = 687331.12500004,
							['x'] = -68840.624999968,
							['heading'] = 49.620155104423,
						},
						[3] = {
							['y'] = 687302.43750004,
							['x'] = -68849.554687468,
							['heading'] = 49.620149681603,
						},
						[4] = {
							['y'] = 687273.93750004,
							['x'] = -68858.820312468,
							['heading'] = 49.620153115077,
						},
						[5] = {
							['y'] = 687245.56250004,
							['x'] = -68868.453124968,
							['heading'] = 49.620153723591,
						},
					},
				},
				['FARP Bylym'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 728922.77251898,
							['x'] = -118704.74268652,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 713155.26523472,
							['x'] = -135975.39265291,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 728922.74999997,
							['x'] = -118704.74218754,
							['heading'] = 222.39500600081,
						},
						[2] = {
							['y'] = 728948.68749997,
							['x'] = -118689.85937504,
							['heading'] = 222.39500571443,
						},
						[3] = {
							['y'] = 728974.99999997,
							['x'] = -118675.42968754,
							['heading'] = 222.39500571443,
						},
						[4] = {
							['y'] = 729001.62499997,
							['x'] = -118661.54687504,
							['heading'] = 222.39500492131,
						},
						[5] = {
							['y'] = 729028.43749997,
							['x'] = -118648.18750004,
							['heading'] = 222.39500743441,
						},
					},
				},
				['FARP Oni'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 735212.6982621,
							['x'] = -239405.28100135,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 756582.61605167,
							['x'] = -230785.75071567,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 735212.68750005,
							['x'] = -239405.28124998,
							['heading'] = 68.033377758226,
						},
						[2] = {
							['y'] = 735186.12500005,
							['x'] = -239419.15624998,
							['heading'] = 68.033377274848,
						},
						[3] = {
							['y'] = 735158.87500005,
							['x'] = -239431.46874998,
							['heading'] = 68.03337633136,
						},
						[4] = {
							['y'] = 735131.00000005,
							['x'] = -239442.24999998,
							['heading'] = 68.033373740008,
						},
						[5] = {
							['y'] = 735102.50000005,
							['x'] = -239451.45312498,
							['heading'] = 68.033375233331,
						},
					},
				},
				['Beslan'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 853356.14357801,
							['x'] = -166253.08811546,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 842175.67705662,
							['x'] = -149659.6886232,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 853356.12499997,
							['x'] = -166253.09374996,
							['heading'] = 326.02830709922,
						},
						[2] = {
							['y'] = 853383.87499997,
							['x'] = -166264.46874996,
							['heading'] = 326.02830235873,
						},
						[3] = {
							['y'] = 853411.56249997,
							['x'] = -166276.01562496,
							['heading'] = 326.02831085529,
						},
						[4] = {
							['y'] = 853426.81249997,
							['x'] = -166273.03124996,
							['heading'] = 326.02830625189,
						},
						[5] = {
							['y'] = 853432.06249997,
							['x'] = -166259.23437496,
							['heading'] = 326.02830708681,
						},
					},
				},
				['FARP Progress'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 750100.85886328,
							['x'] = -107782.8907986,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 732579.50829088,
							['x'] = -94245.481705176,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 750100.87499996,
							['x'] = -107782.89062497,
							['heading'] = 307.69052281887,
						},
						[2] = {
							['y'] = 750137.68749996,
							['x'] = -107796.97656247,
							['heading'] = 307.69052193519,
						},
						[3] = {
							['y'] = 750143.62499996,
							['x'] = -107815.03906247,
							['heading'] = 307.69052058087,
						},
						[4] = {
							['y'] = 750153.06249996,
							['x'] = -107843.58593747,
							['heading'] = 307.69052340254,
						},
						[5] = {
							['y'] = 750162.43749996,
							['x'] = -107872.13281247,
							['heading'] = 307.69052106999,
						},
					},
				},
				['Nalchik'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 781582.67311933,
							['x'] = -131289.47178043,
							['speed'] = 27.777777777778,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['enabled'] = true,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['number'] = 1,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 1190,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 760450.90425213,
							['x'] = -126816.43389751,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 781582.68749995,
							['x'] = -131289.46874999,
							['heading'] = 281.95159207995,
						},
						[2] = {
							['y'] = 781627.62499995,
							['x'] = -131284.10937499,
							['heading'] = 281.95159207178,
						},
						[3] = {
							['y'] = 781642.49999995,
							['x'] = -131297.34374999,
							['heading'] = 281.95159290705,
						},
						[4] = {
							['y'] = 781664.87499995,
							['x'] = -131317.29687499,
							['heading'] = 281.95159290705,
						},
						[5] = {
							['y'] = 781687.24999995,
							['x'] = -131337.26562499,
							['heading'] = 281.95159290705,
						},
					},
				},
				['Sukhumi-Babushara'] = {
					['country'] = 0,
					['route'] = {
						[1] = {
							['alt'] = 38,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 586952.78205935,
							['x'] = -226803.81436081,
							['speed'] = 5.5555555555556,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
										[1] = {
											['number'] = 1,
											['auto'] = true,
											['id'] = 'WrappedAction',
											['enabled'] = true,
											['params'] = {
												['action'] = {
													['id'] = 'EPLRS',
													['params'] = {
														['value'] = true,
														['groupId'] = 16,
													},
												},
											},
										},
									},
								},
							},
						},
						[2] = {
							['alt'] = 14,
							['type'] = 'Turning Point',
							['action'] = 'On Road',
							['alt_type'] = 'BARO',
							['form'] = 'On Road',
							['y'] = 565593.86511857,
							['x'] = -219319.78579534,
							['speed'] = 20,
							['task'] = {
								['id'] = 'ComboTask',
								['params'] = {
									['tasks'] = {
									},
								},
							},
						},
					},
					['units'] = {
						[1] = {
							['y'] = 586952.81249995,
							['x'] = -226803.81249998,
							['heading'] = 289.31013555517,
						},
						[2] = {
							['y'] = 586982.18749995,
							['x'] = -226809.59374998,
							['heading'] = 289.31013926032,
						},
						[3] = {
							['y'] = 587011.68749995,
							['x'] = -226814.87499998,
							['heading'] = 289.3101370017,
						},
						[4] = {
							['y'] = 587041.24999995,
							['x'] = -226819.68749998,
							['heading'] = 289.31013408327,
						},
						[5] = {
							['y'] = 587070.87499995,
							['x'] = -226824.09374998,
							['heading'] = 289.31013569475,
						},
					},
				},
			},
		},
		['awacs'] = {
			['blue'] = {
				['modulation'] = 0,
				['country'] = 'usa',
				['uncontrolled'] = false,
				['hidden'] = false,
				['units'] = {
					[1] = {
						['alt'] = 10000,
						['type'] = 'E-3A',
						['unitId'] = 1092,
						['livery_id'] = 'nato',
						['skill'] = 'High',
						['alt_type'] = 'BARO',
						['y'] = 594920.60643376,
						['x'] = -287370.57826735,
						['speed'] = 180.55555555556,
						['payload'] = {
							['pylons'] = {
							},
							['fuel'] = '65000',
							['flare'] = 60,
							['chaff'] = 120,
							['gun'] = 100,
						},
						['unitName'] = 'AWACS_blue',
						['heading'] = 1.7601466271953,
						['onboard_num'] = '068',
						['callsign'] = {
							[1] = 1,
							[2] = 1,
							['name'] = 'Overlord11',
							[3] = 1,
						},
					},
				},
				['countryId'] = 2,
				['radioSet'] = false,
				['route'] = {
					[1] = {
						['alt'] = 10000,
						['type'] = 'Turning Point',
						['action'] = 'Turning Point',
						['alt_type'] = 'BARO',
						['form'] = 'Turning Point',
						['y'] = 594920.60643376,
						['x'] = -287370.57826735,
						['speed'] = 180.55555555556,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = true,
										['id'] = 'AWACS',
										['enabled'] = true,
										['params'] = {
										},
									},
									[2] = {
										['number'] = 2,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 8,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 10000,
						['type'] = 'Turning Point',
						['action'] = 'Turning Point',
						['alt_type'] = 'BARO',
						['form'] = 'Turning Point',
						['y'] = 709302.49612603,
						['x'] = -309291.43182016,
						['speed'] = 180.55555555556,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = false,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'SwitchWaypoint',
												['params'] = {
													['goToWaypointIndex'] = 1,
													['fromWaypointIndex'] = 2,
												},
											},
										},
									},
								},
							},
						},
					},
				},
				['category'] = 'plane',
				['coalition'] = 'blue',
				['startTime'] = 0,
				['task'] = 'AWACS',
				['frequency'] = 251,
			},
			['red'] = {
				['modulation'] = 0,
				['country'] = 'usaf aggressors',
				['uncontrolled'] = false,
				['hidden'] = false,
				['units'] = {
					[1] = {
						['alt'] = 10000,
						['type'] = 'E-3A',
						['callsign'] = {
							[1] = 1,
							[2] = 1,
							['name'] = 'Overlord11',
							[3] = 1,
						},
						['skill'] = 'Excellent',
						['y'] = 713237.00830218,
						['x'] = -38091.128250047,
						['payload'] = {
							['pylons'] = {
							},
							['fuel'] = '65000',
							['flare'] = 60,
							['chaff'] = 120,
							['gun'] = 100,
						},
						['speed'] = 180.55555555556,
						['alt_type'] = 'BARO',
						['heading'] = 2.014883715007,
						['onboard_num'] = '068',
					},
				},
				['countryId'] = 7,
				['radioSet'] = false,
				['route'] = {
					[1] = {
						['alt'] = 10000,
						['type'] = 'Turning Point',
						['action'] = 'Turning Point',
						['alt_type'] = 'BARO',
						['form'] = 'Turning Point',
						['y'] = 713237.00830218,
						['x'] = -38091.128250047,
						['speed'] = 180.55555555556,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = true,
										['id'] = 'AWACS',
										['enabled'] = true,
										['params'] = {
										},
									},
									[2] = {
										['number'] = 2,
										['auto'] = true,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'EPLRS',
												['params'] = {
													['value'] = true,
													['groupId'] = 8,
												},
											},
										},
									},
								},
							},
						},
					},
					[2] = {
						['alt'] = 10000,
						['type'] = 'Turning Point',
						['action'] = 'Turning Point',
						['alt_type'] = 'BARO',
						['form'] = 'Turning Point',
						['y'] = 811880.84928986,
						['x'] = -85024.237779794,
						['speed'] = 180.55555555556,
						['task'] = {
							['id'] = 'ComboTask',
							['params'] = {
								['tasks'] = {
									[1] = {
										['number'] = 1,
										['auto'] = false,
										['id'] = 'WrappedAction',
										['enabled'] = true,
										['params'] = {
											['action'] = {
												['id'] = 'SwitchWaypoint',
												['params'] = {
													['goToWaypointIndex'] = 1,
													['fromWaypointIndex'] = 2,
												},
											},
										},
									},
								},
							},
						},
					},
				},
				['category'] = 'plane',
				['coalition'] = 'red',
				['startTime'] = 0,
				['task'] = 'AWACS',
				['frequency'] = 251,
			},
		},
	},
}
return t